﻿Côté Staff :

--> Le staff est composé de plusieurs enseignants (nldr : plusieurs comptes).
--> Le staff doit pouvoir changer de catégorie un pratiquant, manuellement, vers la catégorie du dessus.
--> Le staff peut, pour une période déterminée, mettre un nouveau pratiquant dans une catégorie inférieur.
--> Le staff peut inscrire un pratiqunt à une nouvelle compétition.
--> Le staff peut mettre la fiche d'un pratiquant à jour (taille/poid/examens/etc.)
--> Le staff doit pouvoir mettre à jour la liste des cours et des examens.
--> Le staff doit pouvoir mettre sur le calendrier les heures et lieux des cours/examens.

Côté pratiquants :

--> Le pratiquant peut s'inscrire (Avec une clef ?)
--> Le pratiquant peut modifier ses informations.
--> Le pratiquant peut voir son calendrier.
--> Le pratiquant peut s'inscrire à des examens/cours.
--> Le pratiquant doit avoir accès à son historique pédagogique.

Autre :

--> La saison commence en septembre.
--> Il y a plusieurs sports, mais un système unifié de groupes et de grades.
--> Un élève doit avoir suivi un certain nombre de mois de cours avant de pouvoir passer l'examen d'accès au grade suivant.
--> Plusieurs salles localisées à divers lieux.
--> Plusieurs types de cours pour un même sport.
--> La date des examens est fixée dès septembre pour l'ensemble de la saison.
--> Les couleurs des ceintures sont équivalentes entre les différents groupes.
--> Le suivi pédagogique doit-être, au minima, une suite des examens passés par l'élève avec une mention réussite/échec et le grade actuel.
--> Un élève ne peut s'inscrire à deux compétitions ayant lieux au même moment à deux endroits différents.
--> Il se peut qu'il y ait plus de 4 catégories pour les compétitions (à ne pas hardcoder donc).

demandes du prof :
--> Diagrammes de séquences, de use cases et diagrammes de classes.
--> On peut ajouter d'autres diagrammes.

Questions sup:

--> Membre du staff qui peut changer le statut d'un élève, inclut-il le secrétariat (spectre large)? 
--> Qu'entend-il par rôle dans le cadre des uses cases (cahier des charges) ?
--> Au niveau du cahier de charge, qu'entend-on par exigences fonctionnelles ?
--> Examen = cours et examen = stage ? 
--> Doit-on ajouter un Paypal ou autre ? Et gestion de comptabilité minimaliste ? 
------------------------------------------------
--> Le staff peut-il réinitialisé/changer le mdp d'un utilisateur ?
--> Que doit-on sortir sur PDF ?
--> Quelle approche pour la gestion des comptes enseignants/élèves ?
--> Que doit comprendre la fiche pédagogique ? 
--> Mêmes thèmes de cours et de stages ? 
--> Un examen est lié à un cours/stage ?
