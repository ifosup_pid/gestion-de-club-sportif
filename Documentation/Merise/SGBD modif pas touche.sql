CREATE DATABASE  IF NOT EXISTS `lvagma129870com29190_gcsp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `lvagma129870com29190_gcsp`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: gcsp
-- ------------------------------------------------------
-- Server version	5.1.73-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorie` (
  `id_categorie` int(11) NOT NULL AUTO_INCREMENT,
  `categorie` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
INSERT INTO `categorie` VALUES (1,'catégorie 1'),(2,'catégorie 2'),(3,'catégorie 3');
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competition`
--

DROP TABLE IF EXISTS `competition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competition` (
  `titre` varchar(150) DEFAULT NULL,
  `id_evenement` int(11) NOT NULL,
  PRIMARY KEY (`id_evenement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competition`
--

LOCK TABLES `competition` WRITE;
/*!40000 ALTER TABLE `competition` DISABLE KEYS */;
INSERT INTO `competition` VALUES ('une compétition',21),('un autre compétition',27);
/*!40000 ALTER TABLE `competition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competitions_categories`
--

DROP TABLE IF EXISTS `competitions_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competitions_categories` (
  `id_categorie` int(11) NOT NULL,
  `id_evenement` int(11) NOT NULL,
  PRIMARY KEY (`id_categorie`,`id_evenement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competitions_categories`
--

LOCK TABLES `competitions_categories` WRITE;
/*!40000 ALTER TABLE `competitions_categories` DISABLE KEYS */;
INSERT INTO `competitions_categories` VALUES (2,13),(2,14),(2,16),(2,21),(2,27);
/*!40000 ALTER TABLE `competitions_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cours`
--

DROP TABLE IF EXISTS `cours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cours` (
  `section` tinyint(4) DEFAULT NULL,
  `id_evenement` int(11) NOT NULL,
  PRIMARY KEY (`id_evenement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cours`
--

LOCK TABLES `cours` WRITE;
/*!40000 ALTER TABLE `cours` DISABLE KEYS */;
INSERT INTO `cours` VALUES (1,5),(3,12),(2,19);
/*!40000 ALTER TABLE `cours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cours_themes`
--

DROP TABLE IF EXISTS `cours_themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cours_themes` (
  `id_evenement` int(11) NOT NULL,
  `id_theme` int(11) NOT NULL,
  PRIMARY KEY (`id_evenement`,`id_theme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cours_themes`
--

LOCK TABLES `cours_themes` WRITE;
/*!40000 ALTER TABLE `cours_themes` DISABLE KEYS */;
INSERT INTO `cours_themes` VALUES (0,3),(5,2),(12,3),(19,2);
/*!40000 ALTER TABLE `cours_themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evenement`
--

DROP TABLE IF EXISTS `evenement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evenement` (
  `id_evenement` int(11) NOT NULL AUTO_INCREMENT,
  `debut` datetime DEFAULT NULL,
  `fin` datetime DEFAULT NULL,
  `id_lieu` int(11) NOT NULL,
  `jour` enum('lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche') DEFAULT NULL,
  `description` mediumtext,
  PRIMARY KEY (`id_evenement`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evenement`
--

LOCK TABLES `evenement` WRITE;
/*!40000 ALTER TABLE `evenement` DISABLE KEYS */;
INSERT INTO `evenement` VALUES (2,'2016-12-22 17:50:00','2016-12-22 18:05:00',52,NULL,'une description'),(3,'2016-12-08 18:15:00','2018-12-04 18:15:00',53,NULL,'une description'),(4,'2016-12-14 06:15:00','2016-12-14 06:15:00',54,NULL,NULL),(5,'2016-12-24 18:50:00','2016-12-24 19:05:00',55,'mercredi','une description'),(6,'2016-12-14 20:45:00','2016-12-14 20:45:00',56,NULL,'une description'),(7,'2016-12-22 20:50:00','2016-12-22 20:50:00',57,NULL,'une description'),(12,'2024-12-20 19:40:00','2024-12-20 19:40:00',62,'jeudi','une description'),(13,'2024-12-20 19:50:00','2024-12-20 19:50:00',63,NULL,'une description'),(15,'2016-12-24 18:05:00','2016-12-24 18:05:00',65,NULL,'une description'),(16,'2016-12-15 18:00:00','2016-12-15 18:00:00',66,NULL,NULL),(17,'2016-12-15 06:40:00','2016-12-15 06:40:00',67,NULL,NULL),(18,'2016-12-15 06:15:00','2016-12-15 06:15:00',68,NULL,'une description'),(19,'2016-12-07 19:05:00','2016-12-24 21:05:00',69,'mercredi','une description'),(20,'2016-12-15 17:00:00','2016-12-15 19:00:00',70,NULL,'une description'),(21,'2016-12-06 11:55:00','2016-12-06 11:55:00',71,NULL,'une description'),(26,'2016-12-16 12:15:00','2016-12-16 12:15:00',76,NULL,'une description'),(27,'2006-12-20 12:15:00','2006-12-20 12:15:00',77,NULL,'une description');
/*!40000 ALTER TABLE `evenement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `examen`
--

DROP TABLE IF EXISTS `examen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examen` (
  `section` enum('groupe_1','groupe_2','groupe_3') DEFAULT NULL,
  `grade` enum('blanche','blanche_avec_une_ligne_jaune','blanche_avec_une_ligne_orange','blanche_avec_une_ligne_verte','jaune','orange','verte','verte_avec_une_barette','verte_avec_deux_barettes','rouge','rouge_avec_une_barette','rouge_avec_deux_barettes','noir_1_dan','noir_2_dan','noir_3_dan','noir_4_dan','noir_5_dan','noir_6_dan','noir_7_dan','noir_8_dan','noir_9_dan','noir_10_dan') DEFAULT NULL,
  `id_evenement` int(11) NOT NULL,
  `id_evenement_1` int(11) NOT NULL,
  PRIMARY KEY (`id_evenement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examen`
--

LOCK TABLES `examen` WRITE;
/*!40000 ALTER TABLE `examen` DISABLE KEYS */;
INSERT INTO `examen` VALUES (NULL,'verte_avec_une_barette',6,5),(NULL,'verte',7,5),(NULL,'rouge',13,12),(NULL,'blanche',16,0),(NULL,'blanche',17,0),(NULL,'blanche_avec_une_ligne_orange',18,12),(NULL,'verte',20,19);
/*!40000 ALTER TABLE `examen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extra`
--

DROP TABLE IF EXISTS `extra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extra` (
  `titre` varchar(150) DEFAULT NULL,
  `id_evenement` int(11) NOT NULL,
  PRIMARY KEY (`id_evenement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extra`
--

LOCK TABLES `extra` WRITE;
/*!40000 ALTER TABLE `extra` DISABLE KEYS */;
INSERT INTO `extra` VALUES ('un extra',2),('un autre extra',3);
/*!40000 ALTER TABLE `extra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lieu`
--

DROP TABLE IF EXISTS `lieu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lieu` (
  `id_lieu` int(11) NOT NULL AUTO_INCREMENT,
  `adresse` varchar(255) DEFAULT NULL,
  `cp` int(6) DEFAULT NULL,
  `ville` char(150) DEFAULT NULL,
  `pays` varchar(150) DEFAULT NULL,
  `residentielle` tinyint(1) DEFAULT '1',
  `salle` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_lieu`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lieu`
--

LOCK TABLES `lieu` WRITE;
/*!40000 ALTER TABLE `lieu` DISABLE KEYS */;
INSERT INTO `lieu` VALUES (1,'Avenue Charles-quint 48',22,'Wavre','Belgique',1,NULL),(2,'Avenue Charles-Quint, 48',1300,'Wavre [1300]','Martinique',1,NULL),(3,'Avenue Charles-Quint, 48',1300,'Wavre','Belgique',1,NULL),(6,'avenue charles-q',4444,'Wavre','Belgique',1,NULL),(7,'Avenue Charles-Quint, 48',1300,'Wavre','Martinique',1,NULL),(8,'avenue charles-quint 48',1300,'wavre','Belgique',1,NULL),(9,'avenue charles-quint 48',1300,'wavre','Belgique',1,NULL),(10,'4fsqdffdsfds',55,'dfgf','iuy',1,NULL),(11,'avenue charles-quint 48',14,'wavre','Belgique',1,NULL),(12,'Avenue Charles-quint 48',14,'wavre','Belgique',1,NULL),(13,'Avenue Charles-Quint, 48',15,'wavre','Belgique',1,NULL),(14,'Avenue Charles-Quint, 48',14,'Wavre','Belgique',1,NULL),(15,'avenue charles-quint 48',1300,'wavre','Belgique',1,NULL),(16,'Avenue Charles-Quint, 48',1300,'Wavre [1300]','Belgique',1,NULL),(17,'Avenue Charles-Quint, 48',1300,'Wavre [1300]','Belgique',1,NULL),(19,'avenue charles-quint 48',15,'wavre','Belgique',1,NULL),(20,'Avenue Charles-quint 48',8,'Wavre [1300]','Belgique',1,NULL),(21,'Avenue Charles-Quint, 48',1300,'Wavre [1300]','Belgique',1,NULL),(22,'Avenue Charles-Quint, 48',1300,'Wavre [1300]','Belgique',1,NULL),(23,'avenue charles-quint 48',1300,'wavre','Belgique',1,NULL),(24,'Avenue Charles-Quint, 48',1300,'Wavre [1300]','Belgique',1,NULL),(25,'Avenue Charles-Quint, 48',1300,'Wavre [1300]','Belgique',1,NULL),(27,'avenue charles-quint 48',1300,'wavre','Belgique',1,NULL),(28,'avenue charles-quint 48',19,'wavre','Belgique',1,NULL),(30,'Avenue Charles-Quint, 48',32,'wavre','Belgique',1,NULL),(33,'Avenue Charles-Quint, 48',0,'Wavre [1300]','Belgique',1,NULL),(34,'Avenue Charles-Quint, 48',8,'wavre','Belgique',1,NULL),(35,'Avenue Charles-Quint, 48',12,'wavre','Belgique',1,NULL),(39,'adresse',1300,'wavre','Belgique',1,NULL),(40,'qsdf',44,'qsdf','qsdf',1,NULL),(42,'qsdfqsdf',19,'wavre','Belgique',1,NULL),(44,'avenue charles-quint 48',4,'wavre','Belgique',1,NULL),(45,'avenue charles-quint 48',16,'wavre','Belgique',1,NULL),(46,'108 bis,  chemin de rodate,  quartien Tivoli',19,'wavre','Belgique',1,NULL),(47,'avenue charles-quint 48',3,'wavre','Belgique',1,NULL),(48,'Avenue Charles-quint 48',4,'wavre','Belgique',1,NULL),(50,'Avenue Charles-quint 48',1348,'wavre','Belgique',1,NULL),(52,'avenue charles-quint 48',1300,'wavre','Belgique',0,'salle 1'),(53,'Avenue Charles-Quint, 48',1300,'Wavre [1300]','Belgique',0,'salle 3'),(54,'avenue charles-quint 48',1300,'qsdf','sdf',1,NULL),(55,'avenue charles-quint 48',1300,'sqdf','sdf',0,'salle 2'),(56,'avenue charles-quint 48',1300,'wavre','Belgique',0,'salle 4'),(57,'108 bis,  chemin de rodate,  quartien Tivoli',97234,'fort-de-france','Martinique',0,'salle 7'),(62,'Avenue Charles-quint 48',1300,'Wavre','qsdf',0,'salle 5'),(63,'Avenue Charles-quint 48',1300,'wavre','Belgique',0,'salle 4'),(65,'Avenue Charles-Quint, 48',7,'wavre','Belgique',1,'salle 1'),(66,'Avenue Charles-Quint, 48',1300,'Wavre [1300]','Belgique',1,NULL),(67,'Avenue Charles-quint 48',1300,'Wavre','Belgique',1,NULL),(68,'avenue charles-quint 48',1300,'wavre','Belgique',0,'salle 4'),(69,'avenue charles-quint 48',1300,'wavre','qsdf',0,'salle 3'),(70,'avenue charles-quint 48',1300,'wavre','Belgique',0,'salle 4'),(71,'Avenue Charles-quint 48',10,'wavre','Martinique',0,'salle 3'),(76,'avenue charles-quint 48',2,'wavre','Belgique',1,'salle 3'),(77,'avenue charles-quint 48',3,'Wavre','Belgique',0,'salle 3');
/*!40000 ALTER TABLE `lieu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organiser`
--

DROP TABLE IF EXISTS `organiser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organiser` (
  `id_personne` int(11) NOT NULL,
  `id_evenement` int(11) NOT NULL,
  PRIMARY KEY (`id_personne`,`id_evenement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organiser`
--

LOCK TABLES `organiser` WRITE;
/*!40000 ALTER TABLE `organiser` DISABLE KEYS */;
INSERT INTO `organiser` VALUES (3,2),(3,3),(3,5),(3,6),(3,7),(3,12),(3,13),(3,19),(3,20),(3,21),(3,26),(3,27),(6,0),(6,7),(6,13),(6,15),(6,16),(6,18);
/*!40000 ALTER TABLE `organiser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participer`
--

DROP TABLE IF EXISTS `participer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participer` (
  `reussit` tinyint(1) DEFAULT NULL,
  `classement` int(11) DEFAULT NULL,
  `id_personne` int(11) NOT NULL,
  `id_evenement` int(11) NOT NULL,
  PRIMARY KEY (`id_personne`,`id_evenement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participer`
--

LOCK TABLES `participer` WRITE;
/*!40000 ALTER TABLE `participer` DISABLE KEYS */;
/*!40000 ALTER TABLE `participer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personne`
--

DROP TABLE IF EXISTS `personne`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personne` (
  `id_personne` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(250) NOT NULL,
  `mdp` varchar(35) NOT NULL,
  `taille` int(3) DEFAULT NULL,
  `poids` int(4) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `sexe` enum('feminin','masculin','autre') DEFAULT NULL,
  `staff` tinyint(1) DEFAULT NULL,
  `grade` enum('blanche','blanche_avec_une_ligne_jaune','blanche_avec_une_ligne_orange','blanche_avec_une_ligne_verte','jaune','orange','verte','verte_avec_une_barette','verte_avec_deux_barettes','rouge','rouge_avec_une_barette','rouge_avec_deux_barettes','noir_1_dan','noir_2_dan','noir_3_dan','noir_4_dan','noir_5_dan','noir_6_dan','noir_7_dan','noir_8_dan','noir_9_dan','noir_10_dan') DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `id_lieu` int(11) NOT NULL,
  PRIMARY KEY (`id_personne`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personne`
--

LOCK TABLES `personne` WRITE;
/*!40000 ALTER TABLE `personne` DISABLE KEYS */;
INSERT INTO `personne` VALUES (3,'admin','admin','admin',55,55,60,'masculin',1,'verte_avec_deux_barettes','admin@admin.com','485056262',6),(4,'user','user','user',44,44,43,'masculin',0,'noir_5_dan','user@user.com','485056262',7),(6,'admin 2','admin 2','admin',3,3,3,'feminin',1,'noir_3_dan','admin2@admin.com','06459856665',42);
/*!40000 ALTER TABLE `personne` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prix_competition`
--

DROP TABLE IF EXISTS `prix_competition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prix_competition` (
  `id_prix_competition` int(11) NOT NULL AUTO_INCREMENT,
  `prix` decimal(15,3) NOT NULL,
  `nb_categories` int(11) NOT NULL,
  `id_evenement` int(11) NOT NULL,
  PRIMARY KEY (`id_prix_competition`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prix_competition`
--

LOCK TABLES `prix_competition` WRITE;
/*!40000 ALTER TABLE `prix_competition` DISABLE KEYS */;
INSERT INTO `prix_competition` VALUES (33,44.400,1,21),(34,663.000,2,21),(35,66.000,1,27),(36,66.200,2,27);
/*!40000 ALTER TABLE `prix_competition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stage`
--

DROP TABLE IF EXISTS `stage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stage` (
  `cout` int(11) DEFAULT NULL,
  `id_evenement` int(11) NOT NULL,
  PRIMARY KEY (`id_evenement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stage`
--

LOCK TABLES `stage` WRITE;
/*!40000 ALTER TABLE `stage` DISABLE KEYS */;
INSERT INTO `stage` VALUES (29,15),(10,26);
/*!40000 ALTER TABLE `stage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stages_themes`
--

DROP TABLE IF EXISTS `stages_themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stages_themes` (
  `id_theme` int(11) NOT NULL,
  `id_evenement` int(11) NOT NULL,
  PRIMARY KEY (`id_theme`,`id_evenement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stages_themes`
--

LOCK TABLES `stages_themes` WRITE;
/*!40000 ALTER TABLE `stages_themes` DISABLE KEYS */;
INSERT INTO `stages_themes` VALUES (1,1),(2,1),(2,2),(2,15),(2,26),(3,2);
/*!40000 ALTER TABLE `stages_themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `theme`
--

DROP TABLE IF EXISTS `theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `theme` (
  `id_theme` int(11) NOT NULL AUTO_INCREMENT,
  `theme` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_theme`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `theme`
--

LOCK TABLES `theme` WRITE;
/*!40000 ALTER TABLE `theme` DISABLE KEYS */;
INSERT INTO `theme` VALUES (1,'theme a'),(2,'theme b'),(3,'theme c');
/*!40000 ALTER TABLE `theme` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-07 11:41:55
