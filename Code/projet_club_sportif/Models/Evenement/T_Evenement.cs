namespace projet_club_sportif.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public enum JOUR : int
    {
        [Display(Name = "Lundi")]
        lundi = 1,
        [Display(Name = "Mardi")]
        mardi,
        [Display(Name = "Mercredi")]
        marcredi,
        [Display(Name = "Jeudi")]
        jeudi,
        [Display(Name = "Vendredi")]
        vendredi,
        [Display(Name = "Samedi")]
        samedi,
        [Display(Name = "Dimanche")]
        dimanche
    }

    public class T_Evenement
    {
        public int pk_evenement { get; set; }

        [Display(Name = "Date de fin:")]
        [DataType(DataType.DateTime)]
        public DateTime dateDebut { get; set; }

        [Display(Name = "Date de d�but:")]
        [DataType(DataType.DateTime)]
        public DateTime dateFin { get; set; }

        [Display(Name = "Jour de la semaine pour l'�v�nement:")]
        public JOUR? jour { get; set; } = null;

        [Display(Name = "Description:")]
        [DataType(DataType.MultilineText)]
        public string description { get; set; }

        public int fk_lieu { get; set; }
    }
}
