﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Evenement
namespace projet_club_sportif.Models
{
    public class EvenementDAO : DAO<T_Evenement>
    {
        /// <summary>
        /// Ajoute l'objet Evenement dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Extra</returns>
        public override T_Evenement Create(T_Evenement newObject)
        {
            try
            {
                //On insert dans la table événement les données.
                connection.Open();
                string sqlText = "INSERT INTO evenement (debut, fin, id_lieu, jour, description) VALUES(@debut, @fin, @fk_lieu, @jour, @description);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@debut", newObject.dateDebut);
                cmd.Parameters.AddWithValue("@fin", newObject.dateFin);
                cmd.Parameters.AddWithValue("@fk_lieu", newObject.fk_lieu);
                cmd.Parameters.AddWithValue("@jour", newObject.jour);
                cmd.Parameters.AddWithValue("@description", newObject.description);

                cmd.ExecuteNonQuery();
                connection.Close();

                newObject.pk_evenement = (int) cmd.LastInsertedId;
                return newObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(T_Evenement actualObject)
        {
            try
            {
                //On supprime l'événement.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM evenement WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", actualObject.pk_evenement);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// N'a pas de sens. Non implémanté ici.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override T_Evenement Find(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Recherche dans la base de données l'évènement ayant l'ID donné. Retourne l'objet en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override T_Evenement Find(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, jour+0 as jour_as_int FROM evenement WHERE id_evenement = @id_evenement";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_evenement", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                T_Evenement myEvent = new T_Evenement();
                if (reader.HasRows)
                {
                    reader.Read();
                    
                    myEvent.dateDebut = (DateTime)reader["debut"];
                    myEvent.dateFin = (DateTime)reader["fin"];
                    myEvent.fk_lieu = (int)reader["id_lieu"];
                    myEvent.jour = reader["jour_as_int"] == DBNull.Value ? null : (JOUR?)Convert.ToInt32(reader["jour_as_int"]);
                    myEvent.description = reader["description"] == DBNull.Value ? null : (string)reader["description"];
                    myEvent.pk_evenement = id;
                }
                    
                connection.Close();
                return myEvent;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Met à jour la Base de données concernant l'objet passé en argument. Retourne l'objet mis à jour en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns></returns>
        public override T_Evenement Update(T_Evenement actualObject)
        {
            try
            {
                //On modifie les données dans événement.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE evenement SET debut = @debut, fin = @fin, id_lieu = @fk_lieu, jour = @jour, description = @description WHERE id_evenement = @id_evenement";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@debut", actualObject.dateDebut);
                cmd.Parameters.AddWithValue("@fin", actualObject.dateFin);
                cmd.Parameters.AddWithValue("@fk_lieu", actualObject.fk_lieu);
                cmd.Parameters.AddWithValue("@id_evenement", actualObject.pk_evenement);
                cmd.Parameters.AddWithValue("@jour", actualObject.jour);
                cmd.Parameters.AddWithValue("@description", actualObject.description);

                cmd.ExecuteNonQuery();
                connection.Close();
                return actualObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des évènements.
        /// </summary>
        /// <returns>List(Extra)</returns>
        public override List<T_Evenement> GetAll()
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, jour+0 as jour_as_int FROM evenement;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Evenement> rows = new List<T_Evenement>();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        T_Evenement myEvent = new T_Evenement();
                        myEvent.dateDebut = (DateTime)reader["debut"];
                        myEvent.dateFin = (DateTime)reader["fin"];
                        myEvent.fk_lieu = (int)reader["id_lieu"];
                        myEvent.pk_evenement = (int)reader["id_evenement"];
                        myEvent.jour = reader["jour_as_int"] == DBNull.Value ? null : (JOUR?)Convert.ToInt32(reader["jour_as_int"]);
                        myEvent.description = reader["description"] == DBNull.Value ? null : (string)reader["description"];

                        rows.Add(myEvent);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
    }
}