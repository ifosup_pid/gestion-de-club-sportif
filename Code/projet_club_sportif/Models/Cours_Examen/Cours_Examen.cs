﻿namespace projet_club_sportif.Models
{
    using System;
    using System.Collections.Generic;

    public class Cours_Examen
    {
        private CoursDAO coursDao;

        public Cours myCours;
        public List<Examen> myExamens;


        /// <summary>
        /// Constructeur du "Cours_Examen".
        /// </summary>
        public Cours_Examen()
        {
            coursDao = new CoursDAO();

            myCours = new Cours();
            myExamens = new List<Examen>();
            myExamens.Add(new Examen());
        }


        /// <summary>
        /// Permet de créer un cours et ses examens.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns></returns>
        public Cours_Examen Create(Cours_Examen newObject)
        {
            myCours = newObject.myCours;
            myExamens = newObject.myExamens;

            try
            {
                //partie cours
                myCours.Create(myCours);

                //partie examens
                foreach (Examen unExam in myExamens)
                {
                    unExam.fk_cours = myCours.fk_evenement;
                    unExam.Create(unExam);

                }

                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Permet de supprimer un cours et ses examens
        /// </summary>
        /// <returns>Boolean</returns>
        public bool Delete()
        {
            bool suppressionOk;

            try
            {
                //partie cours
                suppressionOk = myCours.Delete();
                
                //partie examens
                foreach (Examen unExam in myExamens)
                {
                    suppressionOk = suppressionOk && unExam.Delete();
                }
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }

            return suppressionOk;
        }


        public bool Update(Cours_Examen actualObject)
        {
            //update avec l'objet en paramettre de la fct
            try
            {
                //partie cours
                myCours.Update(actualObject.myCours);

                //partie examen
                Examen unExam = new Examen();
                foreach (Examen actualExam in actualObject.myExamens)
                {
                    if (actualExam.fk_evenement != 0)//exams a mettre à jour
                    {
                        unExam.Find(actualExam.fk_evenement);
                        unExam.Update(actualExam);
                    }
                    else//nouveaux exams
                    {
                        unExam.Create(actualExam);
                    }
                }
                foreach (Examen oldExam in myExamens)//exams à supprimer
                {
                    Examen examPresent = actualObject.myExamens.Find(model => model.fk_evenement == oldExam.fk_evenement);
                    if (examPresent == null)
                    {
                        oldExam.Delete();
                    }
                }

                    return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }


        public Cours_Examen FindByCours(int id)
        {
            try
            {
                //partie cours
                myCours = myCours.Find(id);

                //partie examen
                myExamens = new Examen().GetAllByCours(id);

                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public List<Cours_Examen> GetAll()
        {
            List<Cours_Examen> myCours_Examens = new List<Cours_Examen>();

            try
            {
                List<T_Cours> myT_Cours = coursDao.GetAll();

                myT_Cours.ForEach(unT_Cours =>
                                   {
                                       Cours_Examen unCoursExamen = new Cours_Examen();
                                       unCoursExamen.FindByCours(unT_Cours.pk_cours);

                                       myCours_Examens.Add(unCoursExamen);
                                   }
                                );

                return myCours_Examens;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
        
    }
}
