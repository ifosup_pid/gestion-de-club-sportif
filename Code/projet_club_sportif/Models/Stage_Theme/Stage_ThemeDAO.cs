﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Participer
namespace projet_club_sportif.Models
{
    public class Stage_ThemeDAO : DAO<T_Stage_Theme>
    {
        /// <summary>
        /// Ajoute l'objet Participer dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Extra</returns>
        public override T_Stage_Theme Create(T_Stage_Theme newObject)
        {
            try
            {
                //On insert dans la table extra les données.
                connection.Open();
                string sqlText = "INSERT INTO stages_themes (id_evenement, id_theme) VALUES(@id_evenement, @id_theme);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", newObject.pk_stage);
                cmd.Parameters.AddWithValue("@id_theme", newObject.pk_theme);

                cmd.ExecuteNonQuery();
                connection.Close();

                return newObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(T_Stage_Theme actualObject)
        {
            try
            {
                //On supprime le cours.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM stages_themes WHERE id_evenement = @id_evenement AND id_theme = @id_theme;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", actualObject.pk_stage);
                cmd.Parameters.AddWithValue("@id_theme", actualObject.pk_theme);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public bool DeleteAllForTheme(int id_pers)
        {
            try
            {
                //On supprime le cours.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM stages_themes WHERE id_theme = @id_theme;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_theme", id_pers);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public bool DeleteAllForStage(int id_event)
        {
            try
            {
                //On supprime le cours.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM stages_themes WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", id_event);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// N'a pas de sens dans ce cas-ci. N'est donc pas implémanté.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override T_Stage_Theme Find(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// N'est pas encore implémanté. Et n'a d'ailleurs pas beaucoup de sens.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override T_Stage_Theme Find(int id)
        {
            throw new NotImplementedException();
        }

        public override T_Stage_Theme Update(T_Stage_Theme actualObject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retourne l'ensemble des évènements auxquels la personne (ID) participe.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<T_Stage_Theme> GetAllForTheme(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM stages_themes WHERE id_theme = @id_theme;";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_theme", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Stage_Theme> rows = new List<T_Stage_Theme>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Stage_Theme myStageTheme = new T_Stage_Theme();
                        myStageTheme.pk_theme = id;
                        myStageTheme.pk_stage = (int)reader["id_evenement"];
                        rows.Add(myStageTheme);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne l'ensemble des évènements auxquels la personne (ID) participe.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<T_Stage_Theme> GetAllForStage(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM stages_themes WHERE id_evenement = @id_evenement;";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_evenement", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Stage_Theme> rows = new List<T_Stage_Theme>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Stage_Theme myStageTheme = new T_Stage_Theme();
                        myStageTheme.pk_theme = (int)reader["id_theme"];
                        myStageTheme.pk_stage = id;
                        rows.Add(myStageTheme);
                    }
                }
                connection.Close();

                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public override List<T_Stage_Theme> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}