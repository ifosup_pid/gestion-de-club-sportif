﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Cours
namespace projet_club_sportif.Models
{
    public class CoursDAO : DAO<T_Cours>
    {
        /// <summary>
        /// Ajoute l'objet Cours dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Extra</returns>
        public override T_Cours Create(T_Cours newObject)
        {
            try
            {
                //On insert dans la table extra les données.
                connection.Open();
                string sqlText = "INSERT INTO cours (section, id_evenement) VALUES(@section, @id_evenement);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@section", newObject.section);
                cmd.Parameters.AddWithValue("@id_evenement", newObject.pk_cours);

                cmd.ExecuteNonQuery();
                connection.Close();

                return newObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(T_Cours actualObject)
        {
            try
            {
                //On supprime le cours.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM cours WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", actualObject.pk_cours);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Recherche dans la base de données l'extra ayant la section donnée. Retourne l'objet en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Extra</returns>
        public override T_Cours Find(string name)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT id_evenement FROM cours WHERE section = @section;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@section", name);

                MySqlDataReader reader = cmd.ExecuteReader();

                reader.Read();
                int id = (int)reader["id_evenement"];
                connection.Close();

                T_Cours myLesson = Find(id);

                return myLesson;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Recherche dans la base de données l'Extra ayant l'ID donné. Retourne l'objet en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override T_Cours Find(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, section+0 AS section_as_int FROM cours WHERE id_evenement = @id_evenement";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_evenement", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                T_Cours myLesson = new T_Cours();

                if (reader.HasRows)
                {
                    reader.Read();
                    myLesson.section = (SECTION?)Convert.ToInt32(reader["section_as_int"]);
                    myLesson.pk_cours = (int)reader["id_evenement"];
                }

                connection.Close();
                return myLesson;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Met à jour la Base de données concernant l'objet passé en argument. Retourne l'objet mis à jour en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns></returns>
        public override T_Cours Update(T_Cours actualObject)
        {
            try
            {
                //On modifie les données dans Cours.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE cours SET section = @section WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@section", actualObject.section);
                cmd.Parameters.AddWithValue("@id_evenement", actualObject.pk_cours);

                cmd.ExecuteNonQuery();
                connection.Close();

                return actualObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des Cours.
        /// </summary>
        /// <returns>List(Extra)</returns>
        public override List<T_Cours> GetAll()
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, section+0 AS section_as_int FROM cours;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Cours> rows = new List<T_Cours>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Cours myLesson = new T_Cours();
                        myLesson.section = (SECTION?)Convert.ToInt32(reader["section_as_int"]);
                        myLesson.pk_cours = (int)reader["id_evenement"];

                        rows.Add(myLesson);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des cours auxquelles un étudiant participe.
        /// Retourne null s'il y a une erreur/problème.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<T_Cours></returns>
        public List<T_Cours> GetAllForIdMemberParticipating(int id_pers)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, section+0 AS section_as_int FROM cours INNER JOIN evenement ON evenement.id_evenement = cours.id_evenement "
                    + "INNER JOIN participer ON participer.id_evenement = evenement.id_evenement WHERE participer.id_personne = @id_personne";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_personne", id_pers);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Cours> rows = new List<T_Cours>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Cours myLesson = new T_Cours();
                        myLesson.section = (SECTION?)Convert.ToInt32(reader["section_as_int"]);
                        myLesson.pk_cours = (int)reader["id_evenement"];

                        rows.Add(myLesson);
                    }
                }
                connection.Close();
                return rows;              
            }
            catch(Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
            
        }

        //TODO
        /// <summary>
        /// Retourne la liste des cours auxquelles un étudiant participe.
        /// Retourne null s'il y a une erreur/problème.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<T_Cours></returns>
        public List<T_Cours> GetAllForIdMemberNotParticipating(int id_pers)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, section+0 AS section_as_int FROM cours WHERE id_evenement NOT IN " +
                    "(SELECT cours.id_evenement FROM cours " +
                    "INNER JOIN evenement ON evenement.id_evenement = cours.id_evenement " +
                    "INNER JOIN participer ON participer.id_evenement = evenement.id_evenement WHERE participer.id_personne = @id_personne)";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_personne", id_pers);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Cours> rows = new List<T_Cours>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Cours myLesson = new T_Cours();
                        myLesson.section = (SECTION?)Convert.ToInt32(reader["section_as_int"]);
                        myLesson.pk_cours = (int)reader["id_evenement"];

                        rows.Add(myLesson);
                    }
                }
                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }

        }

    }
}