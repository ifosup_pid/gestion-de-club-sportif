namespace projet_club_sportif.Models
{
    using System.ComponentModel.DataAnnotations;

    public class T_Cours
    {
        public int pk_cours { get; set; }

        [Display(Name = "Section:")]
        public SECTION? section { get; set; }
    }
}
