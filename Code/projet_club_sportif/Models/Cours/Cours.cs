﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace projet_club_sportif.Models
{
    public class Cours
    {
        private CoursDAO coursDao;
        private EvenementDAO evenementDao;
        private LieuDAO lieuDao;
        private ThemeDAO themeDao;
        private Cours_ThemeDAO coursThemeDao;
        private ParticiperDAO participerDao;
        private OrganiserDAO organiserDao;
        private PersonneDAO personneDao;

        public T_Cours myCours;
        public T_Evenement myEvent;
        public T_Lieu myLieu;

        [Display(Name = "Section:")]
        [Required(ErrorMessage = "Requis")]
        [EnumDataType(typeof(SECTION), ErrorMessage = "Valeur incorrecte")]
        public SECTION? section { get; set; }

        public int fk_evenement { get; set; }

        //attributs de l'évènement lié au cours
        [Display(Name = "Date et heure de début:")]
        [Required(ErrorMessage = "Requis")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = false)]
        public DateTime dateDebut { get; set; }

        [Display(Name = "Date et heure de fin:")]
        [Required(ErrorMessage = "Requis")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = false)]
        public DateTime dateFin { get; set; }

        [Display(Name = "Description:")]
        [DataType(DataType.MultilineText)]
        //mysql MEDIUMTEXT | worst case : 5,592,415 | best case : 16,777,215
        [StringLength(5592415, ErrorMessage = "La description doit faire moins de {1} charactères.")]
        public string description { get; set; }

        [Display(Name = "Jour de la semaine pour le cours:")]
        [Required(ErrorMessage = "Requis")]
        [EnumDataType(typeof(JOUR), ErrorMessage = "Valeur incorrecte")]
        public JOUR? jour { get; set; }


        //attributs du lieu lié au cours
        [Display(Name = "Salle:")]
        public string salle { get; set; }

        [Display(Name = "Adresse:")]
        public string adresse { get; set; }

        [Display(Name = "Code Postal:")]
        public int cp { get; set; }

        [Display(Name = "Ville:")]
        public string ville { get; set; }

        [Display(Name = "Pays:")]
        public string pays { get; set; }

        [Display(Name = "Sélectionner un emplacement:")]
        [Required(ErrorMessage = "Requis")]
        [Range(0, int.MaxValue, ErrorMessage = "Lieu non valide")]
        public int fk_lieu { get; set; }


        [Display(Name = "Thèmes")]
        [Required(ErrorMessage = "Requis")]
        public List<T_Theme> themes { get; set; }
        [Display(Name = "Thèmes")]
        [Required(ErrorMessage = "Requis")]
        public List<int> themesSelectedId { get; set; }


        //attributs des table de liasion entre evenement et personne
        [Display(Name = "Organisateurs du cours:")]
        [Required(ErrorMessage = "Requis")]
        public List<T_Personne> organisateurs { get; set; }
        [Display(Name = "Organisateurs du cours:")]
        [Required(ErrorMessage = "Requis")]
        public List<int> organisateursSelectedId { get; set; }

        [Display(Name = "Participants du cours:")]
        [Required(ErrorMessage = "Requis")]
        public List<T_Personne> participants { get; set; }
        [Display(Name = "Participants du cours:")]
        [Required(ErrorMessage = "Requis")]
        public List<int> participantsSelectedId { get; set; }

        /// <summary>
        /// Constructeur de la classe.
        /// </summary>
        public Cours()
        {
            coursDao = new CoursDAO();
            evenementDao = new EvenementDAO();
            lieuDao = new LieuDAO();
            themeDao = new ThemeDAO();
            coursThemeDao = new Cours_ThemeDAO();

            participerDao = new ParticiperDAO();
            organiserDao = new OrganiserDAO();
            personneDao = new PersonneDAO();

            myCours = new T_Cours();
            myEvent = new T_Evenement();
            myLieu = new T_Lieu();

            themes = themeDao.GetAll();
            themesSelectedId = new List<int>();
            organisateurs = personneDao.GetAllStaff();
            organisateursSelectedId = new List<int>();
            participants = new List<T_Personne>();
            participantsSelectedId = new List<int>();

            dateDebut = DateTime.Now;
            dateFin = DateTime.Now;
            jour = JOUR.lundi;
        }

        /// <summary>
        /// Permet de créer un cours et le retourne
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Cours</returns>
        public Cours Create(Cours newObject)
        {
            myEvent.dateDebut = newObject.dateDebut;
            myEvent.dateFin = newObject.dateFin;
            myEvent.description = newObject.description;
            myEvent.jour = newObject.jour;

            myCours.section = newObject.section;

            try
            {
                //partie cours
                fk_lieu = newObject.fk_lieu;
                myLieu = lieuDao.Find(fk_lieu);
                myEvent.fk_lieu = fk_lieu;

                evenementDao.Create(myEvent);
                fk_evenement = myEvent.pk_evenement;

                myCours.pk_cours = fk_evenement;
                coursDao.Create(myCours);

                //themes
                themesSelectedId = newObject.themesSelectedId;
                //creation des nouveaux liens
                T_Cours_Theme unCoursTheme;
                themesSelectedId.ForEach(delegate (int unThemeId)
                {
                    unCoursTheme = new T_Cours_Theme();
                    unCoursTheme.pk_theme = unThemeId;
                    unCoursTheme.pk_evenement = fk_evenement;
                    coursThemeDao.Create(unCoursTheme);
                });

                //organisateurs du cours
                organisateursSelectedId = newObject.organisateursSelectedId;
                //creation des nouveaux liens
                T_Personne un_organisateur;
                T_Organiser une_organisation;
                organisateursSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_organisateur = new T_Personne();
                    un_organisateur = personneDao.Find(unOrganisateurId);

                    une_organisation = new T_Organiser();
                    une_organisation.pk_evenement = fk_evenement;
                    une_organisation.pk_personne = unOrganisateurId;
                    organiserDao.Create(une_organisation);
                });

                //participants au cours
                participantsSelectedId = newObject.participantsSelectedId;
                //creation des nouveaux liens
                T_Personne un_participant;
                T_Participer une_participation;
                participantsSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_participant = new T_Personne();
                    un_participant = personneDao.Find(unOrganisateurId);

                    une_participation = new T_Participer();
                    une_participation.pk_evenement = fk_evenement;
                    une_participation.pk_personne = unOrganisateurId;
                    participerDao.Create(une_participation);
                });

                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Permet de supprimer un cours. reste à implémanter.
        /// </summary>
        /// <returns>?</returns>
        public bool Delete()
        {
            bool suppressionOk = true;

            try
            {
                //supprimer organisateurs du cours
                organiserDao.DeleteAllForEvent(fk_evenement);
                //supprimer participants au cours
                participerDao.DeleteAllForEvent(fk_evenement);

                //themes : supression des liens
                themes.Clear();//?
                coursThemeDao.DeleteAllForEvent(fk_evenement);

                //partie examen
                suppressionOk = (coursDao.Delete(myCours) &&
                                evenementDao.Delete(myEvent));
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }

            return suppressionOk;
        }

        /// <summary>
        /// Permet d'update un cours, le retourne à jour. retourne un boolean
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>boolean</returns>
        public bool Update(Cours actualObject)
        {
            // modif dans les sous-objets
            myCours.section = actualObject.section;

            myEvent.dateDebut = actualObject.dateDebut;
            myEvent.dateFin = actualObject.dateFin;
            myEvent.description = actualObject.description;
            myEvent.jour = actualObject.jour;

            //update avec l'objet en paramettre de la fct
            try
            {
                //partie cours
                myCours = coursDao.Update(myCours);
                myEvent.fk_lieu = actualObject.fk_lieu;
                myEvent = evenementDao.Update(myEvent);

                //suppression des anciens liens
                coursThemeDao.DeleteAllForEvent(fk_evenement);
                //themes
                themesSelectedId = actualObject.themesSelectedId;
                //creation des nouveaux liens
                T_Cours_Theme unCoursTheme;
                themesSelectedId.ForEach(delegate (int unThemeId)
                {
                    unCoursTheme = new T_Cours_Theme();
                    unCoursTheme.pk_theme = unThemeId;
                    unCoursTheme.pk_evenement = fk_evenement;
                    coursThemeDao.Create(unCoursTheme);
                });

                //suppression des anciens liens
                organiserDao.DeleteAllForEvent(fk_evenement);
                //organisateurs du cours
                organisateursSelectedId = actualObject.organisateursSelectedId;
                //creation des nouveaux liens
                T_Personne un_organisateur;
                T_Organiser une_organisation;
                organisateursSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_organisateur = new T_Personne();
                    un_organisateur = personneDao.Find(unOrganisateurId);

                    une_organisation = new T_Organiser();
                    une_organisation.pk_evenement = fk_evenement;
                    une_organisation.pk_personne = unOrganisateurId;
                    organiserDao.Create(une_organisation);
                });

                //suppression des anciens liens
                participerDao.DeleteAllForEvent(fk_evenement);
                //participants au cours
                participantsSelectedId = actualObject.participantsSelectedId;
                //creation des nouveaux liens
                T_Personne un_participant;
                T_Participer une_participation;
                participantsSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_participant = new T_Personne();
                    un_participant = personneDao.Find(unOrganisateurId);

                    une_participation = new T_Participer();
                    une_participation.pk_evenement = fk_evenement;
                    une_participation.pk_personne = unOrganisateurId;
                    participerDao.Create(une_participation);
                });

                // find car actualObject est incomplet : pas d'id ni sous-objets.
                Find(actualObject.fk_evenement);

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Permet de trouver un cours à partir de son ID et de le retourner sous forme d'objet.
        /// ! la fonction modifie aussi l'objet courant !
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Cours</returns>
        public Cours Find(int id)
        {
            try
            {
                //partie cours
                myCours = coursDao.Find(id);
                fk_evenement = id;
                section = myCours.section;

                myEvent = evenementDao.Find(id);
                dateDebut = myEvent.dateDebut;
                dateFin = myEvent.dateFin;
                fk_lieu = myEvent.fk_lieu;
                description = myEvent.description;
                jour = myEvent.jour;

                myLieu = lieuDao.Find(myEvent.fk_lieu);
                adresse = myLieu.adresse;
                ville = myLieu.ville;
                cp = myLieu.cp;
                pays = myLieu.pays;
                salle = myLieu.salle;

                //themes
                themesSelectedId.Clear();
                List<T_Cours_Theme> lesCoursThemes = coursThemeDao.GetAllForEvent(fk_evenement);
                lesCoursThemes.ForEach(delegate (T_Cours_Theme unCoursTheme)
                {
                    themesSelectedId.Add(unCoursTheme.pk_theme);
                });

                organisateursSelectedId.Clear();
                List<T_Organiser> lesOrganisations = organiserDao.GetAllForEvent(fk_evenement);
                lesOrganisations.ForEach(delegate (T_Organiser uneOrganisation)
                {
                    organisateursSelectedId.Add(uneOrganisation.pk_personne);
                });

                participantsSelectedId.Clear();
                List<T_Participer> lesParticipations = participerDao.GetAllForEvent(fk_evenement);
                lesParticipations.ForEach(delegate (T_Participer uneParticipation)
                {
                    participantsSelectedId.Add(uneParticipation.pk_personne);
                });

                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne l'ensemble 
        /// </summary>
        /// <returns></returns>
        public List<Cours> GetAll()
        {
            List<Cours> myCours = new List<Cours>();

            try
            {
                List<T_Cours> myT_Cours = coursDao.GetAll();

                myT_Cours.ForEach(unT_Cours =>
                {
                    Cours unCours = new Cours();
                    unCours.Find(unT_Cours.pk_cours);
                    myCours.Add(unCours);
                }
                                );

                return myCours;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }


        public List<Cours> GetAllForIdMemberParticipating(int id_pers)
        {
            List<Cours> myCours = new List<Cours>();

            try
            {
                List<T_Cours> myT_Cours = coursDao.GetAllForIdMemberParticipating(id_pers);

                myT_Cours.ForEach(unT_Cours =>
                {
                    Cours unCours = new Cours();
                    unCours.Find(unT_Cours.pk_cours);
                    myCours.Add(unCours);
                }
                                );

                return myCours;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public List<Cours> GetAllForIdMemberNotParticipating(int id_pers)
        {
            List<Cours> myCours = new List<Cours>();

            try
            {
                List<T_Cours> myT_Cours = coursDao.GetAllForIdMemberNotParticipating(id_pers);

                myT_Cours.ForEach(unT_Cours =>
                {
                    Cours unCours = new Cours();
                    unCours.Find(unT_Cours.pk_cours);
                    myCours.Add(unCours);
                }
                                );

                return myCours;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
    }
}