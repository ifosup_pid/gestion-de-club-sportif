namespace projet_club_sportif.Models
{
    using System.ComponentModel.DataAnnotations;

    public class T_Competition
    {
        public int fk_evenement { get; set; }

        [Display(Name = "Titre de la compétition:")]
        [StringLength(150, ErrorMessage = "La compétition doit faire moins de {1} charactères.")]
        public string titre { get; set; }

    }
}
