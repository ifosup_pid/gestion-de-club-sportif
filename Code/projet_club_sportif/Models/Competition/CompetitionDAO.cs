﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Competition
namespace projet_club_sportif.Models
{
    public class CompetitionDAO : DAO<T_Competition>
    {
        /// <summary>
        /// Ajoute l'objet Competition dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Extra</returns>
        public override T_Competition Create(T_Competition newObject)
        {
            try
            {
                //On insert dans la table événement les données.
                connection.Open();
                string sqlText = "INSERT INTO competition (titre, id_evenement) VALUES(@titre, @id_evenement);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@titre", newObject.titre);
                cmd.Parameters.AddWithValue("@id_evenement", newObject.fk_evenement);

                cmd.ExecuteNonQuery();
                connection.Close();

                return newObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(T_Competition actualObject)
        {
            try
            {
                //On supprime l'événement.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM competition WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", actualObject.fk_evenement);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Recherche dans la base de données l'évènement ayant le nom donné. Retourne l'objet en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override T_Competition Find(string name)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT id_evenement FROM competition WHERE titre = @titre;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@titre", name);

                MySqlDataReader reader = cmd.ExecuteReader();

                reader.Read();

                T_Competition myCompetition = new T_Competition();
                myCompetition.titre = name;
                myCompetition.fk_evenement = (int)reader["id_evenement"];
                connection.Close();

                return myCompetition;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Recherche dans la base de données l'évènement ayant l'ID donné. Retourne l'objet en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override T_Competition Find(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM competition WHERE id_evenement = @id_evenement";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_evenement", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                reader.Read();
                T_Competition myCompetition = new T_Competition();
                myCompetition.titre = (string)reader["titre"];
                myCompetition.fk_evenement = id;

                connection.Close();
                return myCompetition;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Met à jour la Base de données concernant l'objet passé en argument. Retourne l'objet mis à jour en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns></returns>
        public override T_Competition Update(T_Competition actualObject)
        {
            try
            {
                //On modifie les données dans événement.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE competition SET titre = @titre WHERE id_evenement = @id_evenement";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", actualObject.fk_evenement);
                cmd.Parameters.AddWithValue("@titre", actualObject.titre);

                cmd.ExecuteNonQuery();
                connection.Close();


                return actualObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des Compétitions.
        /// </summary>
        /// <returns>List(Extra)</returns>
        public override List<T_Competition> GetAll()
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM competition;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Competition> rows = new List<T_Competition>();

                while (reader.Read())
                {
                    T_Competition myCompetition = new T_Competition();
                    myCompetition.titre = (string)reader["titre"];
                    myCompetition.fk_evenement = (int)reader["id_evenement"];

                    rows.Add(myCompetition);
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return new List<T_Competition>();
            }
        }


        /// <summary>
        /// Retourne la liste des compétitions auxquelles un étudiant participe.
        /// Retourne null s'il y a une erreur/problème.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<Competition></returns>
        public List<T_Competition> GetAllForIdMemberParticipating(int id_pers)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM competition INNER JOIN evenement ON evenement.id_evenement = competition.id_evenement "
                    + "INNER JOIN participer ON participer.id_evenement = evenement.id_evenement WHERE participer.id_personne = @id_personne";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_personne", id_pers);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Competition> rows = new List<T_Competition>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Competition myCompetition = new T_Competition();
                        myCompetition.titre = (string)reader["titre"];
                        myCompetition.fk_evenement = (int)reader["id_evenement"];

                        rows.Add(myCompetition);
                    }
                }
                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des compétitions auxquelles un étudiant participe.
        /// Retourne null s'il y a une erreur/problème.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<Competition></returns>
        public List<T_Competition> GetAllForIdMemberNotParticipating(int id_pers)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT  * FROM competition WHERE id_evenement NOT IN " +
                    "(SELECT competition.id_evenement FROM competition " +
                    "INNER JOIN evenement ON evenement.id_evenement = competition.id_evenement " +
                    "INNER JOIN participer ON participer.id_evenement = evenement.id_evenement WHERE participer.id_personne = @id_personne)";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_personne", id_pers);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Competition> rows = new List<T_Competition>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Competition myCompetition = new T_Competition();
                        myCompetition.titre = (string)reader["titre"];
                        myCompetition.fk_evenement = (int)reader["id_evenement"];

                        rows.Add(myCompetition);
                    }
                }
                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
    }
}