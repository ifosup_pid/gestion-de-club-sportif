﻿namespace projet_club_sportif.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Competition
    {
        private CompetitionDAO competitionDao;
        private EvenementDAO evenementDao;
        private LieuDAO lieuDao;
        private CategorieDAO categorieDao;
        private Competition_CategorieDAO competion_categorieDao;
        private ParticiperDAO participerDao;
        private OrganiserDAO organiserDao;
        private PrixCompetitionDAO prixCompetitionDao;
        private PersonneDAO personneDao;

        public T_Lieu myLieuCompetition;
        public T_Evenement myEventCompetition;
        public T_Competition myCompetition;
        public List<T_PrixCompetition> listPrix;


        //attributs de competition
        public int fk_evenement { get; set; }

        [Display(Name = "Titre de la compétition:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(150, ErrorMessage = "La compétition doit faire moins de {1} charactères.")]
        public string titre { get; set; }

        //attributs de l'évènement lié à l'évènement
        [Display(Name = "Date et heure de début:")]
        [Required(ErrorMessage = "Requis")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = false)]
        public DateTime dateDebut { get; set; }

        [Display(Name = "Date et heure de fin:")]
        [Required(ErrorMessage = "Requis")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = false)]
        public DateTime dateFin { get; set; }

        [Display(Name = "Description:")]
        [DataType(DataType.MultilineText)]
        //mysql MEDIUMTEXT | worst case : 5,592,415 | best case : 16,777,215
        [StringLength(5592415, ErrorMessage = "La description doit faire moins de {1} charactères.")]
        public string description { get; set; }


        //attributs lié au lieu
        [Display(Name = "Salle:")]
        public string salle { get; set; }

        [Display(Name = "Adresse:")]
        public string adresse { get; set; }

        [Display(Name = "Code Postal:")]
        public int cp { get; set; }

        [Display(Name = "Ville:")]
        public string ville { get; set; }

        [Display(Name = "Pays:")]
        public string pays { get; set; }

        [Display(Name = "Sélectionner un emplacement:")]
        [Required(ErrorMessage = "Requis")]
        [Range(0, int.MaxValue, ErrorMessage = "Lieu non valide")]
        public int fk_lieu { get; set; }


        //attributs des table de liasion entre evenement et personne
        [Display(Name = "Organisateurs de la compétition:")]
        [Required(ErrorMessage = "Requis")]
        public List<T_Personne> organisateurs { get; set; }
        [Display(Name = "Organisateurs de la compétition:")]
        [Required(ErrorMessage = "Requis")]
        public List<int> organisateursSelectedId { get; set; }

        [Display(Name = "Participants à la compétition:")]
        [Required(ErrorMessage = "Requis")]
        public List<T_Personne> participants { get; set; }
        [Display(Name = "Participants à la compétition:")]
        [Required(ErrorMessage = "Requis")]
        public List<int> participantsSelectedId { get; set; }

        [Display(Name = "Catégories")]
        [Required(ErrorMessage = "Requis")]
        public List<int> categoriesSelectedId { get; set; }
        public List<T_Categorie> categories { get; set; }


        public Competition()
        {
            competitionDao = new CompetitionDAO();
            evenementDao = new EvenementDAO();
            lieuDao = new LieuDAO();
            categorieDao = new CategorieDAO();
            competion_categorieDao = new Competition_CategorieDAO();
            prixCompetitionDao = new PrixCompetitionDAO();

            participerDao = new ParticiperDAO();
            organiserDao = new OrganiserDAO();
            personneDao = new PersonneDAO();
            
            listPrix = new List<T_PrixCompetition>();
            T_PrixCompetition unPrix = new T_PrixCompetition();
            listPrix.Add(unPrix);

            myCompetition = new T_Competition();
            myEventCompetition = new T_Evenement();
            myLieuCompetition = new T_Lieu();

            categories = categorieDao.GetAll();
            categoriesSelectedId = new List<int>();
            organisateurs = personneDao.GetAllStaff();
            organisateursSelectedId = new List<int>();
            participants = new List<T_Personne>();
            participantsSelectedId = new List<int>();

            dateDebut = DateTime.Now;
            dateFin = DateTime.Now;
        }

        public Competition Create(Competition newObject)
        {
            myEventCompetition.dateDebut = newObject.dateDebut;
            myEventCompetition.dateFin = newObject.dateFin;
            myEventCompetition.description = newObject.description;

            myCompetition.titre = newObject.titre;

            try
            {
                fk_lieu = newObject.fk_lieu;
                myLieuCompetition = lieuDao.Find(fk_lieu);
                myEventCompetition.fk_lieu = fk_lieu;

                evenementDao.Create(myEventCompetition);
                fk_evenement = myEventCompetition.pk_evenement;

                myCompetition.fk_evenement = myEventCompetition.pk_evenement;
                competitionDao.Create(myCompetition);

                //listPrix
                listPrix = newObject.listPrix;
                listPrix.ForEach(delegate (T_PrixCompetition unPrix)
                {
                    unPrix.fk_evenement = fk_evenement;
                    prixCompetitionDao.Create(unPrix);

                });

                //categories
                categoriesSelectedId = newObject.categoriesSelectedId;
                //creation des nouveaux liens
                T_Competition_Categorie uneCompetition_categorie;
                categoriesSelectedId.ForEach(delegate (int unCategorieId)
                {
                    uneCompetition_categorie = new T_Competition_Categorie();
                    uneCompetition_categorie.pk_categorie = unCategorieId;
                    uneCompetition_categorie.pk_evenement = fk_evenement;
                    competion_categorieDao.Create(uneCompetition_categorie);
                });

                //organisateurs du stage
                organisateursSelectedId = newObject.organisateursSelectedId;
                //creation des nouveaux liens
                T_Personne un_organisateur;
                T_Organiser une_organisation;
                organisateursSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_organisateur = new T_Personne();
                    un_organisateur = personneDao.Find(unOrganisateurId);

                    une_organisation = new T_Organiser();
                    une_organisation.pk_evenement = fk_evenement;
                    une_organisation.pk_personne = unOrganisateurId;
                    organiserDao.Create(une_organisation);
                });

                //participants du stage
                participantsSelectedId = newObject.participantsSelectedId;
                //creation des nouveaux liens
                T_Personne un_participant;
                T_Participer une_participation;
                participantsSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_participant = new T_Personne();
                    un_participant = personneDao.Find(unOrganisateurId);

                    une_participation = new T_Participer();
                    une_participation.pk_evenement = fk_evenement;
                    une_participation.pk_personne = unOrganisateurId;
                    participerDao.Create(une_participation);
                });

                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public bool Delete()
        {
            bool suppressionOk;

            try
            {
                //supprimer organisateurs de la competition
                organiserDao.DeleteAllForEvent(fk_evenement);

                //supprimer participants à la competition
                participerDao.DeleteAllForEvent(fk_evenement);

                //categories
                competion_categorieDao.DeleteAllForEvent(fk_evenement);

                //(moche) listPrix
                listPrix.ForEach(delegate (T_PrixCompetition unTPrix)
                {
                    prixCompetitionDao.Delete(unTPrix);
                });

                suppressionOk = (competitionDao.Delete(myCompetition) &&
                                    evenementDao.Delete(myEventCompetition));
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }

            return suppressionOk;
        }

        public bool Update(Competition actualObject)
        {
            myCompetition.titre = actualObject.titre;

            myEventCompetition.dateDebut = actualObject.dateDebut;
            myEventCompetition.dateFin = actualObject.dateFin;
            myEventCompetition.description = actualObject.description;

            //update avec l'objet en paramettre de la fct
            try
            {
                //partie competition
                myCompetition = competitionDao.Update(myCompetition);
                myEventCompetition.fk_lieu = actualObject.fk_lieu;
                myEventCompetition = evenementDao.Update(myEventCompetition);

                //suppression des anciens liens
                competion_categorieDao.DeleteAllForEvent(fk_evenement);
                //categories
                categoriesSelectedId = actualObject.categoriesSelectedId;
                //creation des nouveaux liens
                T_Competition_Categorie unT_competition_categorie;
                categoriesSelectedId.ForEach(delegate (int intCategorie)
                {
                    unT_competition_categorie = new T_Competition_Categorie();
                    unT_competition_categorie.pk_evenement = fk_evenement;
                    unT_competition_categorie.pk_categorie = intCategorie;
                    competion_categorieDao.Create(unT_competition_categorie);
                });

                //suppression des anciens liens
                organiserDao.DeleteAllForEvent(fk_evenement);
                //organisateurs du stage
                organisateursSelectedId = actualObject.organisateursSelectedId;
                //creation des nouveaux liens
                T_Personne un_organisateur;
                T_Organiser une_organisation;
                organisateursSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_organisateur = new T_Personne();
                    un_organisateur = personneDao.Find(unOrganisateurId);

                    une_organisation = new T_Organiser();
                    une_organisation.pk_evenement = fk_evenement;
                    une_organisation.pk_personne = unOrganisateurId;
                    organiserDao.Create(une_organisation);
                });

                //suppression des anciens liens
                participerDao.DeleteAllForEvent(fk_evenement);
                //participants au stage
                participantsSelectedId = actualObject.participantsSelectedId;
                //creation des nouveaux liens
                T_Personne un_participant;
                T_Participer une_participation;
                participantsSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_participant = new T_Personne();
                    un_participant = personneDao.Find(unOrganisateurId);

                    une_participation = new T_Participer();
                    une_participation.pk_evenement = fk_evenement;
                    une_participation.pk_personne = unOrganisateurId;
                    participerDao.Create(une_participation);
                });

                //listPrix
                //suppression
                prixCompetitionDao.DeleteAllForCompetition(fk_evenement);
                //modification dans l'objet
                listPrix = actualObject.listPrix;
                //creation
                listPrix.ForEach(delegate (T_PrixCompetition unPrix)
                {
                    unPrix.fk_evenement = fk_evenement;
                    prixCompetitionDao.Create(unPrix);
                });

                // find car actualObject est incomplet : pas d'id ni sous-objets.
                Find(actualObject.fk_evenement);

                return true;

            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }


        public Competition Find(int id)
        {
            try
            {
                myCompetition = competitionDao.Find(id);

                fk_evenement = id;
                titre = myCompetition.titre;

                myEventCompetition = evenementDao.Find(id);
                dateDebut = myEventCompetition.dateDebut;
                dateFin = myEventCompetition.dateFin;
                fk_lieu = myEventCompetition.fk_lieu;
                description = myEventCompetition.description;

                myLieuCompetition = lieuDao.Find(myEventCompetition.fk_lieu);
                adresse = myLieuCompetition.adresse;
                ville = myLieuCompetition.ville;
                cp = myLieuCompetition.cp;
                pays = myLieuCompetition.pays;
                salle = myLieuCompetition.salle;

                //categories
                categoriesSelectedId.Clear();
                List<T_Competition_Categorie> lesCompetitions_categories = competion_categorieDao.GetAllForEvent(fk_evenement);
                lesCompetitions_categories.ForEach(delegate (T_Competition_Categorie uneCompetitionCategorie)
                {
                    categoriesSelectedId.Add(uneCompetitionCategorie.pk_categorie);
                });

                organisateursSelectedId.Clear();
                List<T_Organiser> lesOrganisations = organiserDao.GetAllForEvent(fk_evenement);
                lesOrganisations.ForEach(delegate (T_Organiser uneOrganisation)
                {
                    organisateursSelectedId.Add(uneOrganisation.pk_personne);
                });

                participantsSelectedId.Clear();
                List<T_Participer> lesParticipations = participerDao.GetAllForEvent(fk_evenement);
                lesParticipations.ForEach(delegate (T_Participer uneParticipation)
                {
                    participantsSelectedId.Add(uneParticipation.pk_personne);
                });

                //listPrix
                listPrix.Clear();
                listPrix = prixCompetitionDao.GetAllForCompetition(fk_evenement);

                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public List<Competition> GetAll()
        {
            List<Competition> myCompetitions = new List<Competition>();

            try
            {
                List<T_Competition> myT_Competitions = competitionDao.GetAll();

                myT_Competitions.ForEach(unT_competition =>
                {
                    Competition unCompetition = new Competition();
                    unCompetition.Find(unT_competition.fk_evenement);
                    myCompetitions.Add(unCompetition);
                });

                return myCompetitions;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        
        public List<Competition> GetAllForIdMemberParticipating(int id_pers)
        {
            List<Competition> myCompetitions = new List<Competition>();

            try
            {
                List<T_Competition> myT_Competitions = competitionDao.GetAllForIdMemberParticipating(id_pers);

                myT_Competitions.ForEach(unT_competition =>
                {
                    Competition unCompetition = new Competition();
                    unCompetition.Find(unT_competition.fk_evenement);
                    myCompetitions.Add(unCompetition);
                });

                return myCompetitions;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        //TODO
        /// <summary>
        /// Retourne la liste des competitions auxquelles un étudiant ne participe pas.
        /// Retourne null s'il y a une erreur/problème.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<Extra></returns>
        public List<Competition> GetAllForIdMemberNotParticipating(int id_pers)
        {
            List<Competition> myCompetitions = new List<Competition>();

            try
            {
                List<T_Competition> myT_Competitions = competitionDao.GetAllForIdMemberNotParticipating(id_pers);

                myT_Competitions.ForEach(unT_competition =>
                {
                    Competition unCompetition = new Competition();
                    unCompetition.Find(unT_competition.fk_evenement);
                    myCompetitions.Add(unCompetition);
                });

                return myCompetitions;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
    }
}