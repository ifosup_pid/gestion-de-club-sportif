﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.IO;

namespace projet_club_sportif.Models
{

    public class creationPdf
    {
        private MemoryStream ms = new MemoryStream();
        private Document nouveauDocument = new Document();

        public MemoryStream Ms
        {
            get
            {
                return ms;
            }

            set
            {
                ms = value;
            }
        }

        public void créationPDF_2(string nom, string prénom)
        {
            //byte[] bPDF = null;


            try
            {
                PdfWriter.GetInstance(nouveauDocument, Ms);

                nouveauDocument.Open();
                Paragraph titre = new Paragraph(nom + " " + prénom);

                titre.Alignment = Element.ALIGN_CENTER;
                titre.Leading = 15; // création espace entre les paragraphes
                nouveauDocument.Add(titre);
                espace();
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
            }


        }

        public byte[] affichePDF()
        {
            try
            {
                nouveauDocument.Close();
                return Ms.ToArray();
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }

        }

        private void espace()
        {
            try
            {
                Paragraph espace = new Paragraph(" ");
                espace.Alignment = Element.ALIGN_CENTER;
                espace.Leading = 15;
                nouveauDocument.Add(espace);
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
  
            }
        }

        public void tableau(string[] tab, string choix)
        {
            try
            {

                espace();
                Paragraph paragraphTable1 = new Paragraph();
                paragraphTable1.SpacingAfter = 15f;





                switch (choix)
                {
                    case "examen":
                        PdfPTable tableau_examen = new PdfPTable(4);

                        PdfPCell cellule_examen = new PdfPCell(new Paragraph(choix));
                        cellule_examen.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellule_examen.Colspan = tab.Length + 4; // le + 4 pour les grades/Date/status
                        tableau_examen.AddCell(cellule_examen);

                        tableau_examen.AddCell("grade");
                        tableau_examen.AddCell("Date de début");
                        tableau_examen.AddCell("Date de fin");
                        tableau_examen.AddCell("Statut");

                        foreach (string donnée in tab)
                        {
                            tableau_examen.AddCell(donnée);
                        }

                        tableau_examen.HorizontalAlignment = Element.ALIGN_MIDDLE;
                        paragraphTable1.Add(tableau_examen);

                        break;
                    case "cours":
                        PdfPTable tableau_cours = new PdfPTable(3);

                        PdfPCell cellule_cours = new PdfPCell(new Paragraph(choix));
                        cellule_cours.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellule_cours.Colspan = tab.Length + 3;
                        tableau_cours.AddCell(cellule_cours);

                        tableau_cours.AddCell("Section");
                        tableau_cours.AddCell("Date du début");
                        tableau_cours.AddCell("Date de fin");

                        foreach (string donnée in tab)
                        {
                            tableau_cours.AddCell(donnée);
                        }

                        tableau_cours.HorizontalAlignment = Element.ALIGN_MIDDLE;
                        paragraphTable1.Add(tableau_cours);

                        break;
                    case "competition":
                        PdfPTable tableau_competition = new PdfPTable(3);

                        PdfPCell cellule_compétiton = new PdfPCell(new Paragraph(choix));
                        cellule_compétiton.HorizontalAlignment = Element.ALIGN_CENTER;
                        cellule_compétiton.Colspan = tab.Length + 3;
                        tableau_competition.AddCell(cellule_compétiton);

                        tableau_competition.AddCell("Titre");
                        tableau_competition.AddCell("Date de début");
                        tableau_competition.AddCell("Date de Fin");


                        foreach (string donnée in tab)
                        {
                            tableau_competition.AddCell(donnée);
                        }

                        tableau_competition.HorizontalAlignment = Element.ALIGN_MIDDLE;
                        paragraphTable1.Add(tableau_competition);

                        break;
                    default:
                        break;
                }



                nouveauDocument.Add(paragraphTable1);
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
            }


        }

        public void paragraphe(string donnée)
        {
            try
            {
                Paragraph paragraphe = new Paragraph(donnée);
                paragraphe.Alignment = Element.ALIGN_LEFT;
                paragraphe.Leading = 15;
                nouveauDocument.Add(paragraphe);
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
            }
        }
    }
}