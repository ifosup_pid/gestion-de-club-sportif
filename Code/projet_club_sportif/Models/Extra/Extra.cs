﻿namespace projet_club_sportif.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Extra
    {
        private ExtraDAO extraDao;
        private EvenementDAO evenementDao;
        private LieuDAO lieuDao;
        private ParticiperDAO participerDao;
        private OrganiserDAO organiserDao;
        private PersonneDAO personneDao;

        public T_Extra myExtra;
        public T_Evenement myEvent;
        public T_Lieu myLieu;

        [Display(Name = "Nom:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(150, ErrorMessage = "Le titre doit faire moins de {1} charactères.")]
        public string titre { get; set; }

        public int fk_evenement { get; set; }

        [Display(Name = "Date et heure de début:")]
        [Required(ErrorMessage = "Requis")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = false)]
        public DateTime dateDebut { get; set; }

        [Display(Name = "Date et heure de fin:")]
        [Required(ErrorMessage = "Requis")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = false)]
        public DateTime dateFin { get; set; }

        [Display(Name = "Description:")]
        [DataType(DataType.MultilineText)]
        //mysql MEDIUMTEXT | worst case : 5,592,415 | best case : 16,777,215
        [StringLength(5592415, ErrorMessage = "La description doit faire moins de {1} charactères.")]
        public string description { get; set; }


        [Display(Name = "Salle:")]
        public string salle { get; set; }

        [Display(Name = "Adresse:")]
        public string adresse { get; set; }

        [Display(Name = "Code Postal:")]
        public int cp { get; set; }

        [Display(Name = "Ville:")]
        public string ville { get; set; }

        [Display(Name = "Pays:")]
        public string pays { get; set; }

        [Display(Name = "Sélectionner un emplacement:")]
        [Required(ErrorMessage = "Requis")]
        [Range(0, int.MaxValue, ErrorMessage = "Lieu non valide")]
        public int fk_lieu { get; set; }


        [Display(Name = "Organisateurs:")]
        [Required(ErrorMessage = "Requis")]
        public List<T_Personne> organisateurs { get; set; }
        [Display(Name = "Organisateurs:")]
        [Required(ErrorMessage = "Requis")]
        public List<int> organisateursSelectedId { get; set; }

        [Display(Name = "Participants:")]
        [Required(ErrorMessage = "Requis")]
        public List<T_Personne> participants { get; set; }
        [Display(Name = "Participants:")]
        [Required(ErrorMessage = "Requis")]
        public List<int> participantsSelectedId { get; set; }

        //constructeur
        public Extra()
        {
            extraDao = new ExtraDAO();
            evenementDao = new EvenementDAO();
            lieuDao = new LieuDAO();

            organiserDao = new OrganiserDAO();
            participerDao = new ParticiperDAO();
            personneDao = new PersonneDAO();

            myExtra = new T_Extra();
            myEvent = new T_Evenement();
            myLieu = new T_Lieu();

            organisateurs = personneDao.GetAllStaff();
            organisateursSelectedId = new List<int>();
            participants = new List<T_Personne>();
            participantsSelectedId = new List<int>();

            dateDebut = DateTime.Now;
            dateFin = DateTime.Now;
        }
        

        public Extra Create(Extra newObject)
        {
            myEvent.dateDebut = newObject.dateDebut;
            myEvent.dateFin = newObject.dateFin;
            myEvent.description = newObject.description;

            myExtra.titre = newObject.titre;

            try
            {
                fk_lieu = newObject.fk_lieu;
                myLieu = lieuDao.Find(fk_lieu);
                myEvent.fk_lieu = fk_lieu;

                evenementDao.Create(myEvent);
                fk_evenement = myEvent.pk_evenement;

                myExtra.fk_evenement = myEvent.pk_evenement;
                extraDao.Create(myExtra);

                //organisateurs de l'extra
                organisateursSelectedId = newObject.organisateursSelectedId;
                //creation des nouveaux liens
                T_Personne un_organisateur;
                T_Organiser une_organisation;
                organisateursSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_organisateur = new T_Personne();
                    un_organisateur = personneDao.Find(unOrganisateurId);

                    une_organisation = new T_Organiser();
                    une_organisation.pk_evenement = fk_evenement;
                    une_organisation.pk_personne = unOrganisateurId;
                    organiserDao.Create(une_organisation);
                });

                //participants à l'extra
                participantsSelectedId = newObject.participantsSelectedId;
                //creation des nouveaux liens
                T_Personne un_participant;
                T_Participer une_participation;
                participantsSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_participant = new T_Personne();
                    un_participant = personneDao.Find(unOrganisateurId);

                    une_participation = new T_Participer();
                    une_participation.pk_evenement = fk_evenement;
                    une_participation.pk_personne = unOrganisateurId;
                    participerDao.Create(une_participation);
                });

                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public bool Delete()
        {
            //TODO
            bool suppressionOk = true;

            try
            {
                //supprimer organisateurs de l'extra
                organiserDao.DeleteAllForEvent(fk_evenement);

                //supprimer participants à l'examen
                participerDao.DeleteAllForEvent(fk_evenement);

                suppressionOk = (extraDao.Delete(myExtra) &&
                                evenementDao.Delete(myEvent));
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }

            return suppressionOk;
        }

        public bool Update(Extra actualObject)
        {
            // modif dans les sous-objets

            myExtra.titre = actualObject.titre;

            myEvent.dateDebut = actualObject.dateDebut;
            myEvent.dateFin = actualObject.dateFin;
            myEvent.description = actualObject.description;

            //update avec l'objet en paramettre de la fct
            try
            {
                myExtra = extraDao.Update(myExtra);
                myEvent.fk_lieu = actualObject.fk_lieu;
                myEvent = evenementDao.Update(myEvent);

                //suppression des anciens liens
                organiserDao.DeleteAllForEvent(fk_evenement);
                organisateursSelectedId.Clear();
                //organisateurs de l'extra
                organisateursSelectedId = actualObject.organisateursSelectedId;
                //creation des nouveaux liens
                T_Personne un_organisateur;
                T_Organiser une_organisation;
                organisateursSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_organisateur = new T_Personne();
                    un_organisateur = personneDao.Find(unOrganisateurId);

                    une_organisation = new T_Organiser();
                    une_organisation.pk_evenement = fk_evenement;
                    une_organisation.pk_personne = unOrganisateurId;
                    organiserDao.Create(une_organisation);
                });

                //suppression des anciens liens
                participerDao.DeleteAllForEvent(fk_evenement);
                participantsSelectedId.Clear();
                //participants à l'extra
                participantsSelectedId = actualObject.participantsSelectedId;
                //creation des nouveaux liens
                T_Personne un_participant;
                T_Participer une_participation;
                participantsSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_participant = new T_Personne();
                    un_participant = personneDao.Find(unOrganisateurId);

                    une_participation = new T_Participer();
                    une_participation.pk_evenement = fk_evenement;
                    une_participation.pk_personne = unOrganisateurId;
                    participerDao.Create(une_participation);
                });

                // find car actualObject est incomplet : pas d'id ni sous-objets.
                Find(actualObject.fk_evenement);

                return true;

            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }
        

        public Extra Find(int id)
        {
            try
            {
                myExtra = extraDao.Find(id);
                titre = myExtra.titre;
                fk_evenement = myExtra.fk_evenement;

                myEvent = evenementDao.Find(id);
                dateDebut = myEvent.dateDebut;
                dateFin = myEvent.dateFin;   
                fk_lieu = myEvent.fk_lieu;
                description = myEvent.description;

                myLieu = lieuDao.Find(myEvent.fk_lieu);
                adresse = myLieu.adresse;
                ville = myLieu.ville;
                cp = myLieu.cp;
                pays = myLieu.pays;
                salle = myLieu.salle;

                organisateursSelectedId.Clear();
                List<T_Organiser> lesOrganisations = organiserDao.GetAllForEvent(fk_evenement);
                lesOrganisations.ForEach(delegate (T_Organiser uneOrganisation)
                {
                    organisateursSelectedId.Add(uneOrganisation.pk_personne);
                });

                participantsSelectedId.Clear();
                List<T_Participer> lesParticipations = participerDao.GetAllForEvent(fk_evenement);
                lesParticipations.ForEach(delegate (T_Participer uneParticipation)
                {
                    participantsSelectedId.Add(uneParticipation.pk_personne);
                });

                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public List<Extra> GetAll()
        {
            List<Extra> myExtras = new List<Extra>();

            try
            {
                List<T_Extra> myT_Extras = extraDao.GetAll();

                myT_Extras.ForEach(unT_extra =>
                {
                    Extra unExtra = new Extra();
                    unExtra.Find(unT_extra.fk_evenement);
                    myExtras.Add(unExtra);
                }
                                );

                return myExtras;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public List<Extra> GetAllForIdMemberParticipating(int id_pers)
        {
            List<Extra> myExtras = new List<Extra>();

            try
            {
                List<T_Extra> myT_Extras = extraDao.GetAllForIdMemberParticipating(id_pers);

                myT_Extras.ForEach(unT_extra =>
                {
                    Extra unExtra = new Extra();
                    unExtra.Find(unT_extra.fk_evenement);
                    myExtras.Add(unExtra);
                }
                                );

                return myExtras;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public List<Extra> GetAllForIdMemberNotParticipating(int id_pers)
        {
            List<Extra> myExtras = new List<Extra>();

            try
            {
                List<T_Extra> myT_Extras = extraDao.GetAllForIdMemberNotParticipating(id_pers);

                myT_Extras.ForEach(unT_extra =>
                {
                    Extra unExtra = new Extra();
                    unExtra.Find(unT_extra.fk_evenement);
                    myExtras.Add(unExtra);
                }
                                );

                return myExtras;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
    }
}