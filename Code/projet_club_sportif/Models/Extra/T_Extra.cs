namespace projet_club_sportif.Models
{
    using System.ComponentModel.DataAnnotations;

    public class T_Extra
    {
        //public T_Evenement myEvent = new T_Evenement();

        [Required(ErrorMessage = "SVP entrez un titre pour votre évènement")]
        [StringLength(150, ErrorMessage = "Le titre doit faire moins de {1} charactères.")]
        [Display(Name = "Nom:")]
        public string titre { get; set; }

        public int fk_evenement { get; set; }
	}
}
