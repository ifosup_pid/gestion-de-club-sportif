﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Extra
namespace projet_club_sportif.Models
{
    public class ExtraDAO : DAO<T_Extra>
    {
        /// <summary>
        /// Ajoute l'objet Extra dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Extra</returns>
        public override T_Extra Create(T_Extra newObject)
        {
            try
            {
                //On insert dans la table extra les données.
                connection.Open();
                string sqlText = "INSERT INTO extra (titre, id_evenement) VALUES(@titre, @id_evenement);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@titre", newObject.titre);
                cmd.Parameters.AddWithValue("@id_evenement", newObject.fk_evenement);

                cmd.ExecuteNonQuery();
                connection.Close();

                newObject.fk_evenement = (int)cmd.LastInsertedId;
                return newObject;
            }
            catch(Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(T_Extra actualObject)
        {
            try
            {
                //On supprime l'extra.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM extra WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", actualObject.fk_evenement);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Recherche dans la base de données l'Extra ayant l'ID donné. Retourne l'objet en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override T_Extra Find(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM extra WHERE id_evenement = @id_evenement";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_evenement", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                reader.Read();
                T_Extra myExtra = new T_Extra();
                myExtra.titre = (string)reader["titre"];
                myExtra.fk_evenement = (int)reader["id_evenement"];

                connection.Close();
                return myExtra;
            }
            catch(Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Recherche dans la base de données l'extra ayant le titre donné. Retourne l'objet en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Extra</returns>
        public override T_Extra Find(string name)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT id_evenement FROM extra WHERE titre = @titre;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@titre", name);

                MySqlDataReader reader = cmd.ExecuteReader();

                reader.Read();
                int id = (int)reader["id_evenement"];
                connection.Close();

                T_Extra myExtra = Find(id);

                return myExtra;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des Extras.
        /// </summary>
        /// <returns>List(Extra)</returns>
        public override List<T_Extra> GetAll()
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM extra;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Extra> rows = new List<T_Extra>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Extra myExtra = new T_Extra();
                        myExtra.titre = (string)reader["titre"];
                        myExtra.fk_evenement = (int)reader["id_evenement"];

                        rows.Add(myExtra);
                    }
                } 

                connection.Close();
                return rows;
            }
            catch(Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Met à jour la Base de données concernant l'objet passé en argument. Retourne l'objet mis à jour en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns></returns>
        public override T_Extra Update(T_Extra actualObject)
        {
            try
            {
                //On modifie les données dans Extra.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE extra SET titre = @titre WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@titre", actualObject.titre);
                cmd.Parameters.AddWithValue("@id_evenement", actualObject.fk_evenement);

                cmd.ExecuteNonQuery();
                connection.Close();

                return actualObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des extra auxquelles un étudiant participe.
        /// Retourne null s'il y a une erreur/problème.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<Extra></returns>
        public List<T_Extra> GetAllForIdMemberParticipating(int id_pers)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM extra INNER JOIN evenement ON evenement.id_evenement = extra.id_evenement "
                    + "INNER JOIN participer ON participer.id_evenement = evenement.id_evenement WHERE participer.id_personne = @id_personne";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_personne", id_pers);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Extra> rows = new List<T_Extra>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Extra myExtra = new T_Extra();
                        myExtra.titre = (string)reader["titre"];
                        myExtra.fk_evenement = (int)reader["id_evenement"];

                        rows.Add(myExtra);
                    }
                }
                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        //TODO
        /// <summary>
        /// Retourne la liste des extra auxquelles un étudiant ne participe pas.
        /// Retourne null s'il y a une erreur/problème.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<Extra></returns>
        public List<T_Extra> GetAllForIdMemberNotParticipating(int id_pers)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT  * FROM extra WHERE id_evenement NOT IN "+
                    "(SELECT extra.id_evenement FROM extra "+
                    "INNER JOIN evenement ON evenement.id_evenement = extra.id_evenement "+
                    "INNER JOIN participer ON participer.id_evenement = evenement.id_evenement WHERE participer.id_personne = @id_personne)";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_personne", id_pers);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Extra> rows = new List<T_Extra>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Extra myExtra = new T_Extra();
                        myExtra.titre = (string)reader["titre"];
                        myExtra.fk_evenement = (int)reader["id_evenement"];

                        rows.Add(myExtra);
                    }
                }
                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
    }
}