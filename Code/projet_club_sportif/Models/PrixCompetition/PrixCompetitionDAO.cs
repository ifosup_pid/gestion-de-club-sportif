﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Implémentation de l'abstract DAO dans le cadre de la classe métier PrixCompetition
namespace projet_club_sportif.Models
{
    public class PrixCompetitionDAO : DAO<T_PrixCompetition>
    {
        /// <summary>
        /// Ajoute l'objet PrixComptetion dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Extra</returns>
        public override T_PrixCompetition Create(T_PrixCompetition newObject)
        {
            try
            {
                //On insert dans la table les données.
                connection.Open();
                string sqlText = "INSERT INTO prix_competition (prix, nb_categories, id_evenement) VALUES(@prix, @nb_categories, @id_evenement);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@prix", newObject.prix);
                cmd.Parameters.AddWithValue("@nb_categories", newObject.nbCategories);
                cmd.Parameters.AddWithValue("@id_evenement", newObject.fk_evenement);

                cmd.ExecuteNonQuery();
                connection.Close();

                newObject.fk_evenement = (int)cmd.LastInsertedId;

                return newObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(T_PrixCompetition actualObject)
        {
            try
            {
                //On supprime l'extra.
                connection.Open();
                string sqlText = "DELETE FROM prix_competition WHERE id_prix_competition = @id_prix_competition";

                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_prix_competition", actualObject.pk_prixCompetition);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public bool DeleteAllForCompetition(int id_competition)
        {
            try
            {
                //On supprime l'extra.
                connection.Open();
                string sqlText = "DELETE FROM prix_competition WHERE id_evenement = @id_evenement";

                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", id_competition);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// N'a pas d'intérêt dans ce cas-ci. N'est pas implémanté.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Extra</returns>
        public override T_PrixCompetition Find(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Recherche dans la base de données l'Extra ayant l'ID donné. Retourne l'objet en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override T_PrixCompetition Find(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM prix_competition WHERE id_prix_competition = @id_prix_competition";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue ("@id_prix_competition", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                reader.Read();
                T_PrixCompetition myPrice = new T_PrixCompetition();
                myPrice.pk_prixCompetition = id;
                myPrice.prix = (decimal)reader["prix"];
                myPrice.nbCategories = (int)reader["nb_categories"];
                myPrice.fk_evenement = (int)reader["id_evenement"];

                connection.Close();
                return myPrice;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }


        /// <summary>
        /// Met à jour la Base de données concernant l'objet passé en argument. Retourne l'objet mis à jour en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns></returns>
        public override T_PrixCompetition Update(T_PrixCompetition actualObject)
        {
            try
            {
                //On modifie les données dans Prix_Categorie.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE prix_competition SET prix = @prix, nb_categories = @nb_categories WHERE id_prix_competition = @id_prix_competition;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@prix", actualObject.prix);
                cmd.Parameters.AddWithValue("@nb_categories", actualObject.nbCategories);
                cmd.Parameters.AddWithValue("@id_prix_competition", actualObject.pk_prixCompetition);

                cmd.ExecuteNonQuery();
                connection.Close();

                return actualObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Pas d'intérêt ici. Non implémanté.
        /// </summary>
        /// <returns></returns>
        public override List<T_PrixCompetition> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Pas d'intérêt ici. Non implémanté.
        /// </summary>
        /// <returns></returns>
        public List<T_PrixCompetition> GetAllForCompetition(int id_competition)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM prix_competition WHERE id_evenement = @id_evenement";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_evenement", id_competition);

                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_PrixCompetition> rows = new List<T_PrixCompetition>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_PrixCompetition myPrice = new T_PrixCompetition();
                        myPrice.fk_evenement = id_competition;
                        myPrice.prix = (decimal)reader["prix"];
                        myPrice.nbCategories = (int)reader["nb_categories"];
                        myPrice.pk_prixCompetition = (int)reader["id_prix_competition"];
                        rows.Add(myPrice);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
    }
}