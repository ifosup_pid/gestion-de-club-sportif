namespace projet_club_sportif.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class T_PrixCompetition
    {
        public int pk_prixCompetition { get; set; }

        [Display(Name = "Prix:")]
        //[Range(0.00, 9999.99, ErrorMessage = "le prix doit �tre compris entre {1} et {2}.")]//pas de valeur n�gative
        [RegularExpression(@"^\d+\,\d{0,2}$", ErrorMessage = "prix invalide. n�cessite un chiffre � 2 d�cimale max. eg: 99,99")]
        public decimal prix { get; set; }

        [Display(Name = "Nombre de cat�gories:")]
        [Range(0, 100)]//pas de valeur n�gative
        public int nbCategories { get; set; } = 1;

        public int fk_evenement { get; set; }
    }
}
