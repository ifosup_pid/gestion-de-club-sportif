﻿using System;
using System.IO;

namespace projet_club_sportif.Models
{
    class gcspFileLog
    {
        string nomFichier;

        public string NomFichier
        {
            get
            {
                return nomFichier;
            }

            set
            {
                nomFichier = value;
            }
        }

        private StreamWriter creationFichier()
        {
            //Création d'un fichier vide.
            StreamWriter sw = null;
            //if(!Directory.Exists("../erreur"))
            //{
            //    Directory.CreateDirectory("erreur");
            //}
            string chemin = "\\erreur\\" + DateTime.Now.ToString("yy") + "\\" + DateTime.Now.ToString("MMMM");
            // n'écrase pas le fichier si il existe 
            Directory.CreateDirectory(chemin);
            // chemin acces 
            NomFichier = (chemin + "\\" + DateTime.Now.ToString("dd") + ".txt");
            return sw = new StreamWriter(NomFichier, true); // true permet de lire a la suite grâce à appends


        }
        private void fermetureEcriture(StreamWriter sw)
        {
            // Fermeture streamwriter 
            if (sw != null)
            {
                sw.Close();
                sw = null;
            }
        }
        public void ecrire(String message)
        {

            StreamWriter sw = creationFichier();

            if (File.Exists(NomFichier))
            {
                sw.WriteLine(
                DateTime.Now.ToLongDateString() + ("|") + DateTime.Now.ToLongTimeString() + ("|") + message + ("\n"));
               
            }
            fermetureEcriture(sw);
        }
    }
}