﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Examen
namespace projet_club_sportif.Models
{
    public class ExamenDAO : DAO<T_Examen>
    {
        /// <summary>
        /// Ajoute l'objet Examen dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns></returns>
        public override T_Examen Create(T_Examen newObject)
        {
            try
            {
                //On insert dans la table examen les données.
                connection.Open();
                string sqlText = "INSERT INTO examen (grade, id_evenement, id_evenement_1) VALUES(@grade, @id_evenement, @id_cours);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();


                cmd.Parameters.AddWithValue("@grade", newObject.grade);
                cmd.Parameters.AddWithValue("@id_evenement", newObject.pk_examen);
                cmd.Parameters.AddWithValue("@id_cours", newObject.fk_cours);

                cmd.ExecuteNonQuery();
                connection.Close();

                newObject.pk_examen = (int)cmd.LastInsertedId;
                return newObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(T_Examen actualObject)
        {
            try
            {
                EvenementDAO myDAO = new EvenementDAO();
                //On récupère l'Event
                T_Evenement myEvent = myDAO.Find(actualObject.pk_examen);
                //On supprime l'évènement.
                if (!myDAO.Delete(myEvent))
                {
                    return false;
                }
                //On supprime l'examen.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM examen WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", actualObject.pk_examen);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// N'a pas de sens ici.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override T_Examen Find(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Recherche dans la base de données l'Examen ayant l'ID donné. Retourne l'objet en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>T_Examen</returns>
        public override T_Examen Find(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, grade+0 AS grade_as_int FROM examen WHERE id_evenement = @id_evenement";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_evenement", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                T_Examen myExamen = new T_Examen();

                if (reader.HasRows)
                {
                    reader.Read();
                    myExamen.fk_cours = (int)reader["id_evenement_1"];
                    myExamen.pk_examen = (int)reader["id_evenement"];
                    myExamen.grade = (GRADE?)Convert.ToInt32(reader["grade_as_int"]);
                }

                connection.Close();
                return myExamen;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Met à jour la Base de données concernant l'objet passé en argument. Retourne l'objet mis à jour en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns></returns>
        public override T_Examen Update(T_Examen actualObject)
        {
            try
            {
                //On modifie les données dans Extra.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE examen SET grade = @grade, id_evenement_1 = @id_cours WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@grade", actualObject.grade);
                cmd.Parameters.AddWithValue("@id_cours", actualObject.fk_cours);
                cmd.Parameters.AddWithValue("@id_evenement", actualObject.pk_examen);

                cmd.ExecuteNonQuery();
                connection.Close();

                return actualObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des Examens.
        /// </summary>
        /// <returns>List<Examen></returns>
        public override List<T_Examen> GetAll()
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, grade+0 AS grade_as_int FROM examen;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Examen> rows = new List<T_Examen>();

                if(reader.HasRows)
                { 
                    while (reader.Read())
                    {
                        T_Examen myExamen = new T_Examen();
                        myExamen.grade = (GRADE?)Convert.ToInt32(reader["grade_as_int"]);
                        myExamen.fk_cours = (int)reader["id_evenement_1"];
                        myExamen.pk_examen = (int)reader["id_evenement"];
                        rows.Add(myExamen);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return new List<T_Examen>();
            }
        }

        /// <summary>
        /// Retourne la liste des Examens.
        /// </summary>
        /// <returns>List<Examen></returns>
        public List<T_Examen> GetAllByCours(int id_cours)
        {
            List<T_Examen> rows = new List<T_Examen>();

            try
            {
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT *, grade+0 AS grade_as_int FROM examen WHERE id_evenement_1 = @id_cours";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_cours", id_cours);
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Examen myExamen = new T_Examen();
                        myExamen.grade = (GRADE?)Convert.ToInt32(reader["grade_as_int"]);
                        myExamen.fk_cours = (int)reader["id_evenement_1"];
                        myExamen.pk_examen = (int)reader["id_evenement"];

                        rows.Add(myExamen);
                    }
                }
                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return new List<T_Examen>();
            }
        }

        /// <summary>
        /// Retourne la liste des examen auxquelles un étudiant participe.
        /// Retourne null s'il y a une erreur/problème.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<T_Examen></returns>
        public List<T_Examen> GetAllForIdMemberParticipating(int id_pers)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, grade+0 AS grade_as_int FROM examen INNER JOIN evenement ON evenement.id_evenement = examen.id_evenement "
                    + "INNER JOIN participer ON participer.id_evenement = evenement.id_evenement WHERE participer.id_personne = @id_personne";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_personne", id_pers);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Examen> rows = new List<T_Examen>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Examen myExamen = new T_Examen();
                        myExamen.grade = (GRADE?)Convert.ToInt32(reader["grade_as_int"]);
                        myExamen.fk_cours = (int)reader["id_evenement_1"];
                        myExamen.pk_examen = (int)reader["id_evenement"]; 

                        rows.Add(myExamen);
                    }
                }
                connection.Close();
                return rows;              
            }
            catch(Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
            
        }

        //TODO
        /// <summary>
        /// Retourne la liste des examen auxquelles un étudiant participe.
        /// Retourne null s'il y a une erreur/problème.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<T_Examen></returns>
        public List<T_Examen> GetAllForIdMemberNotParticipating(int id_pers)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, grade+0 AS grade_as_int FROM examen WHERE id_evenement NOT IN " +
                    "(SELECT examen.id_evenement FROM examen " +
                    "INNER JOIN evenement ON evenement.id_evenement = examen.id_evenement " +
                    "INNER JOIN participer ON participer.id_evenement = evenement.id_evenement WHERE participer.id_personne = @id_personne)";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_personne", id_pers);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Examen> rows = new List<T_Examen>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Examen myExamen = new T_Examen();
                        myExamen.grade = (GRADE?)Convert.ToInt32(reader["grade_as_int"]);
                        myExamen.fk_cours = (int)reader["id_evenement_1"];
                        myExamen.pk_examen = (int)reader["id_evenement"];

                        rows.Add(myExamen);
                    }
                }
                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }

        }

        /// <summary>
        /// Retourne la liste des examen auxquelles un étudiant participe.
        /// Retourne null s'il y a une erreur/problème.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<T_Examen></returns>
        public List<T_Examen> GetAllForIdMemberParticipatingAndIdCourse(int id_pers, int id_course)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, grade+0 AS grade_as_int FROM examen INNER JOIN evenement ON evenement.id_evenement = examen.id_evenement "
                    + "INNER JOIN participer ON participer.id_evenement = evenement.id_evenement WHERE participer.id_personne = @id_personne AND examen.id_evenement_1 = @id_course";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_personne", id_pers);
                cmd.Parameters.AddWithValue("@id_course", id_course);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Examen> rows = new List<T_Examen>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Examen myExamen = new T_Examen();
                        myExamen.grade = (GRADE?)Convert.ToInt32(reader["grade_as_int"]);
                        myExamen.fk_cours = (int)reader["id_evenement_1"];
                        myExamen.pk_examen = (int)reader["id_evenement"];

                        rows.Add(myExamen);
                    }
                }
                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }

        }
    }
}