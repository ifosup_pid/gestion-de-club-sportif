﻿namespace projet_club_sportif.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Examen
    {
        private ExamenDAO examenDao;
        private EvenementDAO evenementDao;
        private LieuDAO lieuDao;
        private ParticiperDAO participerDao;
        private OrganiserDAO organiserDao;
        private PersonneDAO personneDao;

        public T_Lieu myLieuExamen;
        public T_Evenement myEventExamen;
        public T_Examen myExamen;


        //attributs de examen
        public int pk_examen { get; set; }

        [Display(Name = "Grade:")]
        [Required(ErrorMessage = "Requis")]
        [EnumDataType(typeof(GRADE), ErrorMessage = "Valeur incorrecte")]
        public GRADE? grade { get; set; }

        public int fk_evenement { get; set; }
        public int fk_cours { get; set; }

        //attributs de l'évènement lié au examen
        [Display(Name = "Date et heure de début:")]
        [Required(ErrorMessage = "Requis")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = false)]
        public DateTime dateDebut { get; set; }

        [Display(Name = "Date et heure de fin:")]
        [Required(ErrorMessage = "Requis")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = false)]
        public DateTime dateFin { get; set; }

        [Display(Name = "Description:")]
        [DataType(DataType.MultilineText)]
        //mysql MEDIUMTEXT | worst case : 5,592,415 | best case : 16,777,215
        [StringLength(5592415, ErrorMessage = "La description doit faire moins de {1} charactères.")]
        public string description { get; set; }


        //attributs du lieu lié au examen
        [Display(Name = "Salle:")]
        public string salle { get; set; }

        [Display(Name = "Adresse:")]
        public string adresse { get; set; }

        [Display(Name = "Code Postal:")]
        public int cp { get; set; }

        [Display(Name = "Ville:")]
        public string ville { get; set; }

        [Display(Name = "Pays:")]
        public string pays { get; set; }

        [Display(Name = "Sélectionner un emplacement:")]
        [Required(ErrorMessage = "Requis")]
        [Range(0, int.MaxValue, ErrorMessage = "Lieu non valide")]
        public int fk_lieu { get; set; }


        //attributs des table de liasion entre evenement et personne
        [Display(Name = "Organisateurs de l'examen:")]
        [Required(ErrorMessage = "Requis")]
        public List<T_Personne> organisateurs { get; set; }
        [Display(Name = "Organisateurs de l'examen:")]
        [Required(ErrorMessage = "Requis")]
        public List<int> organisateursSelectedId { get; set; }

        [Display(Name = "Participants à l'examen:")]
        [Required(ErrorMessage = "Requis")]
        public List<T_Personne> participants { get; set; }
        [Display(Name = "Participants à l'examen:")]
        [Required(ErrorMessage = "Requis")]
        public List<int> participantsSelectedId { get; set; }

        public Examen()
        {
            examenDao = new ExamenDAO();
            evenementDao = new EvenementDAO();
            lieuDao = new LieuDAO();

            participerDao = new ParticiperDAO();
            organiserDao = new OrganiserDAO();
            personneDao = new PersonneDAO();

            myExamen = new T_Examen();
            myEventExamen = new T_Evenement();
            myLieuExamen = new T_Lieu();

            organisateurs = personneDao.GetAllStaff();
            organisateursSelectedId = new List<int>();
            participants = new List<T_Personne>();
            participantsSelectedId = new List<int>();

            dateDebut = DateTime.Now;
            dateFin = DateTime.Now;
        }

        public Examen Create(Examen newObject)
        {
            myEventExamen.dateDebut = newObject.dateDebut;
            myEventExamen.dateFin = newObject.dateFin;
            myEventExamen.description = newObject.description;

            myExamen.grade = newObject.grade;
            myExamen.fk_cours = newObject.fk_cours;

            try
            {
                fk_lieu = newObject.fk_lieu;
                myLieuExamen = lieuDao.Find(fk_lieu);
                myEventExamen.fk_lieu = fk_lieu;

                evenementDao.Create(myEventExamen);
                fk_evenement = myEventExamen.pk_evenement;

                myExamen.pk_examen = myEventExamen.pk_evenement;
                examenDao.Create(myExamen);

                //organisateurs de l'examen
                organisateursSelectedId = newObject.organisateursSelectedId;
                //creation des nouveaux liens
                T_Personne un_organisateur;
                T_Organiser une_organisation;
                organisateursSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_organisateur = new T_Personne();
                    un_organisateur = personneDao.Find(unOrganisateurId);

                    une_organisation = new T_Organiser();
                    une_organisation.pk_evenement = fk_evenement;
                    une_organisation.pk_personne = unOrganisateurId;
                    organiserDao.Create(une_organisation);
                });

                //participants à l'examen
                participantsSelectedId = newObject.participantsSelectedId;
                //creation des nouveaux liens
                T_Personne un_participant;
                T_Participer une_participation;
                participantsSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_participant = new T_Personne();
                    un_participant = personneDao.Find(unOrganisateurId);

                    une_participation = new T_Participer();
                    une_participation.pk_evenement = fk_evenement;
                    une_participation.pk_personne = unOrganisateurId;
                    participerDao.Create(une_participation);
                });

                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }


        public bool Update(Examen actualObject)
        {
            // modif dans les sous-objets
            myExamen.grade = actualObject.grade;

            myEventExamen.dateDebut = actualObject.dateDebut;
            myEventExamen.dateFin = actualObject.dateFin;
            myEventExamen.description = actualObject.description;
            myEventExamen.fk_lieu = actualObject.fk_lieu;

            //update avec l'objet en paramettre de la fct
            try
            {
                //partie examen
                myExamen = examenDao.Update(myExamen);
                
                myEventExamen = evenementDao.Update(myEventExamen);

                //suppression des anciens liens
                organiserDao.DeleteAllForEvent(fk_evenement);
                //organisateurs de l'examen
                organisateursSelectedId = actualObject.organisateursSelectedId;
                //creation des nouveaux liens
                T_Personne un_organisateur;
                T_Organiser une_organisation;
                organisateursSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_organisateur = new T_Personne();
                    un_organisateur = personneDao.Find(unOrganisateurId);

                    une_organisation = new T_Organiser();
                    une_organisation.pk_evenement = fk_evenement;
                    une_organisation.pk_personne = unOrganisateurId;
                    organiserDao.Create(une_organisation);
                });

                //suppression des anciens liens
                participerDao.DeleteAllForEvent(fk_evenement);
                //participants à l'examen
                participantsSelectedId = actualObject.participantsSelectedId;
                //creation des nouveaux liens
                T_Personne un_participant;
                T_Participer une_participation;
                participantsSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_participant = new T_Personne();
                    un_participant = personneDao.Find(unOrganisateurId);

                    une_participation = new T_Participer();
                    une_participation.pk_evenement = fk_evenement;
                    une_participation.pk_personne = unOrganisateurId;
                    participerDao.Create(une_participation);
                });

                // find car actualObject est incomplet : pas d'id ni sous-objets.
                Find(actualObject.fk_evenement);

                return true;

            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }


        public bool Delete()
        {
            bool suppressionOk;

            try
            {
                //supprimer organisateurs de l'examen
                organiserDao.DeleteAllForEvent(pk_examen);

                //supprimer participants à l'examen
                participerDao.DeleteAllForEvent(pk_examen);

                suppressionOk = ( examenDao.Delete(myExamen) &&
                                    evenementDao.Delete(myEventExamen));
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }

            return suppressionOk;
        }

        /// <summary>
        /// Find renvoie l'examen à partir de son id 
        /// ! il modifie aussi l'examen courant !
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Examen Find(int id)
        {
            try
            {
                myExamen = examenDao.Find(id);
                pk_examen = id;
                fk_evenement = id;
                fk_cours = myExamen.fk_cours;
                grade = myExamen.grade;

                myEventExamen = evenementDao.Find(id);
                dateDebut = myEventExamen.dateDebut;
                dateFin = myEventExamen.dateFin;
                fk_lieu = myEventExamen.fk_lieu;
                description = myEventExamen.description;

                myLieuExamen = lieuDao.Find(myEventExamen.fk_lieu);
                adresse = myLieuExamen.adresse;
                ville = myLieuExamen.ville;
                cp = myLieuExamen.cp;
                pays = myLieuExamen.pays;
                salle = myLieuExamen.salle;

                organisateursSelectedId.Clear();
                List<T_Organiser> lesOrganisations = organiserDao.GetAllForEvent(fk_evenement);
                lesOrganisations.ForEach(delegate (T_Organiser uneOrganisation)
                {
                    organisateursSelectedId.Add(uneOrganisation.pk_personne);
                });

                participantsSelectedId.Clear();
                List<T_Participer> lesParticipations = participerDao.GetAllForEvent(fk_evenement);
                lesParticipations.ForEach(delegate (T_Participer uneParticipation)
                {
                    participantsSelectedId.Add(uneParticipation.pk_personne);
                });

                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
        

        public List<Examen> GetAllByCours(int id_cours)
        {
            List<Examen> examensDuCours = new List<Examen>();

            foreach (T_Examen unTExam in examenDao.GetAllByCours(id_cours))
            {
                var unExam = new Examen();
                unExam.Find(unTExam.pk_examen);

                examensDuCours.Add(unExam);
            }

            return examensDuCours;

        }

        public List<Examen> GetAll()
        {
            List<Examen> examen = new List<Examen>();

            foreach(T_Examen unTExam in examenDao.GetAll())
            {
                var unExam = new Examen();
                unExam.Find(unTExam.pk_examen);

                examen.Add(unExam);
            }

            return examen;
        }

        public List<Examen> GetAllForIdMemberParticipating(int id_pers)
        {
            List<Examen> examensDuCours = new List<Examen>();

            foreach (T_Examen unTExam in examenDao.GetAllForIdMemberParticipating(id_pers))
            {
                var unExam = new Examen();
                unExam.Find(unTExam.pk_examen);

                examensDuCours.Add(unExam);
            }

            return examensDuCours;

        }

        public List<Examen> GetAllForIdMemberNotParticipating(int id_pers)
        {
            List<Examen> examensDuCours = new List<Examen>();

            foreach (T_Examen unTExam in examenDao.GetAllForIdMemberNotParticipating(id_pers))
            {
                var unExam = new Examen();
                unExam.Find(unTExam.pk_examen);

                examensDuCours.Add(unExam);
            }

            return examensDuCours;

        }

        public List<Examen> GetAllForIdMemberParticipatingAndIdCourse(int id_pers, int id_cours)
        {
            List<Examen> examensDuCours = new List<Examen>();

            foreach (T_Examen unTExam in examenDao.GetAllForIdMemberParticipatingAndIdCourse(id_pers, id_cours))
            {
                var unExam = new Examen();
                unExam.Find(unTExam.pk_examen);

                examensDuCours.Add(unExam);
            }

            return examensDuCours;

        }

    }
}