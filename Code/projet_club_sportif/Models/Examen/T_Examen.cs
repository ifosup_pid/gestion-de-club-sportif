namespace projet_club_sportif.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public enum SECTION : int {
        [Display(Name = "Groupe 1")]
            groupe_1 = 1,
        [Display(Name = "Groupe 2")]
            groupe_2,
        [Display(Name = "Groupe 3")]
            groupe_3 }

    public enum GRADE : int {
        [Display(Name = "Blanche")]
            blanche =1 ,
        [Display(Name = "Blanche avec une ligne jaune")]
            blanche_avec_une_ligne_jaune,
        [Display(Name = "Blanche avec une ligne orange")]
            blanche_avec_une_ligne_orange,
        [Display(Name = "Blanche avec une ligne verte")]
            blanche_avec_une_ligne_verte,
        [Display(Name = "Jaune")]
            jaune,
        [Display(Name = "Orange")]
            orange,
        [Display(Name = "Verte")]
            verte,
        [Display(Name = "Verte avec une barette")]
            verte_avec_une_barette,
        [Display(Name = "Verte avec deux barettes")]
            verte_avec_deux_barettes,
        [Display(Name = "Rouge")]
            rcouge,
        [Display(Name = "Rouge avec une barette")]
            rouge_avec_une_barette,
        [Display(Name = "Rouge avec deux barettes")]
            rouge_avec_deux_barettes,
        [Display(Name = "Noir 1� dan")]
            noir_1_dan,
        [Display(Name = "Noir 2� dan")]
            noir_2_dan,
        [Display(Name = "Noir 3� dan")]
            noir_3_dan,
        [Display(Name = "Noir 4� dan")]
            noir_4_dan,
        [Display(Name = "Noir 5� dan")]
            noir_5_dan,
        [Display(Name = "Noir 6� dan")]
            noir_6_dan,
        [Display(Name = "Noir 7� dan")]
            noir_7_dan,
        [Display(Name = "Noir 8� dan")]
            noir_8_dan,
        [Display(Name = "Noir 9� dan")]
            noir_9_dan,
        [Display(Name = "Noir 10� dan")]
            noir_10_dan
    }


    public class T_Examen
    {
        [Display(Name = "Grade:")]
        public GRADE? grade { get; set; }

        public int pk_examen { get; set; }

        public int fk_cours { get; set; }

    }
}
