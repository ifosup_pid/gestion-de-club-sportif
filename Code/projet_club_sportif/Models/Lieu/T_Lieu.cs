namespace projet_club_sportif.Models
{
    using System.ComponentModel.DataAnnotations;

    public class T_Lieu
    {
        public int pk_lieu { get; set; }

        public bool residentielle { get; set; }

        [Display(Name = "Salle:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(150, ErrorMessage = "La salle doit faire moins de {1} charactères.")]
        public string salle { get; set; }

        [Display(Name = "Adresse:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(255, ErrorMessage = "L'adresse doit faire moins de {1} charactères.")]
        public string adresse { get; set; }

        [Display(Name = "Code Postal:")]
        [Required(ErrorMessage = "Requis")]
        [Range(0, 999999)]//pas de valeur négative
        public int cp { get; set; }

        [Display(Name = "Ville:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(150, ErrorMessage = "Le nom de la ville doit faire moins de {1} charactères.")]
        public string ville { get; set; }

        [Display(Name = "Pays:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(150, ErrorMessage = "Le nom du pays doit faire moins de {1} charactères.")]
        public string pays { get; set; }
    }
}
