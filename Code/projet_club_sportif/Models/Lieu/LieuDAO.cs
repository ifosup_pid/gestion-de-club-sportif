﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Lieu
namespace projet_club_sportif.Models
{
    public class LieuDAO : DAO<T_Lieu>
    {
        /// <summary>
        /// Ajoute l'objet Lieu dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns></returns>
        public override T_Lieu Create(T_Lieu newObject)
        {
            try
            {
                //On insert dans la table extra les données.
                connection.Open();
                string sqlText = "INSERT INTO Lieu (adresse, cp, ville, pays, residentielle, salle) VALUES( @adresse, @cp, @ville, @pays, @residentielle, @salle);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@adresse", newObject.adresse);
                cmd.Parameters.AddWithValue("@cp", newObject.cp);
                cmd.Parameters.AddWithValue("@ville", newObject.ville);
                cmd.Parameters.AddWithValue("@pays", newObject.pays);
                cmd.Parameters.AddWithValue("@residentielle", newObject.residentielle);
                cmd.Parameters.AddWithValue("@salle", newObject.salle);

                cmd.ExecuteNonQuery();
                connection.Close();

                newObject.pk_lieu = (int)cmd.LastInsertedId;

                return newObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns></returns>
        public override bool Delete(T_Lieu actualObject)
        {
            try
            {
                //On supprime le lieu.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM Lieu WHERE id_lieu = @id_lieu;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_lieu", actualObject.pk_lieu);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// il n'est pas implemente.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override T_Lieu Find(string name)
        {
           
            throw new NotImplementedException();
        }

        /// <summary>
        /// Recherche dans la base de données le Lieu ayant l'ID donné. Retourne l'objet en cas de réussite.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override T_Lieu Find(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM Lieu WHERE id_lieu = @id_lieu";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_lieu", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                T_Lieu myLieu = new T_Lieu();

                if (reader.HasRows)
                {
                    reader.Read();
                    myLieu.adresse = (string)reader["adresse"];
                    myLieu.cp = (int)reader["cp"];
                    myLieu.ville = (string)reader["ville"];
                    myLieu.pays = (string)reader["pays"];
                    myLieu.residentielle = (bool)reader["residentielle"];
                    myLieu.salle = reader["salle"] == DBNull.Value ? null : (string)reader["salle"];

                    myLieu.pk_lieu = id;
                }

                connection.Close();
                return myLieu;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Met à jour la Base de données concernant l'objet passé en argument. Retourne l'objet mis à jour en cas de réussite.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns></returns>
        public override T_Lieu Update(T_Lieu actualObject)
        {
            try
            {
                //On modifie les données dans Lieu.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE Lieu SET adresse = @adresse, cp = @cp, ville = @ville, pays = @pays , residentielle = @residentielle, salle = @salle WHERE id_lieu = @id_lieu;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@adresse", actualObject.adresse);
                cmd.Parameters.AddWithValue("@cp", actualObject.cp);
                cmd.Parameters.AddWithValue("@ville", actualObject.ville);
                cmd.Parameters.AddWithValue("@pays", actualObject.pays);
                cmd.Parameters.AddWithValue("@residentielle", actualObject.residentielle);
                cmd.Parameters.AddWithValue("@salle", actualObject.salle);
                cmd.Parameters.AddWithValue("@id_lieu", actualObject.pk_lieu);

                cmd.ExecuteNonQuery();
                connection.Close();

                return actualObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des Lieus.
        /// </summary>
        /// <returns></returns>
        public override List<T_Lieu> GetAll()
        {
            connection.Open();
            string sqlText = "SELECT * FROM Lieu;";
            MySqlCommand cmd = new MySqlCommand(sqlText, connection);
            MySqlDataReader reader = cmd.ExecuteReader();

            List<T_Lieu> rows = new List<T_Lieu>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    T_Lieu myLieu = new T_Lieu();
                    myLieu.adresse = (string)reader["adresse"];
                    myLieu.cp = (int)reader["cp"];
                    myLieu.ville = (string)reader["ville"];
                    myLieu.pays = (string)reader["pays"];
                    myLieu.residentielle = (bool)reader["residentielle"];
                    myLieu.salle = reader["salle"] == DBNull.Value ? null : (string)reader["salle"];
                    myLieu.pk_lieu = (int)reader["id_lieu"];

                    rows.Add(myLieu);
                }
            }
            
            connection.Close();
            return rows;
        }

        /// <summary>
        /// Retourne la liste des Lieus non-residentiels
        /// </summary>
        /// <returns></returns>
        public List<T_Lieu> GetAllForEvents()
        {
            connection.Open();
            string sqlText = "SELECT * FROM Lieu WHERE residentielle =0;";
            MySqlCommand cmd = new MySqlCommand(sqlText, connection);
            MySqlDataReader reader = cmd.ExecuteReader();

            List<T_Lieu> rows = new List<T_Lieu>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    T_Lieu myLieu = new T_Lieu();
                    myLieu.adresse = (string)reader["adresse"];
                    myLieu.cp = (int)reader["cp"];
                    myLieu.ville = (string)reader["ville"];
                    myLieu.pays = (string)reader["pays"];
                    myLieu.residentielle = (bool)reader["residentielle"];
                    myLieu.salle = reader["salle"] == DBNull.Value ? null : (string)reader["salle"];
                    myLieu.pk_lieu = (int)reader["id_lieu"];

                    rows.Add(myLieu);
                }
            }

            connection.Close();
            return rows;
        }
    }
}