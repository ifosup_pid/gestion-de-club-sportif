namespace projet_club_sportif.Models
{
    using System.ComponentModel.DataAnnotations;

    public class T_Theme
    {
        public int pk_theme { get; set; }

        [Display(Name = "Th�me:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(150, ErrorMessage = "Le theme doit faire moins de {1} charact�res.")]
        public string theme { get; set; }

    }
}
