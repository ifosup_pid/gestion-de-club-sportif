﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Theme
namespace projet_club_sportif.Models
{
    public class ThemeDAO : DAO<T_Theme>
    {
        public override T_Theme Create(T_Theme newObject)
        {
            try
            {
                //On insert dans la table theme les données.
                connection.Open();
                string sqlText = "INSERT INTO theme (theme) VALUES(@theme);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@theme", newObject.theme);

                cmd.ExecuteNonQuery();
                connection.Close();

                newObject.pk_theme = (int)cmd.LastInsertedId;

                return newObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public override bool Delete(T_Theme actualObject)
        {
            try
            {
                //On supprime le theme.
                connection.Open();
                string sqlText = "DELETE FROM theme WHERE id_theme = @id_theme";

                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_theme", actualObject.pk_theme);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        public override T_Theme Find(string name)
        {
            T_Theme myTheme = new T_Theme();

            try
            {
                connection.Open();
                string sqlText = "SELECT theme FROM theme WHERE theme = @theme;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@theme", name);

                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                    myTheme.theme = name;
                    myTheme.pk_theme = (int)reader["id_theme"];
                    connection.Close();
                }

                return myTheme;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public override T_Theme Find(int id)
        {
            T_Theme myTheme = new T_Theme();
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM theme WHERE id_theme = @id_theme";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_theme", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                    myTheme.theme = (string)reader["theme"];
                    myTheme.pk_theme = id;
                }

                connection.Close();
                return myTheme;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public override T_Theme Update(T_Theme actualObject)
        {
            try
            {
                //On modifie les données dans theme.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE theme SET theme = @theme WHERE id_theme = @id_theme;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@theme", actualObject.theme);
                cmd.Parameters.AddWithValue("@id_theme", actualObject.pk_theme);

                cmd.ExecuteNonQuery();
                connection.Close();

                return actualObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public override List<T_Theme> GetAll()
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM theme;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Theme> rows = new List<T_Theme>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Theme myTheme = new T_Theme();
                        myTheme.theme = (string)reader["theme"];
                        myTheme.pk_theme = (int)reader["id_theme"];

                        rows.Add(myTheme);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
    }
}