﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Participer
namespace projet_club_sportif.Models
{
    public class Cours_ThemeDAO : DAO<T_Cours_Theme>
    {
        /// <summary>
        /// Ajoute l'objet Participer dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Extra</returns>
        public override T_Cours_Theme Create(T_Cours_Theme newObject)
        {
            try
            {
                //On insert dans la table extra les données.
                connection.Open();
                string sqlText = "INSERT INTO cours_themes (id_theme, id_evenement) VALUES(@id_theme, @id_evenement);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_theme", newObject.pk_theme);
                cmd.Parameters.AddWithValue("@id_evenement", newObject.pk_evenement);

                cmd.ExecuteNonQuery();
                connection.Close();

                return newObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(T_Cours_Theme actualObject)
        {
            try
            {
                //On supprime le cours.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM cours_themes WHERE id_evenement = @id_evenement AND id_theme = @id_theme;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", actualObject.pk_evenement);
                cmd.Parameters.AddWithValue("@id_theme", actualObject.pk_theme);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public bool DeleteAllForTheme(int id_theme)
        {
            try
            {
                //On supprime le cours.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM cours_themes WHERE id_theme = @id_theme;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_theme", id_theme);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public bool DeleteAllForEvent(int id_event)
        {
            try
            {
                //On supprime tout les liens des themes vers un cours
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM cours_themes WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", id_event);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// N'a pas de sens dans ce cas-ci. N'est donc pas implémanté.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override T_Cours_Theme Find(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// N'est pas encore implémanté. Et n'a d'ailleurs pas beaucoup de sens.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override T_Cours_Theme Find(int id)
        {
            throw new NotImplementedException();
        }

        public override T_Cours_Theme Update(T_Cours_Theme actualObject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retourne l'ensemble des évènements auxquels la personne (ID) participe.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<T_Cours_Theme> GetAllForTheme(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM cours_themes WHERE id_theme = @id_theme;";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_theme", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Cours_Theme> rows = new List<T_Cours_Theme>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Cours_Theme myParticipation = new T_Cours_Theme();
                        myParticipation.pk_theme = id;
                        myParticipation.pk_evenement = (int)reader["id_evenement"];
                        rows.Add(myParticipation);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne l'ensemble des évènements auxquels la personne (ID) participe.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<T_Cours_Theme> GetAllForEvent(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM cours_themes WHERE id_evenement = @id_evenement;";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_evenement", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Cours_Theme> rows = new List<T_Cours_Theme>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Cours_Theme myParticipation = new T_Cours_Theme();
                        myParticipation.pk_theme = (int)reader["id_theme"];
                        myParticipation.pk_evenement = id;
                        rows.Add(myParticipation);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public override List<T_Cours_Theme> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}