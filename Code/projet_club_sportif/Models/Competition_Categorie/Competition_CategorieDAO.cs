﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Participer
namespace projet_club_sportif.Models
{
    public class Competition_CategorieDAO : DAO<T_Competition_Categorie>
    {
        /// <summary>
        /// Ajoute l'objet Participer dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Extra</returns>
        public override T_Competition_Categorie Create(T_Competition_Categorie newObject)
        {
            try
            {
                //On insert dans la table extra les données.
                connection.Open();
                string sqlText = "INSERT INTO competitions_categories (id_categorie, id_evenement) VALUES( @id_categorie, @id_evenement);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_categorie", newObject.pk_categorie);
                cmd.Parameters.AddWithValue("@id_evenement", newObject.pk_evenement);

                cmd.ExecuteNonQuery();
                connection.Close();

                return newObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(T_Competition_Categorie actualObject)
        {
            try
            {
                //On supprime le cours.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM competitions_categories WHERE id_evenement = @id_evenement AND id_categorie = @id_categorie;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", actualObject.pk_evenement);
                cmd.Parameters.AddWithValue("@id_categorie", actualObject.pk_categorie);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public bool DeleteAllForCategorie(int id_cat)
        {
            try
            {
                //On supprime le cours.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM competitions_categories WHERE id_categorie = @id_categorie;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_categorie", id_cat);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public bool DeleteAllForEvent(int id_event)
        {
            try
            {
                //On supprime le cours.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM competitions_categories WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", id_event);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// N'a pas de sens dans ce cas-ci. N'est donc pas implémanté.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override T_Competition_Categorie Find(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// N'est pas encore implémanté. Et n'a d'ailleurs pas beaucoup de sens.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override T_Competition_Categorie Find(int id)
        {
            throw new NotImplementedException();
        }

        public override T_Competition_Categorie Update(T_Competition_Categorie actualObject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retourne l'ensemble des évènements auxquels la personne (ID) participe.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<T_Competition_Categorie> GetAllForCategorie(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM competitions_categories WHERE id_categorie = @id_categorie;";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_categorie", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Competition_Categorie> rows = new List<T_Competition_Categorie>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Competition_Categorie myParticipation = new T_Competition_Categorie();
                        myParticipation.pk_categorie = id;
                        myParticipation.pk_evenement = (int)reader["id_evenement"];
                        rows.Add(myParticipation);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne l'ensemble des évènements auxquels la personne (ID) participe.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<T_Competition_Categorie> GetAllForEvent(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM competitions_categories WHERE id_evenement = @id_evenement;";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_evenement", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Competition_Categorie> rows = new List<T_Competition_Categorie>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Competition_Categorie myParticipation = new T_Competition_Categorie();
                        myParticipation.pk_categorie = (int)reader["id_categorie"];
                        myParticipation.pk_evenement = id;
                        rows.Add(myParticipation);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public override List<T_Competition_Categorie> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}