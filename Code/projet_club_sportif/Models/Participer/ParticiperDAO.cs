﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Participer
namespace projet_club_sportif.Models
{
    public class ParticiperDAO : DAO<T_Participer>
    {
        /// <summary>
        /// Ajoute l'objet Participer dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Extra</returns>
        public override T_Participer Create(T_Participer newObject)
        {
            try
            {
                //On insert dans la table extra les données.
                connection.Open();
                string sqlText = "INSERT INTO participer (reussit, classement, id_personne, id_evenement) VALUES(@reussit, @classement, @id_personne, @id_evenement);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@reussit", newObject.reussit);
                cmd.Parameters.AddWithValue("@classement", newObject.classement);
                cmd.Parameters.AddWithValue("@id_personne", newObject.pk_personne);
                cmd.Parameters.AddWithValue("@id_evenement", newObject.pk_evenement);

                cmd.ExecuteNonQuery();
                connection.Close();

                return newObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(T_Participer actualObject)
        {
            try
            {
                //On supprime le cours.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM participer WHERE id_evenement = @id_evenement AND id_personne = @id_personne;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", actualObject.pk_evenement);
                cmd.Parameters.AddWithValue("@id_personne", actualObject.pk_personne);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public bool DeleteAllForEvent(int id_event)
        {
            try
            {
                //On supprime le cours.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM participer WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", id_event);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public bool DeleteAllForPers(int id_pers)
        {
            try
            {
                //On supprime le cours.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM participer WHERE id_personne = @id_personne;";
                cmd.Prepare();
                
                cmd.Parameters.AddWithValue("@id_personne", id_pers);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// N'a pas de sens dans ce cas-ci. N'est donc pas implémanté.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override T_Participer Find(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// N'est pas encore implémanté. Et n'a d'ailleurs pas beaucoup de sens.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override T_Participer Find(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// N'est pas encore implémanté. Et n'a d'ailleurs pas beaucoup de sens.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T_Participer Find(T_Participer actualObject)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM participer WHERE id_evenement = @id_evenement AND id_personne = @id_personne;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_evenement", actualObject.pk_evenement);
                cmd.Parameters.AddWithValue("@id_personne", actualObject.pk_personne);

                MySqlDataReader reader = cmd.ExecuteReader();

                T_Participer myParticipation = new T_Participer();

                if (reader.HasRows)
                {
                    reader.Read();
                    myParticipation.pk_personne = (int)reader["id_personne"];
                    myParticipation.pk_evenement = (int)reader["id_evenement"];
                    myParticipation.reussit = reader["reussit"] == DBNull.Value ? null : (bool?) Convert.ToBoolean(reader["reussit"]);
                    myParticipation.classement = reader["classement"] == DBNull.Value ? null : (int?) Convert.ToInt32(reader["classement"]);

                }

                connection.Close();
                return myParticipation;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public override T_Participer Update(T_Participer actualObject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retourne l'ensemble des évènements auxquels la personne (ID) participe.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<T_Participer> GetAllForPers(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM participer WHERE id_personne = @id_personne;";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_personne", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Participer> rows = new List<T_Participer>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Participer myParticipation = new T_Participer();
                        myParticipation.reussit = reader["reussit"] == DBNull.Value ? null : (bool?)Convert.ToBoolean(reader["reussit"]);
                        myParticipation.classement = reader["classement"] == DBNull.Value ? null : (int?)Convert.ToInt32(reader["classement"]);
                        myParticipation.pk_personne = id;
                        myParticipation.pk_evenement = (int)reader["id_evenement"];
                        rows.Add(myParticipation);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne l'ensemble des personnes participant à un événement(ID).
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<T_Participer> GetAllForEvent(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM participer WHERE id_evenement = @id_evenement;";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_evenement", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Participer> rows = new List<T_Participer>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Participer myParticipation = new T_Participer();
                        myParticipation.reussit = reader["reussit"] == DBNull.Value ? null : (bool?)Convert.ToBoolean(reader["reussit"]);
                        myParticipation.classement = reader["classement"] == DBNull.Value ? null : (int?)Convert.ToInt32(reader["classement"]);
                        myParticipation.pk_personne = (int)reader["id_personne"];
                        myParticipation.pk_evenement = id;
                        rows.Add(myParticipation);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public override List<T_Participer> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}