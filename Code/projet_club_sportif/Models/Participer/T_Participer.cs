namespace projet_club_sportif.Models
{
    using System.ComponentModel.DataAnnotations;

    public class T_Participer
    {
        [Display(Name = "Classement:")]
        [Required(ErrorMessage = "Requis")]
        [Range(0, 99999999)]//pas de valeur n�gative
        public int? classement { get; set; }

        [Display(Name = "R�ussit:")]
        [Required(ErrorMessage = "Requis")]
        public bool? reussit { get; set; }

        public int pk_personne { get; set; }

        public int pk_evenement { get; set; }
    }
}
