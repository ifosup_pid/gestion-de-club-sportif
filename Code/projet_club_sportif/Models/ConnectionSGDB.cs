﻿using MySql.Data.MySqlClient;

namespace projet_club_sportif.Models
{
    public class ConnectionSGDB
    {
        private static MySqlConnection myConnection;


        private static string IP = "127.0.0.1";
        private static string dataBase = "lvagma129870com29190_gcsp";
        private static string user = "root";
        private static string password = "root";

        //pour serveur kevin
        //private static string IP = "127.0.0.1";
        //private static string dataBase = "lvagma129870com29190_gcsp";
        //private static string user = "lvagm_root";
        //private static string password = "AKDM4";

        public static MySqlConnection Connection()
        {
            string connectionString = "SERVER =" + IP + "; DATABASE =" + dataBase + "; UID =" + user + "; PASSWORD =" + password + ";";
            myConnection = new MySqlConnection(connectionString);

            return myConnection;
        }
    }
}