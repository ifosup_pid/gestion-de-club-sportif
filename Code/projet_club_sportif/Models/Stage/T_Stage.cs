namespace projet_club_sportif.Models
{
    using System.ComponentModel.DataAnnotations;

    public class T_Stage
    {
        public int pk_stage { get; set; }

        [Display(Name = "Co�t:")]
        public int cout { get; set; }

        public int fk_evenement { get; set; }
    }
}
