﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Stage
namespace projet_club_sportif.Models
{
    public class StageDAO : DAO<T_Stage>
    {
        /// <summary>
        /// Ajoute l'objet Stage dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Extra</returns>
        public override T_Stage Create(T_Stage newObject)
        {
            try
            {
                //On insert dans la table stage les données.
                connection.Open();
                string sqlText = "INSERT INTO stage (cout, id_evenement) VALUES(@cout, @id_evenement);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@cout", newObject.cout);
                cmd.Parameters.AddWithValue("@id_evenement", newObject.pk_stage);

                cmd.ExecuteNonQuery();
                connection.Close();

                newObject.fk_evenement = (int)cmd.LastInsertedId;
                return newObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(T_Stage actualObject)
        {
            try
            {
                //On supprime l'examen.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM stage WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", actualObject.fk_evenement);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Non implémenté, car n'a pas de sens.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override T_Stage Find(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Recherche dans la base de données l'Examen ayant l'ID donné. Retourne l'objet en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override T_Stage Find(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM stage WHERE id_evenement = @id_evenement";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_evenement", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                T_Stage myTraineeship = new T_Stage();
                if (reader.HasRows)
                {
                    reader.Read();

                    myTraineeship.cout = (int)reader["cout"];
                    myTraineeship.fk_evenement = (int)reader["id_evenement"];
                }

                connection.Close();
                return myTraineeship;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Met à jour la Base de données concernant l'objet passé en argument. Retourne l'objet mis à jour en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns></returns>
        public override T_Stage Update(T_Stage actualObject)
        {
            try
            {
                //On modifie les données dans Stage.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE stage SET cout = @cout WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@cout", actualObject.cout);
                cmd.Parameters.AddWithValue("@id_evenement", actualObject.fk_evenement);

                cmd.ExecuteNonQuery();
                connection.Close();

                return actualObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des Extras.
        /// </summary>
        /// <returns>List(Extra)</returns>
        public override List<T_Stage> GetAll()
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM stage;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Stage> rows = new List<T_Stage>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Stage myTraineeship = new T_Stage();
                        myTraineeship.cout = (int)reader["cout"];
                        myTraineeship.fk_evenement = (int)reader["id_evenement"];

                        rows.Add(myTraineeship);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des stages auxquelles un étudiant participe.
        /// Retourne null s'il y a une erreur/problème.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<Stage></returns>
        public List<T_Stage> GetAllForIdMemberParticipating(int id_pers)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM stage INNER JOIN evenement ON evenement.id_evenement = stage.id_evenement "
                    + "INNER JOIN participer ON participer.id_evenement = evenement.id_evenement WHERE participer.id_personne = @id_personne";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_personne", id_pers);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Stage> rows = new List<T_Stage>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Stage myTrainee = new T_Stage();
                        myTrainee.cout = (int)reader["cout"];
                        myTrainee.fk_evenement = (int)reader["id_evenement"];
                        myTrainee.pk_stage = (int)reader["id_evenement"];
                        //myTrainee.fk_theme = (int)reader[""];

                        rows.Add(myTrainee);
                    }
                }
                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }

        }

        /// <summary>
        /// Retourne la liste des stages auxquelles un étudiant ne participe pas.
        /// Retourne null s'il y a une erreur/problème.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<Stage></returns>
        public List<T_Stage> GetAllForIdMemberNotParticipating(int id_pers)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT  * FROM stage WHERE id_evenement NOT IN " +
                    "(SELECT stage.id_evenement FROM stage " +
                    "INNER JOIN evenement ON evenement.id_evenement = stage.id_evenement " +
                    "INNER JOIN participer ON participer.id_evenement = evenement.id_evenement WHERE participer.id_personne = @id_personne)";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_personne", id_pers);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Stage> rows = new List<T_Stage>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Stage myTrainee = new T_Stage();
                        myTrainee.cout = (int)reader["cout"];
                        myTrainee.fk_evenement = (int)reader["id_evenement"];
                        myTrainee.pk_stage = (int)reader["id_evenement"];
                        //myTrainee.fk_theme = (int)reader[""];

                        rows.Add(myTrainee);
                    }
                }
                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }

        }

    }
}