﻿namespace projet_club_sportif.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Stage
    {
        private StageDAO stageDao;
        private EvenementDAO evenementDao;
        private LieuDAO lieuDao;
        private ThemeDAO themeDao;
        private Stage_ThemeDAO stageThemeDao;
        private ParticiperDAO participerDao;
        private OrganiserDAO organiserDao;
        private PersonneDAO personneDao;

        public T_Lieu myLieuStage;
        public T_Evenement myEventStage;
        public T_Stage myStage;


        //attributs de stage
        public int pk_stage { get; set; }

        [Display(Name = "Coût:")]
        [Required(ErrorMessage = "Requis")]
        [Range(0, 9999)]//pas de valeur négative
        public int cout { get; set; }

        [Display(Name = "Thèmes")]
        [Required(ErrorMessage = "Requis")]
        public List<int> themesSelectedId { get; set; }
        [Display(Name = "Thèmes")]
        [Required(ErrorMessage = "Requis")]
        public List<T_Theme> themes { get; set; }

        public int fk_evenement { get; set; }


        //attributs de l'évènement
        [Display(Name = "Date et heure de début:")]
        [Required(ErrorMessage = "Requis")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = false)]
        public DateTime dateDebut { get; set; }

        [Display(Name = "Date et heure de fin:")]
        [Required(ErrorMessage = "Requis")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = false)]
        public DateTime dateFin { get; set; }

        [Display(Name = "Description:")]
        [DataType(DataType.MultilineText)]
        //mysql MEDIUMTEXT | worst case : 5,592,415 | best case : 16,777,215
        [StringLength(5592415, ErrorMessage = "La description doit faire moins de {1} charactères.")]
        public string description { get; set; }


        //attributs du lieu
        [Display(Name = "Salle:")]
        public string salle { get; set; }

        [Display(Name = "Adresse:")]
        public string adresse { get; set; }

        [Display(Name = "Code Postal:")]
        public int cp { get; set; }

        [Display(Name = "Ville:")]
        public string ville { get; set; }

        [Display(Name = "Pays:")]
        public string pays { get; set; }

        [Display(Name = "Sélectionner un emplacement:")]
        [Required(ErrorMessage = "Requis")]
        [Range(0, int.MaxValue, ErrorMessage = "Lieu non valide")]
        public int fk_lieu { get; set; }


        //attributs des table de liasion entre evenement et personne
        [Display(Name = "Organisateurs du stage:")]
        [Required(ErrorMessage = "Requis")]
        public List<T_Personne> organisateurs { get; set; }
        [Display(Name = "Organisateurs du stage:")]
        [Required(ErrorMessage = "Requis")]
        public List<int> organisateursSelectedId { get; set; }

        [Display(Name = "Participants au stage:")]
        [Required(ErrorMessage = "Requis")]
        public List<T_Personne> participants { get; set; }
        [Display(Name = "Participants au stage:")]
        [Required(ErrorMessage = "Requis")]
        public List<int> participantsSelectedId { get; set; }

        public Stage()
        {
            stageDao = new StageDAO();
            evenementDao = new EvenementDAO();
            lieuDao = new LieuDAO();
            themeDao = new ThemeDAO();
            stageThemeDao = new Stage_ThemeDAO();

            participerDao = new ParticiperDAO();
            organiserDao = new OrganiserDAO();
            personneDao = new PersonneDAO();

            myStage = new T_Stage();
            myEventStage = new T_Evenement();
            myLieuStage = new T_Lieu();

            themes = themeDao.GetAll();
            themesSelectedId = new List<int>();
            organisateurs = personneDao.GetAllStaff();
            organisateursSelectedId = new List<int>();
            participants = new List<T_Personne>();
            participantsSelectedId = new List<int>();

            dateDebut = DateTime.Now;
            dateFin = DateTime.Now;
        }


        public Stage Create(Stage newObject)
        {
            myEventStage.dateDebut = newObject.dateDebut;
            myEventStage.dateFin = newObject.dateFin;
            myEventStage.description = newObject.description;

            myStage.cout = newObject.cout;

            try
            {
                fk_lieu = newObject.fk_lieu;
                myLieuStage = lieuDao.Find(newObject.fk_lieu);
                myEventStage.fk_lieu = myLieuStage.pk_lieu;

                evenementDao.Create(myEventStage);
                fk_evenement = myEventStage.pk_evenement;

                myStage.pk_stage = myEventStage.pk_evenement;
                pk_stage = myEventStage.pk_evenement;
                stageDao.Create(myStage);

                //themes
                themesSelectedId = newObject.themesSelectedId;
                //creation des nouveaux liens
                T_Stage_Theme unStage_theme;
                themesSelectedId.ForEach(delegate (int unThemeId)
                {
                    unStage_theme = new T_Stage_Theme();
                    unStage_theme.pk_theme = unThemeId;
                    unStage_theme.pk_stage = fk_evenement;
                    stageThemeDao.Create(unStage_theme);
                });

                //organisateurs du stage
                organisateursSelectedId = newObject.organisateursSelectedId;
                //creation des nouveaux liens
                T_Personne un_organisateur;
                T_Organiser une_organisation;
                organisateursSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_organisateur = new T_Personne();
                    un_organisateur = personneDao.Find(unOrganisateurId);

                    une_organisation = new T_Organiser();
                    une_organisation.pk_evenement = fk_evenement;
                    une_organisation.pk_personne = unOrganisateurId;
                    organiserDao.Create(une_organisation);
                });

                //participants du stage
                participantsSelectedId = newObject.participantsSelectedId;
                //creation des nouveaux liens
                T_Personne un_participant;
                T_Participer une_participation;
                participantsSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_participant = new T_Personne();
                    un_participant = personneDao.Find(unOrganisateurId);

                    une_participation = new T_Participer();
                    une_participation.pk_evenement = fk_evenement;
                    une_participation.pk_personne = unOrganisateurId;
                    participerDao.Create(une_participation);
                });


                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public bool Delete()
        {
            bool suppressionOk;

            try
            {
                //supprimer organisateurs de l'stage
                organiserDao.DeleteAllForEvent(pk_stage);

                //supprimer participants à l'stage
                participerDao.DeleteAllForEvent(pk_stage);

                //themes : supression des liens
                stageThemeDao.DeleteAllForStage(pk_stage);

                suppressionOk = (stageDao.Delete(myStage) &&
                                    evenementDao.Delete(myEventStage));
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }

            return suppressionOk;
        }


        public bool Update(Stage actualObject)
        {
            // modif dans les sous-objets
            myStage.cout = actualObject.cout;

            myEventStage.dateDebut = actualObject.dateDebut;
            myEventStage.dateFin = actualObject.dateFin;
            myEventStage.description = actualObject.description;

            //update avec l'objet en paramettre de la fct
            try
            {
                //partie stage
                myStage = stageDao.Update(myStage);
                myEventStage.fk_lieu = actualObject.fk_lieu;
                myEventStage = evenementDao.Update(myEventStage);

                //suppression des anciens liens
                stageThemeDao.DeleteAllForStage(fk_evenement);
                themesSelectedId.Clear();
                //themes
                themesSelectedId = actualObject.themesSelectedId;
                //creation des nouveaux liens
                T_Stage_Theme unStage_theme;
                themesSelectedId.ForEach(delegate (int unThemeId)
                {
                    unStage_theme = new T_Stage_Theme();
                    unStage_theme.pk_theme = unThemeId;
                    unStage_theme.pk_stage = fk_evenement;
                    stageThemeDao.Create(unStage_theme);
                });

                //suppression des anciens liens
                organiserDao.DeleteAllForEvent(pk_stage);
                organisateursSelectedId.Clear();
                //organisateurs du stage
                organisateursSelectedId = actualObject.organisateursSelectedId;
                //creation des nouveaux liens
                T_Personne un_organisateur;
                T_Organiser une_organisation;
                organisateursSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_organisateur = new T_Personne();
                    un_organisateur = personneDao.Find(unOrganisateurId);

                    une_organisation = new T_Organiser();
                    une_organisation.pk_evenement = fk_evenement;
                    une_organisation.pk_personne = unOrganisateurId;
                    organiserDao.Create(une_organisation);
                });

                //suppression des anciens liens
                participerDao.DeleteAllForEvent(pk_stage);
                participantsSelectedId.Clear();
                //participants au stage
                participantsSelectedId = actualObject.participantsSelectedId;
                //creation des nouveaux liens
                T_Personne un_participant;
                T_Participer une_participation;
                participantsSelectedId.ForEach(delegate (int unOrganisateurId)
                {
                    un_participant = new T_Personne();
                    un_participant = personneDao.Find(unOrganisateurId);

                    une_participation = new T_Participer();
                    une_participation.pk_evenement = fk_evenement;
                    une_participation.pk_personne = unOrganisateurId;
                    participerDao.Create(une_participation);
                });

                // find car actualObject est incomplet : pas d'id ni sous-objets.
                Find(actualObject.pk_stage);

                return true;

            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }


        /// <summary>
        /// Trouver un stage par son id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Stage Find(int id)
        {
            try
            {
                myStage = stageDao.Find(id);

                pk_stage = id;
                fk_evenement = id;
                cout = myStage.cout;

                myEventStage = evenementDao.Find(id);
                dateDebut = myEventStage.dateDebut;
                dateFin = myEventStage.dateFin;
                fk_lieu = myEventStage.fk_lieu;
                description = myEventStage.description;

                myLieuStage = lieuDao.Find(myEventStage.fk_lieu);
                adresse = myLieuStage.adresse;
                ville = myLieuStage.ville;
                cp = myLieuStage.cp;
                pays = myLieuStage.pays;
                salle = myLieuStage.salle;

                //themes
                themesSelectedId.Clear();
                List<T_Stage_Theme> lesStageThemes = stageThemeDao.GetAllForStage(fk_evenement);
                lesStageThemes.ForEach(delegate (T_Stage_Theme unStageTheme)
                {
                    themesSelectedId.Add(unStageTheme.pk_theme);
                });

                organisateursSelectedId.Clear();
                List<T_Organiser> lesOrganisations = organiserDao.GetAllForEvent(pk_stage);
                lesOrganisations.ForEach(delegate (T_Organiser uneOrganisation)
                {
                    organisateursSelectedId.Add(uneOrganisation.pk_personne);
                });

                participantsSelectedId.Clear();
                List<T_Participer> lesParticipations = participerDao.GetAllForEvent(pk_stage);
                lesParticipations.ForEach(delegate (T_Participer uneParticipation)
                {
                    participantsSelectedId.Add(uneParticipation.pk_personne);
                });

                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }


        public List<Stage> GetAll()
        {
            List<Stage> myStages = new List<Stage>();

            try
            {
                List<T_Stage> myT_Stages = stageDao.GetAll();

                myT_Stages.ForEach(unT_stage =>
                {
                    Stage unStage = new Stage();
                    unStage.Find(unT_stage.fk_evenement);
                    myStages.Add(unStage);
                }
                                );

                return myStages;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }


        public List<Stage> GetAllForIdMemberParticipating(int id_pers)
        {
            List<Stage> myStages = new List<Stage>();

            try
            {
                List<T_Stage> myT_Stages = stageDao.GetAllForIdMemberParticipating(id_pers);

                myT_Stages.ForEach(unT_stage =>
                {
                    Stage unStage = new Stage();
                    unStage.Find(unT_stage.fk_evenement);
                    myStages.Add(unStage);
                }
                                );

                return myStages;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public List<Stage> GetAllForIdMemberNotParticipating(int id_pers)
        {
            List<Stage> myStages = new List<Stage>();

            try
            {
                List<T_Stage> myT_Stages = stageDao.GetAllForIdMemberNotParticipating(id_pers);

                myT_Stages.ForEach(unT_stage =>
                {
                    Stage unStage = new Stage();
                    unStage.Find(unT_stage.fk_evenement);
                    myStages.Add(unStage);
                }
                                );

                return myStages;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
    }
}