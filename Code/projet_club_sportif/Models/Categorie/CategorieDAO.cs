﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Categorie
namespace projet_club_sportif.Models
{
    public class CategorieDAO : DAO<T_Categorie>
    {
        /// <summary>
        /// Ajoute l'objet Categorie dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Extra</returns>
        public override T_Categorie Create(T_Categorie newObject)
        {
            try
            {
                //On insert dans la table catégorie les données.
                connection.Open();
                string sqlText = "INSERT INTO categorie (categorie) VALUES(@categorie);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@categorie", newObject.categorie);

                cmd.ExecuteNonQuery();
                connection.Close();

                newObject.pk_categorie = (int) cmd.LastInsertedId;

                return newObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(T_Categorie actualObject)
        {
            try
            {
                //On supprime la catégorie.
                connection.Open();
                string sqlText = "DELETE FROM categorie WHERE id_categorie = @id_categorie";

                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_categorie", actualObject.pk_categorie);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Recherche dans la base de données la Categorie ayant le titre donné. Retourne l'objet en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Extra</returns>
        public override T_Categorie Find(string name)
        {
            T_Categorie myCategorie = new T_Categorie();

            try
            {
                connection.Open();
                string sqlText = "SELECT id_categorie FROM categorie WHERE categorie = @categorie;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@categorie", name);

                MySqlDataReader reader = cmd.ExecuteReader();

                reader.Read();
                int id = (int)reader["id_categorie"];
                connection.Close();

                myCategorie.categorie = name;

                return myCategorie;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Recherche dans la base de données la Categorie ayant l'ID donné. Retourne l'objet en cas de réussite.
        ///Retourne null en cas d'échec.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override T_Categorie Find(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM categorie WHERE id_categorie = @id_categorie";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id_categorie", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                reader.Read();
                T_Categorie myCategorie = new T_Categorie();
                myCategorie.categorie = (string)reader["categorie"];
                myCategorie.pk_categorie = id;

                connection.Close();
                return myCategorie;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Met à jour la Base de données concernant l'objet passé en argument. Retourne l'objet mis à jour en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns></returns>
        public override T_Categorie Update(T_Categorie actualObject)
        {
            try
            {
                //On modifie les données dans Extra.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE categorie SET categorie = @categorie WHERE id_categorie = @id_categorie;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@categorie", actualObject.categorie);
                cmd.Parameters.AddWithValue("@id_categorie", actualObject.pk_categorie);

                cmd.ExecuteNonQuery();
                connection.Close();

                return actualObject;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des categories.
        /// </summary>
        /// <returns>List(Extra)</returns>
        public override List<T_Categorie> GetAll()
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM categorie;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Categorie> rows = new List<T_Categorie>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Categorie myCategorie = new T_Categorie();
                        myCategorie.categorie = (string)reader["categorie"];
                        myCategorie.pk_categorie = (int)reader["id_categorie"];

                        rows.Add(myCategorie);
                    }
                }

                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
    }
}