namespace projet_club_sportif.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class T_Categorie
    {
        public int pk_categorie { get; set; }

        [Display(Name = "Intitul� de la cat�gorie:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(150, ErrorMessage = "La cat�gorie doit faire moins de {1} caract�res.")]
        public string categorie { get; set; }
    }
}
