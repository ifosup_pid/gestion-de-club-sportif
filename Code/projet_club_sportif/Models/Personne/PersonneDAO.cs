﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace projet_club_sportif.Models
{
    //Implémentation de l'abstract DAO dans le cadre de la classe métier Personne
    public class PersonneDAO : DAO<T_Personne>
    {
        /// <summary>
        /// Créé une personne dans la base de donnée, il faut lui passer l'objet Personne et vous retourne cet objet avec la clef primaire.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Personne</returns>
        public override T_Personne Create(T_Personne newObject)
        {
            try
            {
                connection.Open();
                string sqlText = "INSERT INTO personne (nom, prenom, mdp, taille, poids, age, sexe, staff, grade, id_lieu, email, telephone) VALUES "
                    + "(@nom, @prenom, @mdp, @taille, @poids, @age, @sexe, @staff, @grade, @id_lieu, @email, @telephone)";

                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@nom", newObject.nom);
                cmd.Parameters.AddWithValue("@prenom", newObject.prenom);
                cmd.Parameters.AddWithValue("@mdp", newObject.mdp);
                cmd.Parameters.AddWithValue("@taille", newObject.taille);
                cmd.Parameters.AddWithValue("@poids", newObject.poids);
                cmd.Parameters.AddWithValue("@age", newObject.age);
                cmd.Parameters.AddWithValue("@sexe", newObject.sexe);
                cmd.Parameters.AddWithValue("@staff", newObject.staff);
                cmd.Parameters.AddWithValue("@grade", newObject.grade);
                cmd.Parameters.AddWithValue("@id_lieu", newObject.fk_lieu);
                cmd.Parameters.AddWithValue("@email", newObject.email);
                cmd.Parameters.AddWithValue("@telephone", newObject.telephone);

                cmd.ExecuteNonQuery();
                connection.Close();

                newObject.pk_personne = (int) cmd.LastInsertedId;

                return newObject;
            }
            catch(Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet Personne de la base de donnée et reourne true en cas de réussite et false en cas d'échec.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(T_Personne actualObject)
        {
            try
            {
                connection.Open();
                string sqlText = "DELETE FROM personne WHERE id_personne = @id_personne";

                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_personne", actualObject.pk_personne);
                cmd.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Trouve l'objet Personne dans la base de données. Puis, passe l'objet au controller.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Personne</returns>
        public override T_Personne Find(string name)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, sexe+0 AS sexe_as_int, grade+0 AS grade_as_int FROM personne WHERE nom = @nom;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@nom", name);

                MySqlDataReader reader = cmd.ExecuteReader();

                reader.Read();
                T_Personne myPerson = new T_Personne();
                myPerson.pk_personne = (int)reader["id_personne"];
                myPerson.nom = (string)reader["nom"];
                myPerson.prenom = (string)reader["prenom"];
                myPerson.mdp = (string)reader["mdp"];
                myPerson.taille = (int)reader["taille"];
                myPerson.poids = (int)reader["poids"];
                myPerson.age = (int)reader["age"];
                myPerson.sexe = (SEXE?)Convert.ToInt32(reader["sexe_as_int"]);
                myPerson.staff = (bool)reader["staff"];
                myPerson.grade = (GRADE?)Convert.ToInt32(reader["grade_as_int"]);
                myPerson.fk_lieu = (int)reader["id_lieu"];
                myPerson.email = (string)reader["email"];
                myPerson.telephone = (string)reader["telephone"];

                connection.Close();
                return myPerson;
            }
            catch(Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Trouve l'objet Personne dans la base de données. Puis, passe l'objet au controller.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Personne</returns>
        public T_Personne FindByEmail(string email)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, sexe+0 AS sexe_as_int, grade+0 AS grade_as_int FROM personne WHERE email = @email;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@email", email);

                MySqlDataReader reader = cmd.ExecuteReader();

                T_Personne myPerson = new T_Personne();
                reader.Read();
                if (reader.HasRows)
                {
                    myPerson.pk_personne = (int)reader["id_personne"];
                    myPerson.nom = (string)reader["nom"];
                    myPerson.prenom = (string)reader["prenom"];
                    myPerson.mdp = (string)reader["mdp"];
                    myPerson.taille = (int)reader["taille"];
                    myPerson.poids = (int)reader["poids"];
                    myPerson.age = (int)reader["age"];
                    myPerson.sexe = (SEXE?)Convert.ToInt32(reader["sexe_as_int"]);
                    myPerson.staff = (bool)reader["staff"];
                    myPerson.grade = (GRADE?)Convert.ToInt32(reader["grade_as_int"]);
                    myPerson.fk_lieu = (int)reader["id_lieu"];
                    myPerson.email = (string)reader["email"];
                    myPerson.telephone = (string)reader["telephone"];
                }
                    
                connection.Close();
                return myPerson;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Trouve l'objet Personne dans la base de données. Puis, passe l'objet au controller.
        /// En cas d'échec retourne null.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Personne</returns>
        public override T_Personne Find(int id)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, sexe+0 AS sexe_as_int, grade+0 AS grade_as_int FROM personne WHERE id_personne = @id;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                reader.Read();
                T_Personne myPerson = new T_Personne();

                if (reader.HasRows)
                {
                    myPerson.pk_personne = id;
                    myPerson.nom = (string)reader["nom"];
                    myPerson.prenom = (string)reader["prenom"];
                    myPerson.mdp = (string)reader["mdp"];
                    myPerson.taille = (int)reader["taille"];
                    myPerson.poids = (int)reader["poids"];
                    myPerson.age = (int)reader["age"];
                    myPerson.sexe = (SEXE?)Convert.ToInt32(reader["sexe_as_int"]);
                    myPerson.staff = (bool)reader["staff"];
                    myPerson.grade = (GRADE?)Convert.ToInt32(reader["grade_as_int"]);
                    myPerson.fk_lieu = (int)reader["id_lieu"];
                    myPerson.email = (string)reader["email"];
                    myPerson.telephone = (string)reader["telephone"];
                }
                    
                connection.Close();
                return myPerson;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Met à jour l'obket Personne dans la base de donnée et retourne l'objet.
        /// En cas d'échec, retourne null.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns></returns>
        public override T_Personne Update(T_Personne actualObject)
        {
            try
            {
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE personne SET nom = @nom, prenom = @prenom, mdp = @mdp, taille = @taille, poids = @poids, age = @age, sexe = @sexe, " +
                    " staff = @staff, grade = @grade, email = @email, telephone = @telephone WHERE id_personne = @id_personne;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@nom", actualObject.nom);
                cmd.Parameters.AddWithValue("@prenom", actualObject.prenom);
                cmd.Parameters.AddWithValue("@mdp", actualObject.mdp);
                cmd.Parameters.AddWithValue("@taille", actualObject.taille);
                cmd.Parameters.AddWithValue("@poids", actualObject.poids);
                cmd.Parameters.AddWithValue("@age", actualObject.age);
                cmd.Parameters.AddWithValue("@sexe", actualObject.sexe);
                cmd.Parameters.AddWithValue("@staff", actualObject.staff);
                cmd.Parameters.AddWithValue("@grade", actualObject.grade);
                cmd.Parameters.AddWithValue("@email", actualObject.email);
                cmd.Parameters.AddWithValue("@telephone", actualObject.telephone);
                cmd.Parameters.AddWithValue("@id_personne", actualObject.pk_personne);

                cmd.ExecuteNonQuery();
                connection.Close();

                return actualObject;
            }
            catch(Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Vérifie la concordance entre les arguments passés et les données existantes, retourne true en cas de concordance
        /// Retourne false en cas d'échec de concordance.
        /// </summary>
        /// <param email="email"></param>
        /// <param name="mdp"></param>
        /// <returns>bool</returns>
        public bool Authenticate(string email, string mdp)
        {
            try
            {
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT mdp FROM personne WHERE email = @email;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@email", email);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();

                if (reader.HasRows)
                {
                    string truePassowd = (string)reader["mdp"];
                    connection.Close();

                    if (mdp == truePassowd)
                    {
                        return true;
                    }

                    return false;
                }
                else
                {
                    connection.Close();
                    return false;
                }
            }
            catch(Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }           

        /// <summary>
        /// Génère la liste du staff et la retourne.
        /// retourne null en cas d'échec.
        /// </summary>
        /// <returns>List(Personne)</returns>
        public List<T_Personne> GetAllStaff()
        {
            try

            {
                connection.Open();
                string sqlText = "SELECT *, sexe+0 AS sexe_as_int, grade+0 AS grade_as_int FROM personne WHERE staff = 1;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                MySqlDataReader reader = cmd.ExecuteReader();

                int number = reader.FieldCount;
                List<T_Personne> rows = new List<T_Personne>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Personne myPerson = new T_Personne();

                        myPerson.pk_personne = (int)reader["id_personne"];
                        myPerson.nom = (string)reader["nom"];
                        myPerson.prenom = (string)reader["prenom"];
                        myPerson.email = (string)reader["email"];
                        myPerson.mdp = (string)reader["mdp"];
                        myPerson.taille = (int)reader["taille"];
                        myPerson.poids = (int)reader["poids"];
                        myPerson.age = (int)reader["age"];
                        myPerson.sexe = (SEXE?)Convert.ToInt32(reader["sexe_as_int"]);
                        myPerson.staff = (bool)reader["staff"];
                        myPerson.grade = (GRADE?) Convert.ToInt32(reader["grade_as_int"]);
                        myPerson.fk_lieu = (int)reader["id_lieu"];
                        myPerson.telephone = (string)reader["telephone"];
                        rows.Add(myPerson);
                    }
                }
                connection.Close();
                return rows;
            }
            catch(Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Génère la liste des pratiquants et la retourne.
        /// En cas d'échec retourne null.
        /// </summary>
        /// <returns></returns>
        public List<T_Personne> GetAllPractising()
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT *, sexe+0 AS sexe_as_int, grade+0 AS grade_as_int FROM personne WHERE staff = 0;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                MySqlDataReader reader = cmd.ExecuteReader();

                List<T_Personne> rows = new List<T_Personne>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Personne myPerson = new T_Personne();

                        myPerson.pk_personne = (int)reader["id_personne"];
                        myPerson.nom = (string)reader["nom"];
                        myPerson.prenom = (string)reader["prenom"];
                        myPerson.email = (string)reader["email"];
                        myPerson.mdp = (string)reader["mdp"];
                        myPerson.taille = (int)reader["taille"];
                        myPerson.poids = (int)reader["poids"];
                        myPerson.age = (int)reader["age"];
                        myPerson.sexe = (SEXE?)Convert.ToInt32(reader["sexe_as_int"]);
                        myPerson.staff = (bool)reader["staff"];
                        myPerson.grade = (GRADE?)Convert.ToInt32(reader["grade_as_int"]);
                        myPerson.fk_lieu = (int)reader["id_lieu"];
                        myPerson.telephone = (string)reader["telephone"];
                        rows.Add(myPerson);
                    }
                }
                    
                connection.Close();
                return rows;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Non implémanté ici, car demande plus de finesse. cf : GetAllStaff et GetAllStaff
        /// </summary>
        /// <returns></returns>
        public override List<T_Personne> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retourne la liste des personnes organisant l'évènement.
        /// </summary>
        /// <returns>The list.</returns>
        /// <param name="actualObject">Actual object.</param>
        public List<T_Personne> OrganizersList(int pk_cours)
        {
            try
            {
                List<T_Personne> myList = new List<T_Personne>();
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM organiser INNER JOIN personne WHERE organiser.id_evenement = organiser.@id_evenement";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", pk_cours);
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        T_Personne myPerson = new T_Personne();

                        myPerson.pk_personne = (int)reader["id_personne"];
                        myPerson.nom = (string)reader["nom"];
                        myPerson.prenom = (string)reader["prenom"];
                        myPerson.email = (string)reader["email"];
                        myPerson.mdp = (string)reader["mdp"];
                        myPerson.taille = (int)reader["taille"];
                        myPerson.poids = (int)reader["poids"];
                        myPerson.age = (int)reader["age"];
                        myPerson.sexe = (SEXE?)Convert.ToInt32(reader["sexe_as_int"]);
                        myPerson.staff = (bool)reader["staff"];
                        myPerson.grade = (GRADE?)Convert.ToInt32(reader["grade_as_int"]);
                        myPerson.fk_lieu = (int)reader["id_lieu"];
                        myPerson.telephone = (string)reader["telephone"];

                        myList.Add(myPerson);
                    }
                }
                connection.Close();

                return myList;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des participants à l'évènement.
        /// </summaryreturn myPersonnes;>
        /// <returns>The list.</returns>
        /// <param name="actualObject">Actual object.</param>
        public List<Personne> ParticipantsList(int pk_cours)
        {
            try
            {
                List<Personne> myList = new List<Personne>();
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM participer INNER JOIN personne ON participer.id_personne = personne.id_personne WHERE participer.id_evenement = @id_evenement";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", pk_cours);
                MySqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Personne myPerson = new Personne();

                        myPerson.pk_personne = (int)reader["id_personne"];
                        myPerson.nom = (string)reader["nom"];
                        myPerson.prenom = (string)reader["prenom"];
                        myPerson.email = (string)reader["email"];
                        myPerson.mdp = (string)reader["mdp"];
                        myPerson.taille = (int)reader["taille"];
                        myPerson.poids = (int)reader["poids"];
                        myPerson.age = (int)reader["age"];
                        //myPerson.sexe = (SEXE?)Convert.ToInt32(reader["sexe_as_int"]);
                        myPerson.staff = (bool)reader["staff"];
                       // myPerson.grade = (GRADE?)Convert.ToInt32(reader["grade_as_int"]);
                        myPerson.fk_lieu = (int)reader["id_lieu"];
                        myPerson.telephone = (string)reader["telephone"];

                        myList.Add(myPerson);
                    }
                }
                connection.Close();

                return myList;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
    }
}