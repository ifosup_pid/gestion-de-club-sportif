namespace projet_club_sportif.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public enum SEXE : int {
        [Display(Name = "F�minin")]
            feminin = 1,
        [Display(Name = "Masculin")]
            masculin,
        [Display(Name = "Autre")]
            autre }

    public class T_Personne
    {
        public int pk_personne { get; set; }

        [Display(Name = "E-mail:")]
        public string email { get; set; }

        [Display(Name = "Nom:")]
        public string nom { get; set; }

        [Display(Name = "Prenom:")]
        public string prenom { get; set; }

        [Display(Name = "Mot de passe:")]
        public string mdp { get; set; }

        [Display(Name = "Taille:")]
        public int taille { get; set; }

        [Display(Name = "Poids:")]
        public int poids { get; set; }

        [Display(Name = "Age:")]
        public int age { get; set; }

        [Display(Name = "Sexe:")]
        //SEXE? => pour autoriser la valeur null
        public SEXE? sexe { get; set; }

        [Display(Name = "Staff:")]
        public bool staff { get; set; }

        [Display(Name = "Grade:")]
        public GRADE? grade { get; set; }

        [Display(Name = "T�l�phone")]
        public String telephone { get; set; }

        public int fk_lieu { get; set; }
	}
}
