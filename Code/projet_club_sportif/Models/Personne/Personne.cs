﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace projet_club_sportif.Models
{
    public class Personne
    {
        private PersonneDAO personneDao;
        private LieuDAO lieuDao;

        public T_Personne myPersonne;
        public T_Lieu myLieu;

        public int pk_personne { get; set; }

        [Display(Name = "E-mail:")]
        [Required(ErrorMessage = "Requis")]
        [EmailAddress(ErrorMessage = "Adresse email invalide")]
        [StringLength(150, ErrorMessage = "L'email doit faire moins de {1} caractères.")]
        public string email { get; set; }

        [Display(Name = "Nom:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(255, ErrorMessage = "Le nom doit faire moins de {1} caractères.")]
        public string nom { get; set; }

        [Display(Name = "Prenom:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(250, ErrorMessage = "Le prenom doit faire moins de {1} caractères.")]
        public string prenom { get; set; }

        [Display(Name = "Mot de passe:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(35, ErrorMessage = "Le mot de passe doit faire moins de {1} caractères.")]
        public string mdp { get; set; }

        [Display(Name = "Taille:")]
        [Required(ErrorMessage = "Requis")]
        [Range(0, 999)] //pas de valeur négative
        public int taille { get; set; }

        [Display(Name = "Poids:")]
        [Required(ErrorMessage = "Requis")]
        [Range(0, 9999)] //pas de valeur négative
        public int poids { get; set; }

        [Display(Name = "Age:")]
        [Required(ErrorMessage = "Requis")]
        [Range(0, 140)] //pas de valeur négative
        public int age { get; set; }

        [Display(Name = "Sexe:")]
        [Required(ErrorMessage = "Requis")]
        [EnumDataType(typeof(SEXE), ErrorMessage = "Valeur incorrecte")]
        public SEXE? sexe { get; set; }

        [Display(Name = "Staff:")]
        [Required(ErrorMessage = "Requis")]
        public bool staff { get; set; }

        [Display(Name = "Grade:")]
        [Required(ErrorMessage = "Requis")]
        [EnumDataType(typeof(GRADE), ErrorMessage = "Valeur incorrecte")]
        public GRADE? grade { get; set; }

        [Display(Name = "Téléphone")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(20, ErrorMessage = "Le pays doit faire moins de {1} caractères.")]
        public String telephone { get; set; }

        public int fk_lieu { get; set; }


        [Display(Name = "Adresse:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(150, ErrorMessage = "L'adresse doit faire moins de {1} caractères.")]
        public string adresse { get; set; }

        [Display(Name = "Code Postal:")]
        [Required(ErrorMessage = "Requis")]
        [Range(0, 999999)]//pas de valeur négative
        public int cp { get; set; }

        [Display(Name = "Ville:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(150, ErrorMessage = "La ville doit faire moins de {1} caractères.")]
        public string ville { get; set; }

        [Display(Name = "Pays:")]
        [Required(ErrorMessage = "Requis")]
        [StringLength(150, ErrorMessage = "Le pays doit faire moins de {1} caractères.")]
        public string pays { get; set; }


        //constructeur
        public Personne()
        {
            personneDao = new PersonneDAO();
            lieuDao = new LieuDAO();

            myPersonne = new T_Personne();
            myLieu = new T_Lieu();
        }

        public Personne Create(Personne newObject)
        {
            myLieu.adresse = newObject.adresse;
            myLieu.ville = newObject.ville;
            myLieu.cp = newObject.cp;
            myLieu.pays = newObject.pays;
            myLieu.residentielle = true;

            myPersonne.nom = newObject.nom;
            myPersonne.prenom = newObject.prenom;
            myPersonne.mdp = newObject.mdp;
            myPersonne.taille = newObject.taille;
            myPersonne.poids = newObject.poids;
            myPersonne.age = newObject.age;
            myPersonne.sexe = newObject.sexe;
            myPersonne.staff = newObject.staff;
            myPersonne.grade = newObject.grade;
            myPersonne.email = newObject.email;
            myPersonne.telephone = newObject.telephone;

            try
            {
                lieuDao.Create(myLieu);
                myPersonne.fk_lieu = myLieu.pk_lieu;
                fk_lieu = myLieu.pk_lieu;

                personneDao.Create(myPersonne);
                pk_personne = myPersonne.pk_personne;
                
                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public bool Delete()
        {
            //TODO
            bool suppressionOk = true;

            try
            {
                lieuDao.Delete(myLieu);
                personneDao.Delete(myPersonne);
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }

            return suppressionOk;
        }

        public Personne Find(int id)
        {
            try
            {
                myPersonne = personneDao.Find(id);

                pk_personne = id;
                nom = myPersonne.nom;
                prenom = myPersonne.prenom;
                mdp = myPersonne.mdp;
                taille = myPersonne.taille;
                poids = myPersonne.poids;
                age = myPersonne.age;
                sexe = myPersonne.sexe;
                staff = myPersonne.staff;
                grade = myPersonne.grade;
                email = myPersonne.email;
                telephone = myPersonne.telephone;
                fk_lieu = myPersonne.fk_lieu;

                myLieu = lieuDao.Find(fk_lieu);

                adresse = myLieu.adresse;
                ville = myLieu.ville;
                cp = myLieu.cp;
                pays = myLieu.pays;

                return this;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public Personne FindByEmail(string name)
        {
            try
            {
                myPersonne = personneDao.FindByEmail(name);
                return Find(myPersonne.pk_personne);
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
}

        public List<Personne> GetAll()
        {
            //car pas implémenté dans PersonneDao
            throw new NotImplementedException();

            List<Personne> myPersonnes = new List<Personne>();

            try
            {
                List<T_Personne> myT_Personnes = personneDao.GetAll();

                myT_Personnes.ForEach(unT_personne =>
                                        {
                                            Personne unPersonne = new Personne();
                                            unPersonne.Find(unT_personne.pk_personne);
                                            myPersonnes.Add(unPersonne);
                                        }
                                     );

                return myPersonnes;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        
        public List<Personne> GetAllPractising()
        {
            List<Personne> myPersonnes = new List<Personne>();

            try
            {
                List<T_Personne> myT_Personnes = personneDao.GetAllPractising();

                myT_Personnes.ForEach(unT_personne =>
                                        {
                                            Personne unPersonne = new Personne();
                                            unPersonne.Find(unT_personne.pk_personne);
                                            myPersonnes.Add(unPersonne);
                                        }
                                     );

                return myPersonnes;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des participants à un événement.
        /// </summary>
        /// <param name="myEvent"></param>
        /// <returns></returns>
        public List<Personne> ParticipantsList(int myEvent)
        {
            List<Personne> myPerson = new List<Personne>();
            try
            {
                myPerson = personneDao.ParticipantsList(myEvent);
                
                return myPerson;
            }
            catch(Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }

        public List<Personne> GetAllStaff()
        {
            List<Personne> myPersonnes = new List<Personne>();

            try
            {
                List<T_Personne> myT_Personnes = personneDao.GetAllStaff();

                  myT_Personnes.ForEach(unT_personne =>
                                            {
                                                Personne unPersonne = new Personne();
                                                unPersonne.Find(unT_personne.pk_personne);
                                                myPersonnes.Add(unPersonne);
                                            }
                                         );

                return myPersonnes;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return null;
            }
        }
        public bool Update(Personne actualObject)
        {
            // find car actualObject est incomplet : pas d'id ni sous-objets.
            //myPersonne = personneDao.Find(actualObject.pk_personne);//TODO : remove

            //modif dans les sous-objets
            myLieu = lieuDao.Find(myPersonne.fk_lieu);
            myLieu.adresse = actualObject.adresse;
            myLieu.ville = actualObject.ville;
            myLieu.cp = actualObject.cp;
            myLieu.pays = actualObject.pays;

            myPersonne.pk_personne = actualObject.pk_personne;
            myPersonne.nom = actualObject.nom;
            myPersonne.prenom = actualObject.prenom;
            myPersonne.mdp = actualObject.mdp;
            myPersonne.taille = actualObject.taille;
            myPersonne.poids = actualObject.poids;
            myPersonne.age = actualObject.age;
            myPersonne.sexe = actualObject.sexe;
            myPersonne.staff = actualObject.staff;
            myPersonne.grade = actualObject.grade;
            myPersonne.email = actualObject.email;
            myPersonne.telephone = actualObject.telephone;
            myPersonne.fk_lieu = actualObject.fk_lieu;

            //update avec l'objet en paramettre de la fct
            try
            {
                myPersonne = personneDao.Update(myPersonne);
                myLieu = lieuDao.Update(myLieu);

                return true;
            }
            catch (Exception e)
            {
                gcspFileLog gestionErr = new gcspFileLog();
                gestionErr.ecrire(e.ToString());
                return false;
            }
        }
    }
}
