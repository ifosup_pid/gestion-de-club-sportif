﻿using projet_club_sportif.Models;

namespace projet_club_sportif.ViewModels
{
    public class PersonneViewModel
    {
        public T_Personne Personne { get; set; }

        public bool Authentifie { get; set; }
    }
}