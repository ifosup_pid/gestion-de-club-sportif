﻿using System.Collections.Generic;
using projet_club_sportif.Models;

namespace projet_club_sportif.ViewModels
{
    public class coursExamenViewModel
    {
        public Cours Cours { get; set; }
        public List<Examen> Examens { get; set; }
    }
}