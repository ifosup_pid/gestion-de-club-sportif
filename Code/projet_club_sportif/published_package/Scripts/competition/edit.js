﻿//après le chargement des librairies javascripts
$(document).ready(function () {
    //pour gerer les nombres à virgules
    jQuery.extend(jQuery.validator.methods, {
        date: function (value, element) {
            return this.optional(element) || /^\d\d?\.\d\d?\.\d\d\d?\d?$/.test(value);
        },
        number: function (value, element) {
            return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:,\d+)?$/.test(value);
        }
    });

    //remplissage des inputs date au demarrage
    dateTimeDebut = moment($('#dateDebut').val(), "DD-MM-YY HH:mm:ss");
    $('#dateDebut-').val(dateTimeDebut.format("DD/MM/YY"));
    $('#heureDebut-').val(dateTimeDebut.format("HH:mm"));
    dateTimeFin = moment($('#dateFin').val(), "DD-MM-YY HH:mm:ss");
    $('#dateFin-').val(dateTimeFin.format("DD/MM/YY"));
    $('#heureFin-').val(dateTimeFin.format("HH:mm"));

    // initialize input widgets first
    //pour les multiselect
    $(".chosen").chosen();

    //date widget
    $('.date').datepicker({
        'dateFormat': 'dd/mm/y',
        'changeMonth': true,
        'changeYear': true,
        'autoclose': true,
        'minDate': 0,
        'defaultDate': 0
    });

    //time widget
    $('.time').timepicker({
        timeFormat: 'H:i',
        step: 5,
        'minTime': '00:00',
        maxTime: '24:00',
        defaultTime: '18:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true,
        showDuration: false,
        scrollDefault: 'now',
        forceRoundTime: true
    });

    //créé le widget dataTable
    var table = $('#table-choix-lieu').DataTable({ paging: false, bInfo: false, bautoWidth: false});

    // Apply the search
    table.columns().every(function () {
        var that = this;

        $('input', this.header()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    // initialize datepair
    datepair = $('#edit-competition-form').datepair({
        anchor: 'start',
        'defaultTimeDelta': '7200000',
        'updateDate': function (input, dateObj) { $(input).datepicker('setDate', dateObj); }
    });
    // si modification de la date ou l'heure => modifie le input hidden
    $('#edit-competition-form').on('rangeSelected', function () {
        updateDates();
    });

    //clic pour ajouter un prix
    var num_prix = $('.PrixCompetition').length;
    var ajax = false;
    $("#AddPrixButton").click(function () {
        if (ajax == false) {
            ajax = true;
            getPrix(num_prix);
        }
        ajax = false;
    });

    //clic pour supprimer le dernier prix
    $("#SupprimerPrixButton").click(function () {
        if (ajax == false) {
            ajax = true;
            if (num_prix > 1) {
                $("#PrixCompetition-" + (num_prix)).remove();
                num_prix--;
                $(jq("listPrix[" + num_prix + "].prix")).off('change');
            }
        }
        ajax = false;
    });

    //les prix sont incrémental
    if (num_prix >= 2)
    {
        for (var i = 2; i <= num_prix; i++)
        {
            //valeur min
            var id = "listPrix[" + i + "].prix";
            var idPrec = "listPrix[" + (i-1) + "].prix";

            var min = parseFloat($(jq(idPrec)).val().replace(/,/, '.'));
            if (parseFloat($(jq(id)).val().replace(/,/, '.')) < min)
            {
                $(jq(id)).attr("min", min);
            }

            //valeur min si changement
            $(jq(id)).change(function ()
            {
                var min = parseFloat($(jq(idPrec)).val().replace(/,/, '.'));

                if (parseFloat($(jq(id)).val().replace(/,/, '.')) < min)
                {
                    $(jq(id)).attr("min", min);
                }
                else {
                    $(jq(id)).removeAttr("min");
                }
            });
        }

        for (var i = 1; i < num_prix; i++) {
            var id = "listPrix[" + i + "].prix";
            var idSuiv = "listPrix[" + (i + 1) + "].prix";

            //attribut min du suivant si changement
            $(jq(id)).change(function () {
                var min = parseFloat($(jq(id)).val().replace(/,/, '.'));

                if (parseFloat($(jq(idSuiv)).val().replace(/,/, '.')) < min) {
                    $(jq(id)).attr("min", min);
                }
                else {
                    $(jq(id)).removeAttr("min");
                }
            });
        }
    }
    

    //fonction pour remplir les inputs dateDebut et dateFin (hidden)
    function updateDates() {
        dateDebut = moment($('#dateDebut-').val(), "DD/MM/YY");
        heureDebut = moment($('#heureDebut-').val(), "HH:mm");
        dateDebut.set({
            'hour': parseInt(heureDebut.hours(), 10),
            'minute': parseInt(heureDebut.minutes(), 10),
            'second': parseInt(heureDebut.seconds(), 10)
        });
        $('#dateDebut').val(dateDebut.format("YYYY-MM-DD HH:mm:ss"));

        dateFin = moment($('#dateFin-').val(), "DD/MM/YY");
        heureFin = moment($('#heureFin-').val(), "HH:mm");
        dateFin.set({
            'hour': parseInt(heureFin.hours(), 10),
            'minute': parseInt(heureFin.minutes(), 10),
            'second': parseInt(heureFin.seconds(), 10)
        });
        $('#dateFin').val(dateFin.format("YYYY-MM-DD HH:mm:ss"));
    }

    function getPrix(num) {
        $.ajax({
            type: "GET",
            url: "/competitions/GetPrix",
            data: { num_prix: num+1 },
            dataType: "html",
            cache: false,
            success: function (data) {
                //incrememntation seulement si appel ajax ok
                num_prix++;

                //insertiion du code html
                $("#prix").append(data);
                
                //resoud pb de validation : multiselect requis
                $("#edit-competition-form").removeData("validator");
                $("#edit-competition-form").removeData("unobtrusiveValidation");
                $.validator.setDefaults({ ignore: ":hidden:not(select.chosen)" });
                $.validator.unobtrusive.parse("#edit-competition-form");

                //le nouveau prix doit etre supérieur au précédent
                var idPrec = "listPrix[" + (num_prix-1) + "].prix";
                var id = "listPrix[" + num_prix + "].prix";
                var idSuiv = "listPrix[" + (num_prix + 1) + "].prix";

                if (num_prix >= 2)
                {
                    if (num_prix > 3)
                    {
                        $(jq(id)).val(String(parseFloat($(jq(idPrec)).val().replace(/,/, '.')) + 10).replace(/\./, ','));
                    }

                    //valeur min
                    var min = parseFloat($(jq(idPrec)).val().replace(/,/, '.'));
                    if (parseFloat($(jq(id)).val().replace(/,/, '.')) < min)
                    {
                        $(jq(id)).attr("min", min);
                    }

                    //valeur min si changement
                    $(jq(id)).change(function ()
                    {
                        var min = parseFloat($(jq(idPrec)).val().replace(/,/, '.'));
                        if (parseFloat($(jq(id)).val().replace(/,/, '.')) < min)
                        {
                            $(jq(id)).attr("min", min);
                        }
                        else
                        {
                            $(jq(id)).removeAttr("min");
                        }
                    });

                    //attribut min du suivant si changement
                    $(jq(id)).change(function ()
                    {
                        if (num_prix > num)
                        {
                            var min = parseFloat($(jq(id)).val().replace(/,/, '.'));

                            if (parseFloat($(jq(idSuiv)).val().replace(/,/, '.')) < min) {
                                $(jq(id)).attr("min", min);
                            }
                            else {
                                $(jq(id)).removeAttr("min");
                            }
                        }
                    });
                }
                
            }
        });
    }

    function jq(myid) { return "#" + String(myid).replace(/(:|\.|\[|\]|,|=)/g, "\\$1"); }

    //validation des données déjà fournies
    //$("#edit-competition-form").removeData("validator");
    //$("#edit-competition-form").removeData("unobtrusiveValidation");
    //$.validator.setDefaults({ ignore: ":hidden:not(select.chosen)" });
    //$.validator.unobtrusive.parse("#edit-competition-form");
    //var Validator = $('#edit-competition-form').validate();
    //Validator.showErrors();

    //avant le submit du formulaire
    $("#edit-competition-form").submit(function (ev) {
        ev.preventDefault(); // to stop the form from submitting

        //sinon erreur =>date format incorrecte
        $(".date").attr("disabled", "disabled");
        $(".time").attr("disabled", "disabled");

        $("#edit-competition-form").removeData("validator");
        $("#edit-competition-form").removeData("unobtrusiveValidation");
        $.validator.setDefaults({ ignore: ":hidden:not(select.chosen)" });
        $.validator.unobtrusive.parse("#edit-competition-form");
        $('#edit-competition-form').validate();

        if ($('#edit-competition-form').valid()) {
            this.submit(); // If all the validations succeeded
        }
        else {
            $(".time").removeAttr("disabled");
            $(".date").removeAttr("disabled");
        }
    });

});