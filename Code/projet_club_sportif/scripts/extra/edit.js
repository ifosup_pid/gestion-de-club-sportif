﻿//fonction pour remplir les inputs dateDebut et dateFin (hidden)
function updateDates() {
    dateDebut = moment($('#dateDebut-').val(), "DD/MM/YY");
    heureDebut = moment($('#heureDebut-').val(), "HH:mm");
    dateDebut.set({
        'hour': parseInt(heureDebut.hours(), 10),
        'minute': parseInt(heureDebut.minutes(), 10),
        'second': parseInt(heureDebut.seconds(), 10)
    });
    $('#dateDebut').val(dateDebut.format("YYYY-MM-DD HH:mm:ss"));

    dateFin = moment($('#dateFin-').val(), "DD/MM/YY");
    heureFin = moment($('#heureFin-').val(), "HH:mm");
    dateFin.set({
        'hour': parseInt(heureFin.hours(), 10),
        'minute': parseInt(heureFin.minutes(), 10),
        'second': parseInt(heureFin.seconds(), 10)
    });
    $('#dateFin').val(dateFin.format("YYYY-MM-DD HH:mm:ss"));
}

//après le chargement des librairies javascripts
$(document).ready(function () {

    //remplissage des inputs date au demarrage
    dateTimeDebut = moment($('#dateDebut').val(), "DD-MM-YY HH:mm:ss");
    $('#dateDebut-').val(dateTimeDebut.format("DD/MM/YY"));
    $('#heureDebut-').val(dateTimeDebut.format("HH:mm"));
    dateTimeFin = moment($('#dateFin').val(), "DD-MM-YY HH:mm:ss");
    $('#dateFin-').val(dateTimeFin.format("DD/MM/YY"));
    $('#heureFin-').val(dateTimeFin.format("HH:mm"));

    // initialize input widgets first
    //date widget
    $('.date').datepicker({
        'dateFormat': 'dd/mm/y',
        'changeMonth': true,
        'changeYear': true,
        'autoclose': true,
        'minDate': 0,
        'defaultDate': 0
    });

    //time widget
    $('.time').timepicker({
        timeFormat: 'H:i',
        step: 5,
        'minTime': '00:00',
        maxTime: '24:00',
        defaultTime: '18:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true,
        showDuration: false,
        scrollDefault: 'now',
        forceRoundTime: true
    });

    // initialize datepair
    datepair = $('#edit-extra-form').datepair({
        'anchor': null,
        'defaultTimeDelta': '7200000',
        'updateDate': function (input, dateObj) { $(input).datepicker('setDate', dateObj); }
    });
    $('#edit-extra-form').on('rangeSelected', function () { updateDates(); });


    //pour les multiselect
    $(".chosen").chosen();

    var table = $('#table-choix-lieu').DataTable({ paging: false, bInfo: false, bautoWidth: false });

    // Apply the search
    table.columns().every(function () {
        var that = this;

        $('input', this.header()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    //validation
    $("#edit-extra-form").removeData("validator");
    $("#edit-extra-form").removeData("unobtrusiveValidation");
    $.validator.setDefaults({ ignore: ":hidden:not(select.chosen)" });
    $.validator.unobtrusive.parse("#edit-extra-form");

    //avant le submit du formulaire
    $("#edit-extra-form").submit(function (ev) {
        ev.preventDefault(); // to stop the form from submitting

        //sinon erreur =>date format incorrecte
        $(".set-no-aria-validation").attr("disabled", "disabled");

        $('#edit-extra-form').validate();
        if ($('#edit-extra-form').valid()) {
            this.submit(); // If all the validations succeeded   
        }
        else {
            $(".set-no-aria-validation").removeAttr("disabled");
        }
    });
});
