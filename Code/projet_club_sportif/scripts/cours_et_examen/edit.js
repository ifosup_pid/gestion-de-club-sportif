﻿//après le chargement des librairies javascripts
$(document).ready(function () {
    var nb_exam = $('.examen').length;//nb d'exam visible
    var num_exam = $('.examen').length;//id du prochain exam cree
    var ajax = false;//bool pour bloquer la creation d'un exam si une creation est deja en cours

    //remplissage des inputs date au demarrage
    function fillDateTimeInput(dateDebutId, heureDebutId, hiddenDebutId, dateFinId, heureFinId, hiddenFinId)
    {
        dateTimeDebut = moment($('#' + hiddenDebutId).val(), "DD-MM-YY HH:mm:ss");
        $('#' + dateDebutId).val(dateTimeDebut.format("DD/MM/YY"));
        $('#' + heureDebutId).val(dateTimeDebut.format("HH:mm"));
        dateTimeFin = moment($('#' + hiddenFinId).val(), "DD-MM-YY HH:mm:ss");
        $('#' + dateFinId).val(dateTimeFin.format("DD/MM/YY"));
        $('#' + heureFinId).val(dateTimeFin.format("HH:mm"));
    }
    //pour le cours
    fillDateTimeInput("dateDebut-", "heureDebut-", "dateDebutCours", "dateFin-", "heureFin-", "dateFinCours");
    //pour les premiers examen
    for (var i = 0; i < num_exam; i++)
    {
        fillDateTimeInput("dateDebutExam-" + i, "heureDebutExam-" + i, "dateDebutExamen-" + i, "dateFinExam-" + i, "heureFinExam-" + i, "dateFinExamen-" + i);
    }

    initializeWidgets(); //initialize les widgets avant de les lier avec datepair

    //datepair pour le cours
    var datepairCours = new Datepair(
        document.getElementById("cours"),
        {
            'anchor': 'start',
            'defaultTimeDelta': 7200000,
            'updateDate': function (input, dateObj) { $(input).datepicker('setDate', dateObj); }
        });
    // si modification de la date ou l'heure => modifie le input hidden
    $('#cours').on('rangeSelected', function () {
        updateDates("dateDebut-", "heureDebut-", "dateDebutCours", "dateFin-", "heureFin-", "dateFinCours");
    });
    updateDates("dateDebut-", "heureDebut-", "dateDebutCours", "dateFin-", "heureFin-", "dateFinCours");

    //fonction pour "datepair" les examens
    function datePairExam(numero_exam) {
        var datepairExamen = new Datepair(
            document.getElementById('Examen-' + numero_exam),
            {
                'anchor': null,
                'defaultTimeDelta': '7200000',
                'updateDate': function (input, dateObj) { $(input).datepicker('setDate', dateObj); }
            });
        // si modification de la date ou l'heure => modifie le input hidden
        $('#Examen-' + numero_exam).on('rangeSelected', function () {
            updateDates("dateDebutExam-" + numero_exam, "heureDebutExam-" + numero_exam, "dateDebutExamen-" + numero_exam,
                        "dateFinExam-" + numero_exam, "heureFinExam-" + numero_exam, "dateFinExamen-" + numero_exam);
        });
        //remplit dès le départ
        updateDates("dateDebutExam-" + numero_exam, "heureDebutExam-" + numero_exam, "dateDebutExamen-" + numero_exam,
                    "dateFinExam-" + numero_exam, "heureFinExam-" + numero_exam, "dateFinExamen-" + numero_exam);
    }

    //pour les premiers examens
    for (var i = 0; i < num_exam; i++) {
        datePairExam(i);
    }

    //clic pour ajouter un examen
    $("#AddExamButton").click(function () {
        if (ajax == false) {
            ajax = true;
            getExamen(num_exam);
        }
        ajax = false;
    });

    //clic event pour supprimer les examen
    function ExamDeleteButton(numero_exam) {
        $("#SupprimerExamenButton-" + numero_exam).click(function () {
            if (ajax == false) {
                ajax = true;
                if (nb_exam >= 2) {
                    $("#Examen-" + numero_exam).remove();
                    nb_exam--;
                }
            }
            ajax = false;
        });
    }
    //pour les premiers examens
    for (var i = 0; i < num_exam; i++) {
        ExamDeleteButton(i);
    }

    //fonction pour remplir les inputs dateDebut et dateFin (hidden)
    function updateDates(dateDebutId, heureDebutId, hiddenDebutId, dateFinId, heureFinId, hiddenFinId) {
        //date de debut
        dateDebut = moment($('#' + dateDebutId).val(), "DD/MM/YY");
        heureDebut = moment($('#' + heureDebutId).val(), "HH:mm");
        dateDebut.set({
            'hour': parseInt(heureDebut.hours(), 10),
            'minute': parseInt(heureDebut.minutes(), 10),
            'second': parseInt(heureDebut.seconds(), 10)
        });
        $('#' + hiddenDebutId).val(dateDebut.format("YYYY-MM-DD HH:mm:ss"));
        //date de fin
        dateDebut = moment($('#' + dateFinId).val(), "DD/MM/YY");
        heureDebut = moment($('#' + heureFinId).val(), "HH:mm");
        dateDebut.set({
            'hour': parseInt(heureDebut.hours(), 10),
            'minute': parseInt(heureDebut.minutes(), 10),
            'second': parseInt(heureDebut.seconds(), 10)
        });
        $('#' + hiddenFinId).val(dateDebut.format("YYYY-MM-DD HH:mm:ss"));
    }

    function initializeWidgets() {
        //date widget
        $('.date').datepicker({
            'dateFormat': 'dd/mm/y',
            'changeMonth': true,
            'changeYear': true,
            'autoclose': true,
            'minDate': 0,
            'defaultDate': 0
        });

        //time widget
        $('.time').timepicker({
            'timeFormat': 'H:i',
            'step': 5,
            'minTime': '00:00',
            'maxTime': '22:00',
            'defaultTime': '18:00',
            'dynamic': false,
            'dropdown': true,
            'scrollbar': true,
            'showDuration': false,
            'scrollDefault': 'now',
            'forceRoundTime': true
        });

        //pour les multiselect
        $(".chosen").chosen();

        var tables = $('.table-choix-lieu').DataTable({ paging: false, bInfo: false, bautoWidth: false, destroy: true });
        tables.each(function () {
            // Apply the search
            var table = this;
            table.columns().every(function () {
                var that = this;

                $('input', this.header()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });
        });
        
    }

    function getExamen(numero_exam) {
        $.ajax({
            type: "GET",
            url: "/cours_et_examen/GetExamForm",
            data: { num: numero_exam },
            dataType: "html",
            cache: false,
            success: function (data) {
                $("#examens").append(data);

                nb_exam++;
                num_exam++;

                //clic event pour supprimer l'examen
                ExamDeleteButton(numero_exam);

                //remplissage des inputs date au demarrage
                fillDateTimeInput("dateDebutExam-" + numero_exam, "heureDebutExam-" + numero_exam, "dateDebutExamen-" + numero_exam,
                                "dateFinExam-" + numero_exam, "heureFinExam-" + numero_exam, "dateFinExamen-" + numero_exam);

                initializeWidgets();

                // initialize datepair
                datePairExam(numero_exam);

                //validation
                $("#edit-cours_examen-form").removeData("validator");
                $("#edit-cours_examen-form").removeData("unobtrusiveValidation");
                $.validator.setDefaults({ ignore: ":hidden:not(select.chosen)" });
                $.validator.unobtrusive.parse("#edit-cours_examen-form");
            }
        });
    }

    $("#edit-cours_examen-form").removeData("validator");
    $("#edit-cours_examen-form").removeData("unobtrusiveValidation");
    $.validator.setDefaults({ ignore: ":hidden:not(select.chosen)" });
    $.validator.unobtrusive.parse("#edit-cours_examen-form");

    //avant le submit du formulaire
    $("#edit-cours_examen-form").submit(function (ev) {
        ev.preventDefault(); // to stop the form from submitting

        //sinon erreur =>date format incorrecte
        //$(".set-no-aria-validation").attr("disabled", "disabled");
        $(".date").attr("disabled", "disabled");
        $(".time").attr("disabled", "disabled");

        $('#edit-cours_examen-form').validate();
        if ($('#edit-cours_examen-form').valid()) {
            this.submit(); // If all the validations succeeded   
        }
        else {
            //$(".set-no-aria-validation").removeAttr("disabled");
            $(".time").removeAttr("disabled");
            $(".date").removeAttr("disabled");
        }
    });
});
