﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using projet_club_sportif.Models;

namespace projet_club_sportif.Controllers
{
    [Authorize]
    public class stagesController : Controller
    {
        private Stage stage;
        private LieuDAO lieuDao;

        public stagesController() : this(new Stage(), new LieuDAO())
        {

        }

        private stagesController(Stage unStage, LieuDAO UnLieuDao)
        {
            stage = unStage;
            lieuDao = UnLieuDao;
        }

        // GET: stages
        public ActionResult Index()
        {
            var stages = stage.GetAll();
            return View(stages.ToList());
        }

        // GET: stages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            stage.Find((int)id);
            if (stage == null)
            {
                return HttpNotFound();
            }
            return View(stage);
        }

        // GET: stages/Create
        public ActionResult Create()
        {
            ViewBag.lieux = lieuDao.GetAllForEvents();

            return View(stage);
        }

        // POST: stages/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Stage unStage)
        {
            if (ModelState.IsValid)
            {
                stage.Create(unStage);
                return RedirectToAction("Index");
            }

            return View(stage);
        }

        // GET: stages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            stage.Find((int) id);
            if (stage == null)
            {
                return HttpNotFound();
            }

            ViewBag.lieux = lieuDao.GetAllForEvents();

            return View(stage);
        }

        // POST: stages/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Stage unStage)
        {
            if (ModelState.IsValid)
            {
                stage.Find(unStage.fk_evenement);
                stage.Update(unStage);
                return RedirectToAction("Index");
            }

            return View(stage);
        }

        // GET: stages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            stage = stage.Find((int) id);
            if (stage == null)
            {
                return HttpNotFound();
            }
            return View(stage);
        }

        // POST: stages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            stage.Find(id);
            stage.Delete();
            return RedirectToAction("Index");
        }

    }
}
