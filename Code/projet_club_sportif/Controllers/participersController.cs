﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using projet_club_sportif.Models;

namespace projet_club_sportif.Controllers
{
    [Authorize]
    public class participersController : Controller
    {
        private ParticiperDAO participationDao = new ParticiperDAO();
        private Personne unMembreSelectionne;
        private Personne LemembreConnecte;

        public participersController() : this(new Personne(), new Personne())
        {

        }

        private participersController(Personne unMembre, Personne leMembre)
        {
            unMembreSelectionne = unMembre;
            LemembreConnecte = leMembre;
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            if (requestContext.HttpContext.User.Identity.IsAuthenticated)
            {
                LemembreConnecte.FindByEmail(requestContext.HttpContext.User.Identity.Name);
            }
        }

        private List<Cours> checkSection(List<Cours> theList)
        {
            List<Cours> myNewList = new List<Cours>();
            foreach(Cours myCourse in theList)
            {
                if(LemembreConnecte.age <= 8)
                {
                    if((int)myCourse.section == 1)
                    {
                        myNewList.Add(myCourse);
                    }
                }
                else if(LemembreConnecte.age <= 12)
                {
                    if ((int)myCourse.section == 2)
                    {
                        myNewList.Add(myCourse);
                    }
                }
                else
                {
                    if ((int)myCourse.section == 3)
                    {
                        myNewList.Add(myCourse);
                    }
                }
            }

            return myNewList;
        }

        ///////////////////////////////////////////////////////
        // POUR LES MEMBRES : INSCRIPTION DU MEMBRE CONNECTÉ //
        ///////////////////////////////////////////////////////


        //#######################
        //  partie inscription //
        /////////////////////////



        /// <summary>
        /// Génère la vue, puis la retourne/affiche
        /// </summary>
        /// <returns>html-vue</returns>
        public ActionResult InscriptionCours()
        {
            try
            {
                Cours donnees = new Cours();
                List<T_Participer> myListParticipation = new List<T_Participer>();
                List<Cours> myList = donnees.GetAll();
                myListParticipation = participationDao.GetAllForPers(LemembreConnecte.pk_personne);
                myList = checkSection(myList);

                List<Cours> theFinalList = new List<Cours>();
                for (int i = 0; i < myList.Count; i++)
                {
                    bool used = false;
                    EvenementDAO eventDAO = new EvenementDAO();
                    DateTime beginDate = myList[i].dateDebut;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(currentDate, beginDate);
                    if (result < 0)
                    {
                        foreach (var item in myListParticipation)
                        {
                            T_Evenement myEvent = eventDAO.Find(item.pk_evenement);
                            if (myEvent.jour == myList[i].jour)
                            {
                                int compare = DateTime.Compare(myEvent.dateDebut, myList[i].dateFin);
                                if (compare > 0)
                                {
                                    compare = DateTime.Compare(myList[i].dateDebut, myEvent.dateFin);
                                    if (compare > 0)
                                    {
                                        used = true;
                                    }
                                }
                                else
                                {
                                    used = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        used = true;
                    }
                    if (used == false)
                    {
                        theFinalList.Add(myList[i]);
                    }
                }

                return View(theFinalList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Récupère l'ID du cours sélectionné, en affiche les détails et demande la confirmation d'inscription.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> View(détail_Cours)</returns>
        public ActionResult JoinCours(int ?id)
        {
            Cours cours = new Cours();
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Cours mySub = cours.Find((int)id);
                if (cours == null)
                {
                    return HttpNotFound();
                }
                return View(mySub);
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Génère l'inscription et renvoie à la page d'inscription des cours.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("JoinCours")]
        [ValidateAntiForgeryToken]
        public ActionResult CoursConfirmed(int id)
        {
            T_Participer participation = new T_Participer();
            participation.pk_personne = LemembreConnecte.pk_personne;
            participation.pk_evenement = id;

            participationDao.Create(participation);

            return RedirectToAction("InscriptionCours");
        }



        /// <summary>
        /// Génère la vue, puis la retourne/affiche.
        /// </summary>
        /// <returns>html-vue</returns>
        public ActionResult InscriptionExamen()
        {
            try
            {
                bool subscribed = false;
                Examen examen = new Examen();
                Cours cours = new Cours();

                List<Cours> ListCourseSubscribed = new List<Cours>();
                List<Examen> ListExamsCanSubscribe = new List<Examen>();
                List<T_Participer> ListEventSubscribed = new List<T_Participer>();


                ListCourseSubscribed = cours.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                ListEventSubscribed = participationDao.GetAllForPers(LemembreConnecte.pk_personne);

                //pour chaque cours inscrit
                foreach(Cours aSubscribedCourse in ListCourseSubscribed)
                {
                    List<Examen> listExamensPourLeCours = examen.GetAllByCours(aSubscribedCourse.fk_evenement);
                    for (int i = 0; i < listExamensPourLeCours.Count; i++)
                    {
                        DateTime beginDate = listExamensPourLeCours[i].dateDebut;
                        DateTime currentDate = DateTime.Today;
                        int result = DateTime.Compare(currentDate, beginDate);

                        //si examen pas passé
                        if (result < 0)
                        {
                            //deja inscrit ?
                            subscribed = ListEventSubscribed.Exists(x => x.pk_evenement == listExamensPourLeCours[i].fk_evenement);
                        }
                        
                        //exam
                        if (subscribed == false)
                        {
                            ListExamsCanSubscribe.Add(listExamensPourLeCours[i]);
                        }
                    }
                }
                
                return View(ListExamsCanSubscribe.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Récupère l'ID de l'examen sélectionné, en affiche les détails et demande la confirmation d'inscription.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> View(détail_Cours)</returns>
        public ActionResult JoinExamen(int? id)
        {
            Examen exam = new Examen();
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Examen mySub = exam.Find((int)id);
                if (exam == null)
                {
                    return HttpNotFound();
                }
                return View(mySub);
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Génère l'inscription et renvoie à la page d'inscription des examens.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("JoinExamen")]
        [ValidateAntiForgeryToken]
        public ActionResult ExamenConfirmed(int id)
        {
            T_Participer participation = new T_Participer();
            participation.pk_personne = LemembreConnecte.pk_personne;
            participation.pk_evenement = id;

            participationDao.Create(participation);

            return RedirectToAction("InscriptionExamen");
        }

        /// <summary>
        /// Choix de la catégorie voulue par le pratiquant.
        /// </summary>
        /// <returns></returns>
        public ActionResult ChoiceCategory()
        {
            try
            {
                CategorieDAO myDAO = new CategorieDAO();
                return View(myDAO.GetAll().ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }


        /// <summary>
        /// Génère la vue, puis la retourne/affiche
        /// </summary>
        /// <returns>html-vue</returns>
        public ActionResult InscriptionCompetition(int? id)
        {
            try
            {
                Competition_CategorieDAO myCCDAO = new Competition_CategorieDAO();
                List<T_Competition_Categorie> myLIstCC = myCCDAO.GetAllForCategorie((int)id);
                List<Competition> myList = new List<Competition>();

                for (int i = 0; i < myLIstCC.Count; i++)
                {
                    Competition myComp = new Competition(); 
                    myList.Add(myComp.Find(myLIstCC[i].pk_evenement));
                }

                List<T_Participer> myListParticipation = new List<T_Participer>();
                myListParticipation = participationDao.GetAllForPers(LemembreConnecte.pk_personne);

                List<Competition> theFinalList = new List<Competition>();
                for (int i = 0; i < myList.Count; i++)
                {
                    DateTime beginDate = myList[i].dateDebut;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(currentDate, beginDate);

                    if (result < 0)
                    {
                        bool used = false;
                        foreach (var item in myListParticipation)
                        {
                            if (item.pk_evenement == myList[i].fk_evenement)
                            {
                                used = true;
                            }
                        }
                        if (used == false)
                        {
                            theFinalList.Add(myList[i]);
                        }
                    }
                }
                return View(theFinalList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Récupère l'ID de la compétition sélectionnée, en affiche les détails et demande la confirmation d'inscription.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> View(détail_Competition)</returns>
        public ActionResult JoinCompetition(int? id)
        {
            Competition competition = new Competition();
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Competition mySub = competition.Find((int)id);
                if (competition == null)
                {
                    return HttpNotFound();
                }
                return View(mySub);
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Génère l'inscription et renvoie à la page d'inscription des compétitions.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("JoinCompetition")]
        [ValidateAntiForgeryToken]
        public ActionResult CompetitionConfirmed(int id)
        {
            T_Participer myParticiaption = new T_Participer();

            myParticiaption.pk_evenement = id;
            myParticiaption.pk_personne = LemembreConnecte.pk_personne;


            participationDao.Create(myParticiaption);
            return RedirectToAction("ChoiceCategory");
        }



        /// <summary>
        /// Génère la vue, puis la retourne/affiche
        /// </summary>
        /// <returns>html-vue</returns>
        public ActionResult InscriptionExtra()
        {
            try
            {
                Extra donnees = new Extra();
                List<T_Participer> myListParticipation = new List<T_Participer>();
                var myList = donnees.GetAll();
                myListParticipation = participationDao.GetAllForPers(LemembreConnecte.pk_personne);

                List<Extra> theFinalList = new List<Extra>();
                for (int i = 0; i < myList.Count; i++)
                {
                    DateTime beginDate = myList[i].dateDebut;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(currentDate, beginDate);

                    if (result < 0)
                    {
                        bool used = false;
                        foreach (var item in myListParticipation)
                        {
                            if (item.pk_evenement == myList[i].fk_evenement)
                            {
                                used = true;
                            }
                        }
                        if (used == false)
                        {
                            theFinalList.Add(myList[i]);
                        }
                    }
                }
                return View(theFinalList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Récupère l'ID de l'extra sélectionnée, en affiche les détails et demande la confirmation d'inscription.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> View(détail_Competition)</returns>
        public ActionResult JoinExtra(int? id)
        {
            Extra extra = new Extra();
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Extra mySub = extra.Find((int)id);
                if (extra == null)
                {
                    return HttpNotFound();
                }
                return View(mySub);
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Génère l'inscription et renvoie à la page d'inscription des extras.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("JoinExtra")]
        [ValidateAntiForgeryToken]
        public ActionResult ExtraConfirmed(int id)
        {
            T_Participer myParticiaption = new T_Participer();

            T_Participer participation = new T_Participer();
            myParticiaption.pk_evenement = id;
            myParticiaption.pk_personne = LemembreConnecte.pk_personne;

            participationDao.Create(myParticiaption);
            return RedirectToAction("InscriptionExtra");
        }



        /// <summary>
        /// Génère la vue, puis la retourne/affiche
        /// </summary>
        /// <returns>html-vue</returns>
        public ActionResult InscriptionStage()
        {
            try
            {
                Stage donnees = new Stage();
                List<T_Participer> myListParticipation = new List<T_Participer>();
                var myList = donnees.GetAll();
                myListParticipation = participationDao.GetAllForPers(LemembreConnecte.pk_personne);

                List<Stage> theFinalList = new List<Stage>();
                for (int i = 0; i < myList.Count; i++)
                {
                    DateTime beginDate = myList[i].dateDebut;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(currentDate, beginDate);

                    if (result < 0)
                    {
                        bool used = false;
                        foreach (var item in myListParticipation)
                        {
                            if (item.pk_evenement == myList[i].fk_evenement)
                            {
                                used = true;
                            }
                        }
                        if (used == false)
                        {
                            theFinalList.Add(myList[i]);
                        }
                    }
                }
                return View(theFinalList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Récupère l'ID du stage sélectionné, en affiche les détails et demande la confirmation d'inscription.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> View(détail_Competition)</returns>
        public ActionResult JoinTrainee(int? id)
        {
            Stage stage = new Stage();
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Stage mySub = stage.Find((int)id);
                if (stage == null)
                {
                    return HttpNotFound();
                }
                return View(mySub);
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Génère l'inscription et renvoie à la page d'inscription des stages.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("JoinTrainee")]
        [ValidateAntiForgeryToken]
        public ActionResult StageConfirmed(int id)
        {
            T_Participer myParticiaption = new T_Participer();

            T_Participer participation = new T_Participer();
            myParticiaption.pk_evenement = id;
            myParticiaption.pk_personne = LemembreConnecte.pk_personne;

            participationDao.Create(myParticiaption);
            return RedirectToAction("InscriptionStage");
        }



        //###########################
        //  partie désinscription //
        ////////////////////////////



        /// <summary>
        /// Génère la vue, puis la retourne/affiche
        /// </summary>
        /// <returns>html-vue</returns>
        public ActionResult DesinscriptionCours()
        {
            try
            {
                Cours_Examen donnees = new Cours_Examen();
                var myList = donnees.myCours.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                return View(myList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        public ActionResult detailCours(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cours cours = new Cours();
            cours.Find((int)id);
            if (cours == null)
            {
                return HttpNotFound();
            }
            return View(cours);
        }

        /// <summary>
        /// Récupère les données émisent par l'utilisateur et les traîtes et les Ajoutent à la SGBD
        /// </summary>
        /// <param name="uneInstance"></param>
        /// <returns>vue-Index</returns>
       public ActionResult DesinscriptionValidationCours(int id_evenement)
        {
            T_Participer participation = new T_Participer();
            participation.pk_personne = LemembreConnecte.pk_personne;
            participation.pk_evenement = id_evenement;

            participationDao.Delete(participation);

            Examen myExam = new Examen();
            List<Examen> myListOfExams = new List<Examen>();

            myListOfExams = myExam.GetAllByCours(id_evenement);

            foreach(Examen item in myListOfExams)
            {
                participation.pk_personne = LemembreConnecte.pk_personne;
                participation.pk_evenement = item.pk_examen;
                participationDao.Delete(participation);
            }

            return RedirectToAction("DesinscriptionCours");
        }



        /// <summary>
        /// Génère la vue, puis la retourne/affiche.
        /// </summary>
        /// <returns></returns>
        public ActionResult DesinscriptionExamen()
        {
            try
            {
                Examen donnees = new Examen();
                var myList = donnees.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                return View(myList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        public ActionResult detailExamen(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Examen examen = new Examen();
            examen.Find((int)id);
            if (examen == null)
            {
                return HttpNotFound();
            }
            return View(examen);
        }

        /// <summary>
        /// Récupère les données émisent par l'utilisateur et les traîtes et les Ajoutent à la SGBD
        /// </summary>
        /// <param name="uneInstance"></param>
        /// <returns>vue-Index</returns>
        public ActionResult DesinscriptionValidationExamen(int id_evenement)
        {
            T_Participer participation = new T_Participer();
            participation.pk_personne = LemembreConnecte.pk_personne;
            participation.pk_evenement = id_evenement;

            participationDao.Delete(participation);

            return RedirectToAction("DesinscriptionExamen");
        }



        /// <summary>
        /// Génère la vue, puis la retourne/affiche
        /// </summary>
        /// <returns>html-vue</returns>
        public ActionResult DesinscriptionCompetition()
        {
            try
            {
                Competition donnees = new Competition();
                var myList = donnees.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                return View(myList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        public ActionResult DetailCompetition(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Competition competition = new Competition();
            competition.Find((int)id);
            if (competition == null)
            {
                return HttpNotFound();
            }
            return View(competition);
        }



        /// <summary>
        /// Récupère les données émisent par l'utilisateur et les traîtes et les Ajoutent à la SGBD
        /// </summary>
        /// <param name="uneInstance"></param>
        /// <returns>vue-Index</returns>
        public ActionResult DesinscriptionValidationCompetition(int id_evenement)
        {
            T_Participer participation = new T_Participer();
            participation.pk_personne = LemembreConnecte.pk_personne;
            participation.pk_evenement = id_evenement;

            participationDao.Delete(participation);

            return RedirectToAction("DesinscriptionCompetition");
        }

        /// <summary>
        /// Génère la vue, puis la retourne/affiche
        /// </summary>
        /// <returns>html-vue</returns>
        public ActionResult DesinscriptionExtra()
        {
            try
            {
                Extra donnees = new Extra();
                var myList = donnees.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                return View(myList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        public ActionResult detailExtra(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Extra extra = new Extra();
            extra.Find((int)id);
            if (extra == null)
            {
                return HttpNotFound();
            }
            return View(extra);
        }

        /// <summary>
        /// Récupère les données émisent par l'utilisateur et les traîtes et les Ajoutent à la SGBD
        /// </summary>
        /// <param name="uneInstance"></param>
        /// <returns>vue-Index</returns>
        public ActionResult DesinscriptionValidationExtra(int id_evenement)
        {
            T_Participer participation = new T_Participer();
            participation.pk_personne = LemembreConnecte.pk_personne;
            participation.pk_evenement = id_evenement;

            participationDao.Delete(participation);

            return RedirectToAction("DesinscriptionExtra");
        }



        /// <summary>
        /// Génère la vue, puis la retourne/affiche
        /// </summary>
        /// <returns>html-vue</returns>
        public ActionResult DesinscriptionStage()
        {
            try
            {
                Stage donnees = new Stage();
                var myList = donnees.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                return View(myList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        public ActionResult detailStage(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Stage stage = new Stage();
            stage.Find((int)id);
            if (stage == null)
            {
                return HttpNotFound();
            }
            return View(stage);
        }

        /// <summary>
        /// Récupère les données émisent par l'utilisateur et les traîtes et les Ajoutent à la SGBD
        /// </summary>
        /// <param name="uneInstance"></param>
        /// <returns>vue-Index</returns>
      
        public ActionResult DesinscriptionValidationStage(int id_evenement)
        {
            T_Participer participation = new T_Participer();
            participation.pk_personne = LemembreConnecte.pk_personne;
            participation.pk_evenement = id_evenement;

            participationDao.Delete(participation);

            return RedirectToAction("DesinscriptionStage");
        }



        /// <summary>
        /// Retourne la liste des événements auxquels est inscrite une personne.
        /// </summary>
        /// <returns></returns>
        public ActionResult Liste_Inscriptions()
        {
            try
            {
                Personne Personne = new Personne();
                Stage Stage = new Stage();
                Extra Extra = new Extra();
                Cours cours = new Cours();
                Examen exam = new Examen();
                Competition competition = new Competition();

                int compNum = 0;
                int stageNum = 0;
                int extraNum = 0;
                int coursNum = 0;
                int examNum = 0;

                List<Competition> myListCompetition = competition.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                List<Extra> myListExtra = Extra.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                List<Stage> myListStage = Stage.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                List<Cours> myListCours = cours.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                List<Examen> myListExamen = exam.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                Personne myListPersonne = Personne.Find(LemembreConnecte.pk_personne);

                List<Competition> finalListCompetition = new List<Competition>();
                List<Extra> finalListExtra = new List<Extra>();
                List<Stage> finalListStage = new List<Stage>();
                List<Cours> finalListCours = new List<Cours>();
                List<Examen> finalListExamen = new List<Examen>();

                if (myListCompetition != null)
                {
                    compNum = myListCompetition.Count;
                }
                if (myListExtra != null)
                {
                    extraNum = myListExtra.Count;
                }
                if (myListStage != null)
                {
                    stageNum = myListStage.Count;
                }
                if (myListCours != null)
                {
                    coursNum = myListCours.Count;
                }
                if (myListExamen != null)
                {
                    examNum = myListExamen.Count;
                }

                //on vérifie la date par rapport à la liste de compétitions.
                for (int i = 0; i < compNum; i++)
                {
                    DateTime endDate = myListCompetition[i].dateFin;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(endDate, currentDate);

                    if (result > 0)
                    {
                        finalListCompetition.Add(myListCompetition[i]);
                    }
                }

                //On vérifie la date par rapport à la liste d'Extras.
                for (int i = 0; i < extraNum; i++)
                {
                    DateTime endDate = myListExtra[i].dateFin;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(endDate, currentDate);

                    if (result > 0)
                    {
                        finalListExtra.Add(myListExtra[i]);
                    }
                }

                //On vérifie la date par rapport à la liste des stages.
                for (int i = 0; i < stageNum; i++)
                {
                    DateTime endDate = myListStage[i].dateFin;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(endDate, currentDate);

                    if (result > 0)
                    {
                        finalListStage.Add(myListStage[i]);
                    }
                }

                //On vérfie la date par rappport à la liste des cours.
                for (int i = 0; i < coursNum; i++)
                {
                    DateTime endDate = myListCours[i].dateFin;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(endDate, currentDate);

                    if (result > 0)
                    {
                        finalListCours.Add(myListCours[i]);
                    }
                }

                //On vérifie la date par rapport à la liste des examens.
                for (int i = 0; i < examNum; i++)
                {
                    DateTime endDate = myListExamen[i].dateFin;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(endDate, currentDate);

                    if (result > 0)
                    {
                        finalListExamen.Add(myListExamen[i]);
                    }
                }

                //On ajoute au viewBag les valeurs gardées précédement.
                ViewBag.Competition = finalListCompetition;
                ViewBag.Extra = finalListExtra;
                ViewBag.Stage = finalListStage;
                ViewBag.Personne = myListPersonne;
                ViewBag.Cours = finalListCours;
                ViewBag.Examen = finalListExamen;

                return View();
            }
            catch
            {
                return View("Erreur");
            }
        }
    }
}

