﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using projet_club_sportif.Models;

namespace projet_club_sportif.Controllers
{
    [Authorize]
    public class cours_et_examenController : Controller
    {
        private Cours_Examen CoursExamen;
        private LieuDAO lieuDao;

        public cours_et_examenController() : this(new Cours_Examen(), new LieuDAO())
        {

        }

        private cours_et_examenController(Cours_Examen UnCoursExamen, LieuDAO UnLieuDao)
        {
            CoursExamen = UnCoursExamen;
            lieuDao = UnLieuDao;
        }

        // GET: cours
        public ActionResult Index()
        {
            List<Cours_Examen> coursEtExamens = CoursExamen.GetAll();
            return View(coursEtExamens.ToList());
        }

        // GET: cours/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cours_Examen myCursus = CoursExamen.FindByCours((int)id);
            if (CoursExamen == null)
            {
                return HttpNotFound();
            }

            return View(myCursus);
        }

        /// <summary>
        ///affiche la page de création d'un nouveau cours
        /// </summary>
        /// <param name=""></param>
        /// <returns>View</returns>
        // GET: cours/Create
        public ActionResult Create()
        {
            ViewBag.lieux = lieuDao.GetAllForEvents();
            //retourne CoursExamen pour pouvoir afficher la liste des themes ect

            return View(CoursExamen);
        }


        /// <summary>
        ///affiche les champs pour un exam
        /// </summary>
        /// <param name=""></param>
        /// <returns>View</returns>
        // GET: cours/GetExamForm
        public ActionResult GetExamForm(int num)
        {
            //retourne un exam pour pouvoir afficher la liste des exams
            ViewBag.numExam = num;
            ViewBag.lieux = lieuDao.GetAllForEvents();

            return View(CoursExamen);
        }

        /// <summary>
        /// arrange l'object cours_examen, le valide et créé le cours
        /// </summary>
        /// <param name="UnCoursExamen"></param>
        /// <returns></returns>
        // POST: cours/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Cours_Examen unCours_Examen)
        {
            //variables
            var form = Request.Form;

            //partie cours
            unCours_Examen.myCours.section = (SECTION?)Convert.ToInt32(form.Get("myCours.section"));
            unCours_Examen.myCours.dateDebut = Convert.ToDateTime(form.Get("myCours.dateDebut"));
            unCours_Examen.myCours.dateFin = Convert.ToDateTime(form.Get("myCours.dateFin"));
            unCours_Examen.myCours.jour = (JOUR?)Convert.ToInt32(form.Get("myCours.jour"));
            unCours_Examen.myCours.fk_lieu = Convert.ToInt32(form.Get("myCours.fk_lieu"));
            unCours_Examen.myCours.description = form.Get("myCours.description");
            //pour les themes du cours
            string[] themesArray = form.GetValues("myCours.themesSelectedId");
            if (themesArray != null)
            {
                foreach (string s in themesArray)
                {
                    unCours_Examen.myCours.themesSelectedId.Add(int.Parse(s));
                }
            }
            //pour les organisateurs du cours
            string[] organisateursCoursArray = form.GetValues("myCours.organisateursSelectedId");
            if (organisateursCoursArray != null)
            {
                foreach (string s in organisateursCoursArray)
                {
                    unCours_Examen.myCours.organisateursSelectedId.Add(int.Parse(s));
                }
            }


            //partie examens
            string[] arrayIndex = form.GetValues("Index");
            unCours_Examen.myExamens.Clear();//?
            foreach (string unIndex in arrayIndex)
            {
                Examen unExamen = new Examen();

                unExamen.grade = (GRADE?)Convert.ToInt32(form.Get("grade["+ unIndex +"]"));
                unExamen.fk_evenement = Convert.ToInt32(form.Get("fk_evenement[" + unIndex + "]"));
                unExamen.dateDebut = Convert.ToDateTime(form.Get("dateDebutExamen[" + unIndex + "]"));
                unExamen.dateFin = Convert.ToDateTime(form.Get("dateFinExamen[" + unIndex + "]"));
                unExamen.fk_lieu = Convert.ToInt32(form.Get("fk_lieu[" + unIndex + "]"));
                unExamen.description = form.Get("description[" + unIndex + "]");

                //pour les organisateurs de l'examen
                unExamen.organisateursSelectedId.Clear();
                string[] organisateursExamArray = form.GetValues("organisateursSelectedId[" + unIndex + "]");
                if (organisateursCoursArray != null)
                {
                    foreach (string s in organisateursExamArray)
                    {
                        unExamen.organisateursSelectedId.Add(int.Parse(s));
                    }
                }

                unCours_Examen.myExamens.Add(unExamen);
            }

            //validation
            ModelState.Clear();
            TryValidateModel(unCours_Examen);

            if (ModelState.IsValid)
            {
                CoursExamen.Create(unCours_Examen);
                return RedirectToAction("Index");
            }

            return View(CoursExamen);
        }

        // GET: cours/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CoursExamen.FindByCours((int)id);

            if (CoursExamen == null)
            {
                return HttpNotFound();
            }

            ViewBag.lieux = lieuDao.GetAllForEvents();

            return View(CoursExamen);
        }

        // POST: cours/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Cours_Examen unCours_Examen)
        {
            var form = Request.Form;

            //bind du cours
            unCours_Examen.myCours.fk_evenement = Convert.ToInt32(form.Get("myCours.fk_evenement"));
            unCours_Examen.myCours.section = (SECTION?)Convert.ToInt32(form.Get("myCours.section"));
            unCours_Examen.myCours.dateDebut = Convert.ToDateTime(form.Get("myCours.dateDebut"));
            unCours_Examen.myCours.dateFin = Convert.ToDateTime(form.Get("myCours.dateFin"));
            unCours_Examen.myCours.jour = (JOUR?)Convert.ToInt32(form.Get("myCours.jour"));
            unCours_Examen.myCours.fk_lieu = Convert.ToInt32(form.Get("myCours.fk_lieu"));
            unCours_Examen.myCours.description = form.Get("myCours.description");

            //pour les themes du cours
            unCours_Examen.myCours.themesSelectedId.Clear();
            string[] themesArray = form.GetValues("myCours.themesSelectedId");
            if (themesArray != null)
            {
                foreach (string s in themesArray)
                {
                    unCours_Examen.myCours.themesSelectedId.Add(int.Parse(s));
                }
            }
            //pour les organisateurs du cours
            unCours_Examen.myCours.organisateursSelectedId.Clear();
            string[] organisateursCoursArray = form.GetValues("myCours.organisateursSelectedId");
            if (organisateursCoursArray != null)
            {
                foreach (string s in organisateursCoursArray)
                {
                    unCours_Examen.myCours.organisateursSelectedId.Add(int.Parse(s));
                }
            }


            string[] arrayIndex = form.GetValues("Index");
            unCours_Examen.myExamens.Clear();
            foreach (string unIndex in arrayIndex)
            {
                Examen unExamen = new Examen();

                unExamen.fk_cours = unCours_Examen.myCours.fk_evenement;
                unExamen.grade = (GRADE?)Convert.ToInt32(form.Get("grade[" + unIndex + "]"));
                unExamen.fk_evenement = Convert.ToInt32(form.Get("fk_evenement[" + unIndex + "]"));
                unExamen.dateDebut = Convert.ToDateTime(form.Get("dateDebutExamen[" + unIndex + "]"));
                unExamen.dateFin = Convert.ToDateTime(form.Get("dateFinExamen[" + unIndex + "]"));
                unExamen.fk_lieu = Convert.ToInt32(form.Get("fk_lieu[" + unIndex + "]"));
                unExamen.description = form.Get("description[" + unIndex + "]");
                //pour les organisateurs de l'examen
                unExamen.organisateursSelectedId.Clear();
                string[] organisateursExamArray = form.GetValues("organisateursSelectedId[" + unIndex + "]");
                if (organisateursCoursArray != null)
                {
                    foreach (string s in organisateursExamArray)
                    {
                        unExamen.organisateursSelectedId.Add(int.Parse(s));
                    }
                }

                unCours_Examen.myExamens.Add(unExamen);
            }

            //validation
            ModelState.Clear();
            TryValidateModel(unCours_Examen);

            if (ModelState.IsValid)
            {
                CoursExamen.FindByCours(Convert.ToInt32(form.Get("myCours.fk_evenement")));
                CoursExamen.Update(unCours_Examen);
                return RedirectToAction("Index");
            }

            return View(CoursExamen);
        }

        // GET: cours/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CoursExamen.FindByCours((int)id);
            if (CoursExamen == null)
            {
                return HttpNotFound();
            }

            return View(CoursExamen);
        }

        // POST: cours/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CoursExamen.FindByCours(id);
            CoursExamen.Delete();
            return RedirectToAction("Index");
        }
    }
}