﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using projet_club_sportif.Models;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace projet_club_sportif.Controllers
{
    [Authorize]
    public class compteController : Controller
    {
        private Personne unMembreSelectionne;
        private Personne LemembreConnecte;
        private ParticiperDAO participationDao;

        public compteController() : this(new Personne(), new Personne(), new ParticiperDAO())
        {

        }

        private compteController(Personne unMembre, Personne leMembre, ParticiperDAO participe)
        {
            unMembreSelectionne = unMembre;
            LemembreConnecte = leMembre;
            participationDao = participe;
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            if (requestContext.HttpContext.User.Identity.IsAuthenticated)
            {
                LemembreConnecte.FindByEmail(requestContext.HttpContext.User.Identity.Name);
            }
        }

        // GET: personnes
        public ActionResult Index()
        {
            if (!LemembreConnecte.staff)
                RedirectToAction("Index", "menu");

            return View();
        }

        // GET: personnes
        public ActionResult ListPraticing()
        {
            if (!LemembreConnecte.staff)
                return RedirectToAction("Index", "menu");

            List<Personne> personnes = unMembreSelectionne.GetAllPractising();

            return View(personnes.ToList());
        }

        // GET: personnes
        public ActionResult ListStaff()
        {
            if (!LemembreConnecte.staff)
                return RedirectToAction("Index", "menu");

            List<Personne> personnes = unMembreSelectionne.GetAllStaff();

            return View(personnes.ToList());
        }

        // GET: personnes/Details/5
        public ActionResult Details(int? id)
        {
            if (!LemembreConnecte.staff)
                return RedirectToAction("Index", "menu");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            unMembreSelectionne.Find((int) id);
            if (unMembreSelectionne == null)
            {
                return HttpNotFound();
            }

            return View(unMembreSelectionne);
        }

        // GET: personnes/Details/5
        public ActionResult Details_Staff(int? id)
        {
            if (!LemembreConnecte.staff)
                return RedirectToAction("Index", "menu");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            unMembreSelectionne.Find((int)id);
            if (unMembreSelectionne == null)
            {
                return HttpNotFound();
            }

            return View(unMembreSelectionne);
        }

        public ActionResult DetailsPDF(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (!LemembreConnecte.staff && id != LemembreConnecte.pk_personne)
            {
                
                    return RedirectToAction("Index", "menu");
            }
            int number;
            string[] array;
            var a = 0;
            unMembreSelectionne.Find((int)id);
            if (unMembreSelectionne == null)
            {
                return HttpNotFound();
            }
            creationPdf generateurpdf = new creationPdf();


            //return File(generateurpdf.créationPDF_2(personne.nom, personne.prenom), "pdf");
            generateurpdf.créationPDF_2(unMembreSelectionne.nom, unMembreSelectionne.prenom);
            generateurpdf.paragraphe("ceinture: " + unMembreSelectionne.grade.GetType().GetMember(unMembreSelectionne.grade.ToString()).First().GetCustomAttribute<DisplayAttribute>().Name);
            generateurpdf.paragraphe("poid: " + unMembreSelectionne.poids);
            generateurpdf.paragraphe("age: " + unMembreSelectionne.age);

            Cours_Examen cours = new Cours_Examen();
            var myListCours = cours.myCours.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
            Examen exam = new Examen();
            var myListExamen = exam.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
            T_Participer participation = new T_Participer();
            String[] myListReussit = new string[myListExamen.Count()];

            foreach (var item in myListExamen)
            {
                participation.pk_personne = LemembreConnecte.pk_personne;
                participation.pk_evenement = item.fk_evenement;
                var test = participationDao.Find(participation);
                if (test.reussit != null)
                {
                    myListReussit[a] = (((test.reussit == true) ? "Réussite" : "Echec"));
                }
                else
                {
                    myListReussit[a] = "Pas encore présenté";
                }
                a++;
            }
           if (myListCours != null && myListCours.Count != 0)
            {
                number = myListCours.Count;
                array = new string[number * 4];
                a = 0;
                foreach(projet_club_sportif.Models.Cours item in  myListCours)
                {
                    array[a++] = item.section.ToString();
                    array[a++] = item.dateDebut.ToString();
                    array[a++] = item.dateFin.ToString();
                }
                generateurpdf.tableau(array, "cours");
            }
            if(myListExamen != null && myListExamen.Count != 0)
            {
                number = myListExamen.Count;
                array = new string[number * 4];
                a = 0;
                var i = 0;
                foreach (projet_club_sportif.Models.Examen item in myListExamen)
                {
                    array[a++] = item.grade.ToString();
                    array[a++] = item.dateDebut.ToString();
                    array[a++] = item.dateFin.ToString();
                    array[a++] = myListReussit[i++].ToString();
                }
                generateurpdf.tableau(array, "examen");
            }
            Competition competition = new Competition();
            var myListCompetition = competition.GetAllForIdMemberParticipating(unMembreSelectionne.pk_personne);

            if (myListCompetition != null && myListCompetition.Count != 0)
            {
                number = myListCompetition.Count;
                array = new string[number *4];
                a = 0;
                foreach (projet_club_sportif.Models.Competition item in myListCompetition)
                {
                    array[a++] = item.titre.ToString();
                    array[a++] = item.dateDebut.ToString();
                    array[a++] = item.dateFin.ToString();
                }
                generateurpdf.tableau(array, "competition");
            }
            return new BinaryContentResult(generateurpdf.affichePDF(), "application/pdf");
        }  
        // GET: personnes/Create
        public ActionResult Create()
        {
            if (!LemembreConnecte.staff)
                return RedirectToAction("Index", "menu");

            return View();
        }

        // POST: personnes/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Personne unePersonne)
        {
            if (!LemembreConnecte.staff)
                return RedirectToAction("Index", "menu");

            if (ModelState.IsValid)
            {
                //check if user exist
                Personne personneExisteDeja = new Personne();
                personneExisteDeja.FindByEmail(unePersonne.email);
                if (personneExisteDeja.pk_personne != 0)
                {
                    ModelState.AddModelError("email", "l'utilisateur existe déjà");
                    return View(unePersonne);
                }

                unMembreSelectionne.Create(unePersonne);
                return RedirectToAction("Index");
            }

            return View(unMembreSelectionne);
        }

        // GET: personnes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!LemembreConnecte.staff)
                return RedirectToAction("Index", "menu");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            unMembreSelectionne.Find((int) id);
            if (unMembreSelectionne == null)
            {
                return HttpNotFound();
            }

            if (unMembreSelectionne.staff)
                return RedirectToAction("Index", "compte");

            return View(unMembreSelectionne);
        }

        // POST: personnes/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Personne unePersonne)
        {
            if (!LemembreConnecte.staff)
                return RedirectToAction("Index", "menu");

            unMembreSelectionne.Find(unePersonne.pk_personne);
            if (unMembreSelectionne == null)
            {
                return HttpNotFound();
            }

            if (unMembreSelectionne.staff)
                return RedirectToAction("Index", "compte");

            if (ModelState.IsValid)
            {
                //check if user exist
                Personne personneExisteDeja = new Personne();
                personneExisteDeja.FindByEmail(unePersonne.email);
                if (personneExisteDeja.pk_personne != 0 && personneExisteDeja.pk_personne != unePersonne.pk_personne)
                {
                    ModelState.AddModelError("email", "l'utilisateur existe déjà");
                    return View(unePersonne);
                }

                unMembreSelectionne.Update(unePersonne);
                return RedirectToAction("Instructeur_Gerer_Les_Donnees_Etudiantes");
            }

            return View(unMembreSelectionne);
        }

        // GET: personnes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!LemembreConnecte.staff)
                return RedirectToAction("Index", "menu");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            unMembreSelectionne.Find((int) id);
            if (unMembreSelectionne == null)
            {
                return HttpNotFound();
            }
            return View(unMembreSelectionne);
        }

        // POST: personnes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!LemembreConnecte.staff)
                return RedirectToAction("Index", "menu");

            unMembreSelectionne.Find(id);
            unMembreSelectionne.Delete();
            return RedirectToAction("Index");
        }

        public ActionResult Instructeur_Gerer_Les_Donnees_Etudiantes()
        {
            if (!LemembreConnecte.staff)
                return RedirectToAction("Index", "menu");

            List<Personne> personnes = unMembreSelectionne.GetAllPractising();

            return View(personnes.ToList());
        }

        // GET: personnes/Edit/5
        public ActionResult Gerer_Son_Compte()
        {
            if (LemembreConnecte.staff)
                return View("Instructeur_Gerer_Son_Compte", LemembreConnecte);
            else
                return View("Eleve_Gerer_Son_Compte", LemembreConnecte);
        }

        // POST: personnes/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Gerer_Son_Compte(Personne membreModifié)
        {
            if (LemembreConnecte.staff)
            {
                //le staff doit rester staff
                membreModifié.staff = true;
                if (ModelState.IsValid)
                {
                    //check if user exist
                    Personne personneExisteDeja = new Personne();
                    personneExisteDeja.FindByEmail(membreModifié.email);
                    if (personneExisteDeja.pk_personne != 0 && personneExisteDeja.pk_personne != membreModifié.pk_personne)
                    {
                        ModelState.AddModelError("email", "l'utilisateur existe déjà");
                        return View(membreModifié);
                    }

                    LemembreConnecte.Update(membreModifié);

                    return RedirectToAction("Index", "menu");
                }
                return View("Instructeur_Gerer_Son_Compte", membreModifié);
            }
            else
            {
                //le pratiquant ne peut pas modifier son grade, son poids, sa taille et doit rester un pratiquant
                membreModifié.grade = LemembreConnecte.grade;
                membreModifié.taille = LemembreConnecte.taille;
                membreModifié.poids = LemembreConnecte.poids;
                membreModifié.staff = false;
                if (ModelState.IsValid)
                {
                    LemembreConnecte.Update(membreModifié);

                    return RedirectToAction("Index", "menu");
                }
                return View("Eleve_Gerer_Son_Compte", membreModifié);
            }
        }

    }
}
