﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using projet_club_sportif.Models;

namespace projet_club_sportif.Controllers
{
    [Authorize]
    public class extrasController : Controller
    {
        private Extra extra;
        private LieuDAO lieuDao;

        public extrasController() : this(new Extra(), new LieuDAO())
        {

        }

        private extrasController(Extra UnExtra, LieuDAO UnLieuDao)
        {
            extra = UnExtra;
            lieuDao = UnLieuDao;
        }

        // GET: extras
        public ActionResult Index()
        {
            var extras = extra.GetAll();

            return View(extras.ToList());
        }

        // GET: extras/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Extra myExtra = extra.Find((int)id);
            if (extra == null)
            {
                return HttpNotFound();
            }
            return View(myExtra);
        }

        // GET: extras/Create
        public ActionResult Create()
        {
            ViewBag.lieux = lieuDao.GetAllForEvents();

            return View(extra);
        }

        // POST: extras/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Extra unExtra)
        {
            if (ModelState.IsValid)
            {
                extra.Create(unExtra);
                return RedirectToAction("Index");
            }

            return View(extra);
        }

        // GET: extras/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            extra.Find((int)id);
            if (extra == null)
            {
                return HttpNotFound();
            }

            ViewBag.lieux = lieuDao.GetAllForEvents();

            return View(extra);
        }

        // POST: extras/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Extra unExtra)
        {
            if (ModelState.IsValid)
            {
                extra.Find(unExtra.fk_evenement);
                extra.Update(unExtra);
                return RedirectToAction("Index");
            }

            return View(extra);
        }

        // GET: extras/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            extra.Find((int)id);
            if (extra == null)
            {
                return HttpNotFound();
            }
            return View(extra);
        }

        // POST: extras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            extra.Find(id);
            extra.Delete();
            return RedirectToAction("Index");
        }
    }
}
