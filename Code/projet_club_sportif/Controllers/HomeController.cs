﻿using System.Web.Mvc;
using projet_club_sportif.Models;
using projet_club_sportif.ViewModels;

namespace projet_club_sportif.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        private PersonneViewModel leViewModel;
        private PersonneDAO PersonneDao;

        public HomeController() : this(new PersonneDAO())
        {

        }

        public HomeController(PersonneDAO UnPersonneDao)
        {
            PersonneDao = UnPersonneDao;
        }

        public ActionResult Index()
        {
            leViewModel = new PersonneViewModel { Authentifie = HttpContext.User.Identity.IsAuthenticated };
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                leViewModel.Personne = PersonneDao.FindByEmail(HttpContext.User.Identity.Name);

                if (leViewModel.Personne.staff)
                    return RedirectToAction("Instructeur", "menu");
                else
                    return RedirectToAction("Eleve", "menu");
            }
            return View(leViewModel);

            
        }
    }
}