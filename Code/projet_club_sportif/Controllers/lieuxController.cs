﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using projet_club_sportif.Models;

namespace projet_club_sportif.Controllers
{
    [Authorize]
    public class lieuxController : Controller
    {
        private LieuDAO lieuDao;

        public lieuxController() : this(new LieuDAO())
        {

        }

        private lieuxController(LieuDAO UnLieuDao)
        {
            lieuDao = UnLieuDao;
        }

        // GET: lieux
        public ActionResult Index()
        {
            return View(lieuDao.GetAllForEvents().ToList());
        }

        // GET: lieux/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T_Lieu lieu = lieuDao.Find((int)id);
            if (lieu == null)
            {
                return HttpNotFound();
            }
            return View(lieu);
        }

        // GET: lieux/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: lieux/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(T_Lieu lieu)
        {
            lieu.residentielle = false;
            if (ModelState.IsValid)
            {
                lieuDao.Create(lieu);
                return RedirectToAction("Index");
            }

            return View(lieu);
        }

        // GET: lieux/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T_Lieu lieu = lieuDao.Find((int)id);
            if (lieu == null)
            {
                return HttpNotFound();
            }
            return View(lieu);
        }

        // POST: lieux/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(T_Lieu lieu)
        {
            lieu.residentielle = false;

            if (ModelState.IsValid)
            {
                lieuDao.Update(lieu);
                return RedirectToAction("Index");
            }
            return View(lieu);
        }

        // GET: lieux/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T_Lieu lieu = lieuDao.Find((int)id);
            if (lieu == null)
            {
                return HttpNotFound();
            }
            return View(lieu);
        }

        // POST: lieux/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            T_Lieu lieu = lieuDao.Find(id);
            lieuDao.Delete(lieu);
            return RedirectToAction("Index");
        }

    }
}
