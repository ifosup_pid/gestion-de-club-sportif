﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using projet_club_sportif.Models;
using projet_club_sportif.ViewModels;
using System.Net;

namespace projet_club_sportif.Controllers
{
    [Authorize]
    public class menuController : Controller
    {

        private PersonneDAO PersonneDao;
        private PersonneViewModel leViewModel;
        private Personne LemembreConnecte;
        private Personne unMembreSelectionne;
        private ParticiperDAO participationDao;

        public menuController() : this(new PersonneDAO(), new Personne(), new Personne(), new ParticiperDAO())
        {

        }

        private menuController(PersonneDAO UnPersonneDao, Personne leMembre, Personne unMembre, ParticiperDAO participe)
        {
            PersonneDao = UnPersonneDao;
            LemembreConnecte = leMembre;
            unMembreSelectionne = unMembre;
            participationDao = participe;
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            if (requestContext.HttpContext.User.Identity.IsAuthenticated)
            {
                LemembreConnecte.FindByEmail(requestContext.HttpContext.User.Identity.Name);
            }
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            leViewModel = new PersonneViewModel { Authentifie = HttpContext.User.Identity.IsAuthenticated };
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                leViewModel.Personne = PersonneDao.FindByEmail(HttpContext.User.Identity.Name);
                if (leViewModel.Personne.staff)
                    return View("Instructeur");
                else
                    return View("Eleve");
            }

            return RedirectToAction("Index", "Home");
        }

        [ChildActionOnly]
        [AllowAnonymous]
        public ActionResult _menu()
        {
            leViewModel = new PersonneViewModel { Authentifie = HttpContext.User.Identity.IsAuthenticated };
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                leViewModel.Personne = PersonneDao.FindByEmail(HttpContext.User.Identity.Name);
            }
            return PartialView(leViewModel);
        }

        public ActionResult Instructeur()
        {
            return View();
        }

        public ActionResult Eleve()
        {
            return View();
        }
        
        public ActionResult Gerer_Ses_Inscriptions()
        {
            return View();
        }

        public ActionResult Se_Desinscrire_D_Un_Evenement()
        {
            return View();
        }

        public ActionResult S_Inscrire_A_Un_Evenement()
        {
            return View();
        }

        public ActionResult Instructeur_Gerer_Historique()
        {
            return View();
        }

        public ActionResult Eleve_Consulter_Son_Historique()
        {
            var i = 0;
            try
            {
                Cours_Examen cours = new Cours_Examen();
                Examen exam = new Examen();
                var myListCours = cours.myCours.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                var myListExamen = exam.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);

                String[] myListReussit = new string[myListExamen.Count()];
                T_Participer participation = new T_Participer();
                foreach (var item in myListExamen)
                {
                    participation.pk_personne = LemembreConnecte.pk_personne;
                    participation.pk_evenement = item.fk_evenement;
                    var test = participationDao.Find(participation);
                    if (test.reussit != null)
                    {
                        myListReussit[i] = (((test.reussit == true) ? "Réussite" : "Echec"));
                    }
                    else
                    {
                        myListReussit[i] = "Pas encore présenté";
                    }
                    i++;
                }
                Competition competition = new Competition();
                var myListCompetition = competition.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                Extra Extra = new Extra();
                var myListExtra = Extra.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                Stage Stage = new Stage();
                var myListStage = Stage.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                Personne Personne = new Personne();
                var myListPersonne = Personne.Find(LemembreConnecte.pk_personne);
                ViewBag.Competition = myListCompetition;
                ViewBag.Extra = myListExtra;
                ViewBag.Stage = myListStage;
                ViewBag.Personne = myListPersonne;
                ViewBag.Cours = myListCours;
                ViewBag.Examen = myListExamen;
                ViewBag.Reussit = myListReussit;
                return View();

            }
            catch
            {
                return View("Erreur");
            }
        }
        
        public ActionResult Instructeur_Consulter_Son_Historique()
        {
            var i = 0;
            try
            {
                Cours_Examen cours = new Cours_Examen();
                Examen exam = new Examen();
                var myListCours = cours.myCours.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                var myListExamen = exam.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);

                String[] myListReussit = new string[myListExamen.Count()];
                T_Participer participation = new T_Participer();
                foreach (var item in myListExamen)
                {
                    participation.pk_personne = LemembreConnecte.pk_personne;
                    participation.pk_evenement = item.fk_evenement;
                    var test = participationDao.Find(participation);
                    if (test.reussit != null)
                    {
                        myListReussit[i] = (((test.reussit == true) ? "Réussite" : "Echec"));
                    }
                    else
                    {
                        myListReussit[i] = "Pas encore présenté";
                    }
                    i++;
                }
                Competition competition = new Competition();
                var myListCompetition = competition.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                Extra Extra = new Extra();
                var myListExtra = Extra.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                Stage Stage = new Stage();
                var myListStage = Stage.GetAllForIdMemberParticipating(LemembreConnecte.pk_personne);
                Personne Personne = new Personne();
                var myListPersonne = Personne.Find(LemembreConnecte.pk_personne);
                ViewBag.Competition = myListCompetition;
                ViewBag.Extra = myListExtra;
                ViewBag.Stage = myListStage;
                ViewBag.Personne = myListPersonne;
                ViewBag.Cours = myListCours;
                ViewBag.Examen = myListExamen;
                ViewBag.Reussit = myListReussit;
                return View();

            }
            catch
            {
                return View("Erreur");
            }
        }

        public ActionResult Instructeur_consulter_historique_etudiant()
        {
            if (!LemembreConnecte.staff)
                return RedirectToAction("Index", "menu");

            List<Personne> personnes = unMembreSelectionne.GetAllPractising();
            Personne Personne = new Personne();
            var myListPersonne = Personne.Find(LemembreConnecte.pk_personne);
            ViewBag.Personne = myListPersonne;
            return View(personnes.ToList());
        }

        public ActionResult Instructeur_consulte_historique_dun_etudiant(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (!LemembreConnecte.staff && id != LemembreConnecte.pk_personne)
            {

                return RedirectToAction("Index", "menu");
            }
            var i = 0;
            try
            {
                unMembreSelectionne.Find((int)id);
                Cours_Examen cours = new Cours_Examen();
                Examen exam = new Examen();
                var myListCours = cours.myCours.GetAllForIdMemberParticipating(unMembreSelectionne.pk_personne);
                var myListExamen = exam.GetAllForIdMemberParticipating(unMembreSelectionne.pk_personne);

                String[] myListReussit = new string[myListExamen.Count()];
                T_Participer participation = new T_Participer();
                foreach (var item in myListExamen)
                {
                    participation.pk_personne = unMembreSelectionne.pk_personne;
                    participation.pk_evenement = item.fk_evenement;
                    var test = participationDao.Find(participation);
                    if (test.reussit != null)
                    {
                        myListReussit[i] = (((test.reussit == true) ? "Réussite" : "Echec"));
                    }
                    else
                    {
                        myListReussit[i] = "Pas encore présenté";
                    }
                    i++;
                }
                Competition competition = new Competition();
                var myListCompetition = competition.GetAllForIdMemberParticipating(unMembreSelectionne.pk_personne);
                Extra Extra = new Extra();
                var myListExtra = Extra.GetAllForIdMemberParticipating(unMembreSelectionne.pk_personne);
                Stage Stage = new Stage();
                var myListStage = Stage.GetAllForIdMemberParticipating(unMembreSelectionne.pk_personne);
                Personne Personne = new Personne();
                var myListPersonne = Personne.Find(unMembreSelectionne.pk_personne);
                ViewBag.Competition = myListCompetition;
                ViewBag.Extra = myListExtra;
                ViewBag.Stage = myListStage;
                ViewBag.Personne = myListPersonne;
                ViewBag.Cours = myListCours;
                ViewBag.Examen = myListExamen;
                ViewBag.Reussit = myListReussit;
                return View();

            }
            catch
            {
                return View("Erreur");
            }
        }

        public ActionResult modifier_historique_Examen(int id_evenement, int id_personne)
        {
            T_Participer participation = new T_Participer();
            participation.pk_personne = id_personne;
            participation.pk_evenement = id_evenement;

            participation = participationDao.Find(participation);
            participation.reussit = (((participation.reussit == true) ? false : true));
            participationDao.Delete(participation);
            participationDao.Create(participation);
            return RedirectToAction("Instructeur_consulter_historique_etudiant", "menu"); 
        }

        public ActionResult reussit_historique(int id_evenement, int id_personne)
        {
            T_Participer participation = new T_Participer();
            participation.pk_personne = id_personne;
            participation.pk_evenement = id_evenement;

            participation = participationDao.Find(participation);
            participation.reussit = true;
            participationDao.Delete(participation);
            participationDao.Create(participation);
            return RedirectToAction("Instructeur_consulter_historique_etudiant", "menu");
        }

        public ActionResult Echec_historique(int id_evenement, int id_personne)
        {
            T_Participer participation = new T_Participer();
            participation.pk_personne = id_personne;
            participation.pk_evenement = id_evenement;

            participation = participationDao.Find(participation);
            participation.reussit = false;
            participationDao.Delete(participation);
            participationDao.Create(participation);
            return RedirectToAction("Instructeur_consulter_historique_etudiant", "menu");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult modifier_historique_Examen(T_Participer participation)
        {
            participationDao.Delete(participation);
            participationDao.Create(participation);

            return View(participation);
        }


        public ActionResult créerUneEntrer_historique(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (!LemembreConnecte.staff && id != LemembreConnecte.pk_personne)
            {

                return RedirectToAction("Index", "menu");
            }
            

            unMembreSelectionne.Find((int)id);
            Examen exam = new Examen();
            var myListExamen = exam.GetAllForIdMemberParticipating(unMembreSelectionne.pk_personne);

            List<Examen> MyListExamenNull = new List<Examen>();

            T_Participer participation = new T_Participer();
            foreach (var item in myListExamen)
            {
                participation.pk_personne = unMembreSelectionne.pk_personne;
                participation.pk_evenement = item.fk_evenement;
                var test = participationDao.Find(participation);
                if (test.reussit == null)
                {
                    MyListExamenNull.Add(item);
                }
                
            }
            Personne Personne = new Personne();
            var myListPersonne = Personne.Find(unMembreSelectionne.pk_personne);
            ViewBag.Personne = myListPersonne;
            return View(MyListExamenNull);
        }
        public ActionResult modifier_historique(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (!LemembreConnecte.staff && id != LemembreConnecte.pk_personne)
            {

                return RedirectToAction("Index", "menu");
            }
            var i = 0;
            try
            {
                unMembreSelectionne.Find((int)id);
                Cours_Examen cours = new Cours_Examen();
                Examen exam = new Examen();
                var myListCours = cours.myCours.GetAllForIdMemberParticipating(unMembreSelectionne.pk_personne);
                var myListExamen = exam.GetAllForIdMemberParticipating(unMembreSelectionne.pk_personne);

                List<Examen> MyListExamenResultat = new List<Examen>();

                String[] myListReussit = new string[myListExamen.Count()];
                T_Participer participation = new T_Participer();
                foreach (var item in myListExamen)
                {
                    participation.pk_personne = unMembreSelectionne.pk_personne;
                    participation.pk_evenement = item.fk_evenement;
                    var test = participationDao.Find(participation);
                   if (test.reussit != null)
                    {
                        myListReussit[i] = (((test.reussit == true) ? "Réussite" : "Echec"));
                        MyListExamenResultat.Add(item);
                        i++;
                    }

                }
                Competition competition = new Competition();
                var myListCompetition = competition.GetAllForIdMemberParticipating(unMembreSelectionne.pk_personne);
                Extra Extra = new Extra();
                var myListExtra = Extra.GetAllForIdMemberParticipating(unMembreSelectionne.pk_personne);
                Stage Stage = new Stage();
                var myListStage = Stage.GetAllForIdMemberParticipating(unMembreSelectionne.pk_personne);
                Personne Personne = new Personne();
                var myListPersonne = Personne.Find(unMembreSelectionne.pk_personne);
                ViewBag.Competition = myListCompetition;
                ViewBag.Extra = myListExtra;
                ViewBag.Stage = myListStage;
                ViewBag.Personne = myListPersonne;
                ViewBag.Cours = myListCours;
                ViewBag.Examen = MyListExamenResultat;
                ViewBag.Reussit = myListReussit;
                return View();
            }
            catch
            {
                return View("Erreur");
            }
        }
       

        public ActionResult Instructeur_Gerer_Les_Evenements()
        {
            return View();
        }

        public ActionResult Instructeur_Gerer_Inscription_Eleve()
        {
            return View();
        }
    }
}