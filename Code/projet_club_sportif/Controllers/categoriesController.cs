﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using projet_club_sportif.Models;

namespace projet_club_sportif.Controllers
{
    [Authorize]
    public class categoriesController : Controller
    {
        private DAO<T_Categorie> CategorieDao;

        public categoriesController() : this(new CategorieDAO())
        {

        }

        private categoriesController(CategorieDAO UnCategorieDao)
        {
            CategorieDao = UnCategorieDao;
        }

        // GET: categories
        public ActionResult Index()
        {
            return View(CategorieDao.GetAll().ToList());
        }

        // GET: categories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T_Categorie categorie = CategorieDao.Find((int) id);
            if (categorie == null)
            {
                return HttpNotFound();
            }
            return View(categorie);
        }

        // GET: categories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: categories/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(T_Categorie uneCategorie)
        {
            if (ModelState.IsValid)
            {
                CategorieDao.Create(uneCategorie);
                return RedirectToAction("Index");
            }

            return View(uneCategorie);
        }

        // GET: categories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T_Categorie categorie = CategorieDao.Find((int) id);
            if (categorie == null)
            {
                return HttpNotFound();
            }
            return View(categorie);
        }

        // POST: categories/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(T_Categorie uneCategorie)
        {
            if (ModelState.IsValid)
            {
                CategorieDao.Update(uneCategorie);
                return RedirectToAction("Index");
            }
            return View(uneCategorie);
        }

        // GET: categories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T_Categorie categorie = CategorieDao.Find((int) id);
            if (categorie == null)
            {
                return HttpNotFound();
            }
            return View(categorie);
        }

        // POST: categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            T_Categorie categorie = CategorieDao.Find(id);
            CategorieDao.Delete(categorie);
            return RedirectToAction("Index");
        }
    }
}
