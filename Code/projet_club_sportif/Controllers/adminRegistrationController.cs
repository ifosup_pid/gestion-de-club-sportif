﻿using projet_club_sportif.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace projet_club_sportif.Controllers
{
    [Authorize]
    public class adminRegistrationController : Controller
    {

        private ParticiperDAO participationDao = new ParticiperDAO();
        private Personne unMembreSelectionne;
        private Personne LemembreConnecte;
        private static int newEvent;
        private static int idPerson = 0;

        public adminRegistrationController() : this(new Personne(), new Personne())
        {

        }

        private adminRegistrationController(Personne unMembre, Personne leMembre)
        {
            unMembreSelectionne = unMembre;
            LemembreConnecte = leMembre;
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            if (requestContext.HttpContext.User.Identity.IsAuthenticated)
            {
                LemembreConnecte.FindByEmail(requestContext.HttpContext.User.Identity.Name);
            }
        }

        public ActionResult DeregistrationList()
        {
            return View();
        }

        public ActionResult RegistrationList()
        {
            return View();
        }



        /////////////////////////////////////////////////////////////
        //PARTIE INSCRIPTION D'UN PRATIQUANT PAR UN MEMBRE DU STAFF//
        /////////////////////////////////////////////////////////////


        /// <summary>
        /// Affiche la liste des Cours disponibles.
        /// </summary>
        /// <returns></returns>
        public ActionResult RegistrationCourse()
        {
            try
            {
                Cours donnees = new Cours();
                var myList = donnees.GetAll();

                List<Cours> theFinalList = new List<Cours>();
                for (int i = 0; i < myList.Count; i++)
                {
                    DateTime beginDate = myList[i].dateDebut;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(currentDate, beginDate);

                    if (result < 0)
                    {
                        theFinalList.Add(myList[i]);
                    }
                }
                return View(theFinalList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Choix de la catégorie voulue pour le pratiquant.
        /// </summary>
        /// <returns></returns>
        public ActionResult ChoiceCategory()
        {
            try
            {
                CategorieDAO myDAO = new CategorieDAO();
                return View(myDAO.GetAll().ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Affiche la liste des compétitions disponibles.
        /// </summary>
        /// <returns></returns>
        public ActionResult RegistrationCompetition(int? id)
        {
            try
            {
                Competition_CategorieDAO myCCDAO = new Competition_CategorieDAO();
                List<T_Competition_Categorie> myLIstCC = myCCDAO.GetAllForCategorie((int)id);
                List<Competition> myList = new List<Competition>();

                for (int i = 0; i < myLIstCC.Count; i++)
                {
                    Competition myComp = new Competition();
                    myList.Add(myComp.Find(myLIstCC[i].pk_evenement));
                }

                List<Competition> theFinalList = new List<Competition>();
                for (int i = 0; i < myList.Count; i++)
                {
                    DateTime beginDate = myList[i].dateDebut;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(currentDate, beginDate);

                    if (result < 0)
                    {
                        theFinalList.Add(myList[i]);
                    }
                }
                return View(theFinalList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }


        /// <summary>
        /// Affiche la liste des stages disponibles.
        /// </summary>
        /// <returns></returns>
        public ActionResult RegistrationTrainee()
        {
            try
            {
                Stage donnees = new Stage(); ;
                var myList = donnees.GetAll();

                List<Stage> theFinalList = new List<Stage>();
                for (int i = 0; i < myList.Count; i++)
                {
                    DateTime beginDate = myList[i].dateDebut;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(currentDate, beginDate);

                    if (result < 0)
                    {
                        theFinalList.Add(myList[i]);
                    }
                }
                return View(theFinalList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Affiche la liste des extras disponibles.
        /// </summary>
        /// <returns></returns>
        public ActionResult RegistrationExtra()
        {
            try
            {
                Extra donnees = new Extra();
                var myList = donnees.GetAll();

                List<Extra> theFinalList = new List<Extra>();
                for (int i = 0; i < myList.Count; i++)
                {
                    DateTime beginDate = myList[i].dateDebut;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(currentDate, beginDate);

                    if (result < 0)
                    {
                        theFinalList.Add(myList[i]);
                    }
                }
                return View(theFinalList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Affiche la liste des étudiants.
        /// </summary>
        /// <returns></returns>
        public ActionResult RegistrationTest()
        {
            try
            {
                List<Personne> myList = new List<Personne>();
                Personne onePerson = new Personne();
                myList = onePerson.GetAllPractising();

                return View(myList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }


        /// <summary>
        /// Affiche la liste des examens disponible pour l'étudiant sélectionné.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult JoinTest(int? id)
        {
            try
            {
                Cours returnCourse = new Models.Cours();
                Examen returnTest = new Examen();
                List<Cours> myListOfCourse = new List<Cours>();
                List<Examen> myListOfTest = new List<Examen>();

                idPerson = (int)id;
                myListOfCourse = returnCourse.GetAllForIdMemberParticipating((int)id);
                foreach (Cours myCourse in myListOfCourse)
                {
                    myListOfTest = returnTest.GetAllByCours(myCourse.fk_evenement);
                }

                return View(myListOfTest.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Enregistre l'inscription et retourne à la liste.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ValidRegistrationTest(int? id)
        {
            try
            {
                T_Participer participation = new T_Participer();
                participation.pk_personne = idPerson;
                participation.pk_evenement = (int)id;
                participationDao.Create(participation);

                return View("RegistrationList");
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Affiche la liste des étudiants pouvant participer à l'événement.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult JoinEvent(int? id)
        {
            try
            {
                newEvent = (int)id;
                Personne myPerson = new Personne();
                ParticiperDAO myDAO = new ParticiperDAO();
                List<T_Participer> myListOfParticipation = myDAO.GetAllForEvent((int)id);
                List<Personne> myListOfPerson = myPerson.GetAllPractising();
                List<Personne> myFinalList = new List<Personne>();
                
                for(int i = 0; i < myListOfPerson.Count; i++)
                {
                    bool used = false;
                    for(int z = 0; z < myListOfParticipation.Count; z++)
                    {
                        if(myListOfPerson[i].pk_personne == myListOfParticipation[z].pk_personne)
                        {
                            used = true;
                        }
                    }
                    if(used == false)
                    {
                        myFinalList.Add(myListOfPerson[i]);
                    }
                }

                return View(myFinalList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Créée la nouvelle participation du pratiquant à l'événement choisi.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ValidRegistration(int? id)
        {
            try
            {
                T_Participer participation = new T_Participer();
                participation.pk_personne = (int)id;
                participation.pk_evenement = newEvent;

                participationDao.Create(participation);
                return View("RegistrationList");
            }
            catch
            {
                return View("Erreur");
            }
        }

        ///////////////////////////////////////////////////////////////   
        //PARTIE DESINSCRIPTION D'UN PRATIQUANT PAR UN MEMBRE DU STAFF//
        ///////////////////////////////////////////////////////////////



        /// <summary>
        /// Retourne la liste des cours.
        /// </summary>
        /// <returns></returns>
        public ActionResult DeregistrationCourse()
        {
            try
            {
                Cours donnees = new Cours();
                var myList = donnees.GetAll();

                List<Cours> theFinalList = new List<Cours>();
                for (int i = 0; i < myList.Count; i++)
                {
                    DateTime beginDate = myList[i].dateDebut;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(currentDate, beginDate);

                    if (result < 0)
                    {
                        theFinalList.Add(myList[i]);
                    }
                }
                return View(theFinalList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Retourne la liste des Stages.
        /// </summary>
        /// <returns></returns>
        public ActionResult DeregistrationTrainee()
        {
            try
            {
                Stage donnees = new Stage(); ;
                var myList = donnees.GetAll();

                List<Stage> theFinalList = new List<Stage>();
                for (int i = 0; i < myList.Count; i++)
                {
                    DateTime beginDate = myList[i].dateDebut;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(currentDate, beginDate);

                    if (result < 0)
                    {
                        theFinalList.Add(myList[i]);
                    }
                }
                return View(theFinalList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Affiche la liste des étudiants pouvant être désinscrits du Stage.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult LeaveTrainee(int? id)
        {
            return View();
        }

        /// <summary>
        /// Retourne la liste des Examens.
        /// </summary>
        /// <returns></returns>
        public ActionResult DeregistrationTest()
        {
            return View();
        }

        /// <summary>
        /// Affiche la liste des étudiants pouvant être désinscrits de l'Examen.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult LeaveTest(int? id)
        {
            return View();
        }

        /// <summary>
        /// Retourne la liste des Compétitions.
        /// </summary>
        /// <returns></returns>
        public ActionResult DeregistrationCompetition()
        {
            try
            {
                Competition donnees = new Competition();
                var myList = donnees.GetAll();

                List<Competition> theFinalList = new List<Competition>();
                for (int i = 0; i < myList.Count; i++)
                {
                    DateTime beginDate = myList[i].dateDebut;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(currentDate, beginDate);

                    if (result < 0)
                    {
                        theFinalList.Add(myList[i]);
                    }
                }
                return View(theFinalList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Affiche la liste des étudiants pouvant être désinscrits de la Compétition.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult LeaveCompetition(int? id)
        {
            return View();
        }

        /// <summary>
        /// Retourne la liste des Extras.
        /// </summary>
        /// <returns></returns>
        public ActionResult DeregistrationExtra()
        {
            try
            {
                Extra donnees = new Extra();
                var myList = donnees.GetAll();

                List<Extra> theFinalList = new List<Extra>();
                for (int i = 0; i < myList.Count; i++)
                {
                    DateTime beginDate = myList[i].dateDebut;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(currentDate, beginDate);

                    if (result < 0)
                    {
                        theFinalList.Add(myList[i]);
                    }
                }
                return View(theFinalList.ToList());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Affiche la liste des étudiants pouvant être désinscrits de l'Extra.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult LeaveExtra(int? id)
        {
            return View();
        }

        /// <summary>
        /// Affiche la liste des étudiants pouvant être désinscrits de l'Event.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult LeaveEvent(int? id)
        {
            try
            {
                newEvent = (int)id;
                Personne myPerson = new Personne();
                return View(myPerson.ParticipantsList(newEvent));
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Affiche la liste des étudiants pouvant tre désinscrits du cours.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult LeaveCourse(int? id)
        {
            try
            {
                newEvent = (int)id;
                Personne myPerson = new Personne();
                return View(myPerson.ParticipantsList(newEvent));
            }
            catch
            {
                return View("Erreur");
            }
        }
        
        public ActionResult ValidDeregistrationCourse(int? id)
        {
            try
            {
                T_Participer participation = new T_Participer();
                participation.pk_personne = (int)id;
                participation.pk_evenement = newEvent;

                participationDao.Delete(participation);

                Examen myExam = new Examen();
                List<Examen> myListOfExams = new List<Examen>();

                myListOfExams = myExam.GetAllByCours(newEvent);

                foreach (Examen item in myListOfExams)
                {
                    participation.pk_personne = (int)id;
                    participation.pk_evenement = item.pk_examen;
                    participationDao.Delete(participation);
                }

                return View("DeregistrationList");
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Fait la désinscription puis retourne à l aliste.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ValidDeregistration(int ?id)
        {
            try
            {
                T_Participer tParticiper = new T_Participer();

                tParticiper.pk_evenement = newEvent;
                tParticiper.pk_personne = (int)id;

                ParticiperDAO myParticipation = new ParticiperDAO();
                myParticipation.Delete(tParticiper);

                return View("DeregistrationList");
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Affiche la liste des pratiquants
        /// </summary>
        /// <returns></returns>
        public ActionResult studentList()
        {
            try
            {
                Personne myPerson = new Personne();
                return View(myPerson.GetAllPractising());
            }
            catch
            {
                return View("Erreur");
            }
        }

        /// <summary>
        /// Affiche les événements auxquels un pratiquant est inscrit.
        /// </summary>
        /// <returns></returns>
        public ActionResult ScreenList(int? id)
        {
            try
            {
                Personne Personne = new Personne();
                Stage Stage = new Stage();
                Extra Extra = new Extra();
                Cours cours = new Cours();
                Examen exam = new Examen();
                Competition competition = new Competition();

                int compNum = 0;
                int stageNum = 0;
                int extraNum = 0;
                int coursNum = 0;
                int examNum = 0;

                List<Competition> myListCompetition = competition.GetAllForIdMemberParticipating((int)id);
                List<Extra> myListExtra = Extra.GetAllForIdMemberParticipating((int)id);
                List<Stage> myListStage = Stage.GetAllForIdMemberParticipating((int)id);
                List<Cours> myListCours = cours.GetAllForIdMemberParticipating((int)id);
                List<Examen> myListExamen = exam.GetAllForIdMemberParticipating((int)id);
                Personne myListPersonne = Personne.Find((int) id);

                List<Competition> finalListCompetition = new List<Competition>();
                List<Extra> finalListExtra = new List<Extra>();
                List<Stage> finalListStage = new List<Stage>();
                List<Cours> finalListCours = new List<Cours>();
                List<Examen> finalListExamen = new List<Examen>();

                if (myListCompetition != null)
                {
                    compNum = myListCompetition.Count;
                }
                if (myListExtra != null)
                {
                    extraNum = myListExtra.Count;
                }
                if (myListStage != null)
                {
                    stageNum = myListStage.Count;
                }
                if(myListCours != null)
                {
                    coursNum = myListCours.Count;
                }
                if(myListExamen != null)
                {
                    examNum = myListExamen.Count;
                }

                //on vérifie la date par rapport à la liste de compétitions.
                for (int i = 0; i < compNum; i++)
                {
                    DateTime endDate = myListCompetition[i].dateFin;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(endDate, currentDate);

                    if (result > 0)
                    {
                        finalListCompetition.Add(myListCompetition[i]);
                    }
                }

                //On vérifie la date par rapport à la liste d'Extras.
                for (int i = 0; i < extraNum; i++)
                {
                    DateTime endDate = myListExtra[i].dateFin;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(endDate, currentDate);

                    if (result > 0)
                    {
                        finalListExtra.Add(myListExtra[i]);
                    }
                }

                //On vérifie la date par rapport à la liste des stages.
                for (int i = 0; i < stageNum; i++)
                {
                    DateTime endDate = myListStage[i].dateFin;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(endDate, currentDate);

                    if (result > 0)
                    {
                        finalListStage.Add(myListStage[i]);
                    }
                }

                //On vérfie la date par rappport à la liste des cours.
                for (int i = 0; i < coursNum; i++)
                {
                    DateTime endDate = myListCours[i].dateFin;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(endDate, currentDate);

                    if (result > 0)
                    {
                        finalListCours.Add(myListCours[i]);
                    }
                }

                //On vérifie la date par rapport à la liste des examens.
                for (int i = 0; i < examNum; i++)
                {
                    DateTime endDate = myListExamen[i].dateFin;
                    DateTime currentDate = DateTime.Today;
                    int result = DateTime.Compare(endDate, currentDate);

                    if (result > 0)
                    {
                        finalListExamen.Add(myListExamen[i]);
                    }
                }

                //On ajoute au viewBag les valeurs gardées précédement.
                ViewBag.Competition = finalListCompetition;
                ViewBag.Extra = finalListExtra;
                ViewBag.Stage = finalListStage;
                ViewBag.Personne = myListPersonne;
                ViewBag.Cours = finalListCours;
                ViewBag.Examen = finalListExamen;

                return View();
            }
            catch
            {
                return View("Erreur");
            }
           
        }
    }
}