﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using projet_club_sportif.Models;

namespace projet_club_sportif.Controllers
{
    [Authorize]
    public class competitionsController : Controller
    {
        private Competition competition;
        private LieuDAO lieuDao;

        public competitionsController() : this(new Competition(), new LieuDAO())
        {

        }

        private competitionsController(Competition UneCompetition, LieuDAO UnLieuDao)
        {
            competition = UneCompetition;
            lieuDao = UnLieuDao;
        }

        // GET: competitions
        public ActionResult Index()
        {
            var competitions = competition.GetAll();
            return View(competitions.ToList());
        }

        // GET: competitions/Create
        public ActionResult GetPrix(int num_prix)
        {
            ViewBag.numPrix = num_prix;

            return View(competition);
        }

        // GET: competitions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            competition.Find((int) id);
            if (competition == null)
            {
                return HttpNotFound();
            }
            return View(competition);
        }

        // GET: competitions/Create
        public ActionResult Create()
        {
            ViewBag.lieux = lieuDao.GetAllForEvents();

            return View(competition);
        }

        // POST: competitions/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Competition uneCompetition)
        {
            //variables
            var form = Request.Form;
            string[] arrayIndex = form.GetValues("Index");
            bool conversionOk = false;

            uneCompetition.listPrix = new List<T_PrixCompetition>();
            int nbCategories;
            decimal prix;
            foreach (string unIndex in arrayIndex)
            {
                T_PrixCompetition unPrix = new T_PrixCompetition();
                conversionOk = int.TryParse(form.Get("listPrix[" + unIndex + "].nbCategories"), out nbCategories);
                conversionOk &= decimal.TryParse(form.Get("listPrix[" + unIndex + "].prix"), out prix);
                if (conversionOk)
                {
                    unPrix.nbCategories = nbCategories;
                    unPrix.prix = prix;
                    uneCompetition.listPrix.Add(unPrix);
                }
            }

            //validation
            ModelState.Clear();
            TryValidateModel(uneCompetition);

            if (ModelState.IsValid)
            {
                competition.Create(uneCompetition);
                return RedirectToAction("Index");
            }

            return View(uneCompetition);
        }

        // GET: competitions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            competition.Find((int) id);
            if (competition == null)
            {
                return HttpNotFound();
            }

            ViewBag.lieux = lieuDao.GetAllForEvents();

            return View(competition);
        }

        // POST: competitions/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Competition uneCompetition)
        {
            //variables
            var form = Request.Form;
            string[] arrayIndex = form.GetValues("Index");
            bool conversionOk = false;

            uneCompetition.listPrix = new List<T_PrixCompetition>();
            int nbCategories;
            decimal prix;
            foreach (string unIndex in arrayIndex)
            {
                T_PrixCompetition unPrix = new T_PrixCompetition();
                conversionOk = int.TryParse(form.Get("listPrix[" + unIndex + "].nbCategories"), out nbCategories);
                conversionOk &= decimal.TryParse(form.Get("listPrix[" + unIndex + "].prix"), out prix);
                if (conversionOk)
                {
                    unPrix.nbCategories = nbCategories;
                    unPrix.prix = prix;
                    uneCompetition.listPrix.Add(unPrix);
                }
            }

            //validation
            ModelState.Clear();
            TryValidateModel(uneCompetition);

            if (ModelState.IsValid)
            {
                competition.Find(uneCompetition.fk_evenement);
                competition.Update(uneCompetition);
                return RedirectToAction("Index");
            }

            return View(uneCompetition);
        }

        // GET: competitions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            competition.Find((int) id);
            if (competition == null)
            {
                return HttpNotFound();
            }
            return View(competition);
        }

        // POST: competitions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            competition.Find(id);
            competition.Delete();
            return RedirectToAction("Index");
        }

    }
}
