﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using projet_club_sportif.Models;

namespace projet_club_sportif.Controllers
{
    [Authorize]
    public class themesController : Controller
    {
        private DAO<T_Theme> themeDao;

        public themesController() : this(new ThemeDAO())
        {

        }

        private themesController(ThemeDAO UnThemeDao)
        {
            themeDao = UnThemeDao;
        }

        // GET: themes
        public ActionResult Index()
        {
            return View(themeDao.GetAll().ToList());
        }

        // GET: themes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T_Theme unTheme = themeDao.Find((int) id);
            if (unTheme == null)
            {
                return HttpNotFound();
            }
            return View(unTheme);
        }

        // GET: themes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: themes/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(T_Theme unTheme)
        {
            if (ModelState.IsValid)
            {
                themeDao.Create(unTheme);
                return RedirectToAction("Index");
            }

            return View(unTheme);
        }

        // GET: themes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T_Theme theme = themeDao.Find((int) id);
            if (theme == null)
            {
                return HttpNotFound();
            }
            return View(theme);
        }

        // POST: themes/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(T_Theme unTheme)
        {
            if (ModelState.IsValid)
            {
                themeDao.Update(unTheme);
                return RedirectToAction("Index");
            }
            return View(unTheme);
        }

        // GET: themes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T_Theme unTheme = themeDao.Find((int) id);
            if (unTheme == null)
            {
                return HttpNotFound();
            }
            return View(unTheme);
        }

        // POST: themes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            T_Theme unTheme = themeDao.Find(id);
            themeDao.Delete(unTheme);
            return RedirectToAction("Index");
        }
    }
}
