﻿using System.Web.Security;
using System.Web.Mvc;
using projet_club_sportif.Models;
using projet_club_sportif.ViewModels;

namespace projet_club_sportif.Controllers
{
    public class loginController : Controller
    {
        private PersonneDAO PersonneDao;
        private PersonneViewModel leViewModel;

        public loginController() : this(new PersonneDAO())
        {

        }

        private loginController(PersonneDAO UnPersonneDao)
        {
            PersonneDao = UnPersonneDao;
        }


        public ActionResult Index()
        {
            leViewModel = new PersonneViewModel { Authentifie = HttpContext.User.Identity.IsAuthenticated };
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                leViewModel.Personne = PersonneDao.FindByEmail(HttpContext.User.Identity.Name);
            }
            return View(leViewModel);
        }


        [HttpPost]
        public ActionResult Index(PersonneViewModel viewModel, string ReturnUrl)
        {
            if (ModelState.IsValid)
            {
                if (PersonneDao.Authenticate(viewModel.Personne.email, viewModel.Personne.mdp))
                {
                    T_Personne personne = PersonneDao.FindByEmail(viewModel.Personne.email);
                    FormsAuthentication.SetAuthCookie(personne.email.ToString(), false);
                    if (!string.IsNullOrWhiteSpace(ReturnUrl) && Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    
                    //si pas de return url
                    if(personne.staff)
                        return RedirectToAction("Instructeur", "menu");
                    else
                        return RedirectToAction("Eleve", "menu");
                }
                ModelState.AddModelError("Personne.email", "Email et/ou mot de passe incorrect(s)");
            }
            return View(viewModel);
        }

        public ActionResult CreerCompte()
        {
            return View();
        }

        public ActionResult Deconnexion()
        {
            FormsAuthentication.SignOut();
            return Redirect("/");
        }
    }
}
