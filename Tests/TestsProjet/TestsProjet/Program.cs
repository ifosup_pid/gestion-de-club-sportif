﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsProjet
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dateDebut = new DateTime(2016, 09, 25, 18, 55, 00);
            DateTime dateFin = new DateTime(2016, 10, 25, 15, 00, 00);
            Extra myExtra = new Extra();
            myExtra.dateDebut = dateDebut;
            myExtra.dateFin = dateFin;
            myExtra.titre = "Ceci est un test nouveau";
            myExtra.fk_lieu = 3;

            try
            {
                ExtraDAO myDAO = new ExtraDAO();

                myExtra = myDAO.Create(myExtra);
                Console.Write(myExtra.dateDebut);
                Console.Write(myExtra.fk_evenement);

                myExtra.dateDebut = new DateTime(2016, 09, 25, 15, 01, 01);
                myExtra = myDAO.Update(myExtra);
                Console.Write(myExtra.dateDebut);
                Console.ReadLine();

                myDAO.Delete(myExtra);
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
                Console.ReadLine();
            }
        }      
    }
}
