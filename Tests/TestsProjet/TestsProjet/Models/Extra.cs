namespace TestsProjet
{
    using System;
    using System.Collections.Generic;
    
    public class Extra
    {
        public Evenement myEvent = new Evenement();
        public string titre { get; set; }
        public int fk_evenement { get; set; }
        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }
        public int fk_lieu { get; set; }
        public List<int> organisateurs { get; set; }
        public List<int> participants { get; set; }
    }
}
