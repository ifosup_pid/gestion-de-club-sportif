namespace TestsProjet
{
    using System;
    using System.Collections.Generic;
    
    public class Theme
    {
        int pk_theme { get; set; }
        string theme { get; set; }
        List<int> fk_stage { get; set; }
        List<int> fk_cours { get; set; }
        List<int> fk_evenement { get; set; }
    }
}
