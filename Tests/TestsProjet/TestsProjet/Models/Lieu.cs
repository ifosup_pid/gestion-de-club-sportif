namespace TestsProjet
{
    using System;
    using System.Collections.Generic;
    
    public class Lieu
    {
        int pk_lieu { get; set; }
        string adresse { get; set; }
        string ville { get; set; }
        string pays { get; set; }
    }
}
