namespace TestsProjet
{
    using System;
    using System.Collections.Generic;
    
    public class Categorie
    {
        int pk_categorie { get; set; }
        string type { get; set; }
        List<int> fk_competition { get; set; }
    }
}
