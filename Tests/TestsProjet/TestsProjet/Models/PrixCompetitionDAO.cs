﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Implémentation de l'abstract DAO dans le cadre de la classe métier PrixCompetition
namespace TestsProjet
{
    public class PrixCompetitionDAO : DAO<PrixCompetition>
    {
        public override PrixCompetition Create(PrixCompetition newObject)
        {
            throw new NotImplementedException();
        }

        public override bool Delete(PrixCompetition actualObject)
        {
            throw new NotImplementedException();
        }

        public override PrixCompetition Find(string name)
        {
            throw new NotImplementedException();
        }

        public override PrixCompetition Find(int id)
        {
            throw new NotImplementedException();
        }

        public override PrixCompetition Update(PrixCompetition actualObject)
        {
            throw new NotImplementedException();
        }
    }
}