﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Extra
namespace TestsProjet
{
    public class ExtraDAO : DAO<Extra>
    {
        /// <summary>
        /// Méthode qui permert de récupérer l'ID de la prochaine ligne enregistrée dans une table.
        /// </summary>
        /// <param name="table"></param>
        /// <returns>int</returns>
        private int RecovertID(string table)
        {
            try
            {
                connection.Open();
                string sqlText = "SHOW TABLE STATUS FROM gcsp LIKE '" + table + "';";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();

                Console.Write(reader["Auto_increment"].ToString());
                string pk_event = reader["Auto_increment"].ToString();
                connection.Close();

                return Int32.Parse(pk_event);
            }
            catch(Exception e)
            {
                Console.Write(e.ToString());
                return 0;
            }
        }

        /// <summary>
        /// Ajoute l'objet Extra dans la base de données et ajoute à l'objet sa clef primaire puis le retourne.
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns>Extra</returns>
        public override Extra Create(Extra newObject)
        {
            try
            {
                int id = RecovertID("evenement");
                //On insert dans la table événement les données.
                connection.Open();
                string sqlText = "INSERT INTO evenement (debut, fin, id_lieu) VALUES(@debut, @fin, @fk_lieu);";
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@debut", newObject.dateDebut);
                cmd.Parameters.AddWithValue("@fin", newObject.dateFin);
                cmd.Parameters.AddWithValue("@fk_lieu", newObject.fk_lieu);

                cmd.ExecuteNonQuery();
                connection.Close();

                //On insert dans la table extra les données.
                connection.Open();
                sqlText = "INSERT INTO extra (titre, id_evenement) VALUES(@titre, @id_evenement);";
                cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@titre", newObject.titre);
                cmd.Parameters.AddWithValue("@id_evenement", id);

                cmd.ExecuteNonQuery();
                connection.Close();

                newObject.fk_evenement = id;
                return newObject;
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Supprime l'objet passé de la base de données et retourne true.
        /// En cas d'échec, retourne false.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns>bool</returns>
        public override bool Delete(Extra actualObject)
        {
            try
            {
                //On supprime l'extra.
                connection.Open();
                string sqlText = "DELETE FROM extra WHERE titre = @titre";

                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@titre", actualObject.titre);
                cmd.ExecuteNonQuery();
                connection.Close();

                //On supprime l'événement.
                connection.Open();
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM evenement WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_evenement", actualObject.fk_evenement);
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Recherche dans la base de données l'Extra ayant l'ID donné. Retourne l'objet en cas de réussite.
        ///Retourne null en cas d'échec.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override Extra Find(int id)
        {
            connection.Open();
            string sqlText = "SELECT * FROM extra INNER JOIN evenement ON " +
                "extra.id_evenement = evenement.id_evenement WHERE extra.id_evenement = @id_evenement";
            MySqlCommand cmd = new MySqlCommand(sqlText, connection);

            cmd.Prepare();
            cmd.Parameters.AddWithValue("@id_evenement", id);

            MySqlDataReader reader = cmd.ExecuteReader();

            reader.Read();
            Extra myExtra = new Extra();
            myExtra.titre = (string)reader["titre"];
            myExtra.dateDebut = (DateTime)reader["debut"];
            myExtra.dateFin = (DateTime)reader["fin"];
            myExtra.fk_lieu = (int)reader["id_lieu"];
            myExtra.fk_evenement = (int)reader["id_evenement"];

            connection.Close();
            return myExtra;
        }

        /// <summary>
        /// Recherche dans la base de données l'extra ayant le titre donné. Retourne l'objet en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Extra</returns>
        public override Extra Find(string name)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT id_evenement FROM extra WHERE titre = @titre;";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@titre", name);

                MySqlDataReader reader = cmd.ExecuteReader();

                reader.Read();
                int id = (int)reader["id_evenement"];
                connection.Close();

                Extra myExtra = Find(id);

                return myExtra;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste des Extras.
        /// </summary>
        /// <returns>List(Extra)</returns>
        public List<Extra> GetAll()
        {
            connection.Open();
            string sqlText = "SELECT * FROM extra INNER JOIN evenement ON " +
                "extra.id_evenement = evenement.id_evenement;";
            MySqlCommand cmd = new MySqlCommand(sqlText, connection);
            MySqlDataReader reader = cmd.ExecuteReader();

            List<Extra> rows = new List<Extra>();

            while (reader.Read())
            {
                Extra myExtra = new Extra();
                myExtra.titre = (string)reader["titre"];
                myExtra.dateDebut = (DateTime)reader["debut"];
                myExtra.dateFin = (DateTime)reader["fin"];
                myExtra.fk_lieu = (int)reader["id_lieu"];
                myExtra.fk_evenement = (int)reader["id_evenement"];

                rows.Add(myExtra);
            }

            connection.Close();
            return rows;
        }

        /// <summary>
        /// Met à jour la Base de données concernant l'objet passé en argument. Retourne l'objet mis à jour en cas de réussite.
        /// Retourne null en cas d'échec.
        /// </summary>
        /// <param name="actualObject"></param>
        /// <returns></returns>
        public override Extra Update(Extra actualObject)
        {
            try
            {
                //On modifie les données dans Extra.
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE extra SET titre = @titre WHERE id_evenement = @id_evenement;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@titre", actualObject.titre);
                cmd.Parameters.AddWithValue("@id_evenement", actualObject.fk_evenement);

                cmd.ExecuteNonQuery();
                connection.Close();

                //On modifie les données dans événement.
                connection.Open();
                cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE evenement SET debut = @debut, fin = @fin, id_lieu = @fk_lieu WHERE id_evenement = @id_evenement";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@debut", actualObject.dateDebut);
                cmd.Parameters.AddWithValue("@fin", actualObject.dateFin);
                cmd.Parameters.AddWithValue("@fk_lieu", actualObject.fk_lieu);
                cmd.Parameters.AddWithValue("@id_evenement", actualObject.fk_evenement);

                cmd.ExecuteNonQuery();
                connection.Close();
                return actualObject;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}