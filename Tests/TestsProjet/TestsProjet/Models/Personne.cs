using System;
using System.Collections.Generic;

namespace TestsProjet
{    
    public class Personne
    {
        private List<string> _fk_evenement = new List<string>();
        public int pk_personne { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
        public string mdp { get; set; }
        public int taille { get; set; }
        public int poids { get; set; }
        public int age { get; set; }
        public short sexe { get; set; }
        public bool role { get; set; }
        public short grade { get; set; }
        public int fk_lieu { get; set; }

        //impossible de faire un get/set classique avec une liste
        public List<string> fk_evenement
        {
            get
            {
                return _fk_evenement;
            }
        }
        public void setEvenement(string value)
        {
            _fk_evenement.Add(value);
        }
    }
}
