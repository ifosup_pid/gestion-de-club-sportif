﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Categorie
namespace TestsProjet
{
    public class CategorieDAO : DAO<Categorie>
    {
        public override Categorie Create(Categorie newObject)
        {
            throw new NotImplementedException();
        }

        public override bool Delete(Categorie actualObject)
        {
            throw new NotImplementedException();
        }

        public override Categorie Find(string name)
        {
            throw new NotImplementedException();
        }

        public override Categorie Find(int id)
        {
            throw new NotImplementedException();
        }

        public override Categorie Update(Categorie actualObject)
        {
            throw new NotImplementedException();
        }
    }
}