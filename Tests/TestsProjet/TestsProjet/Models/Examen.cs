namespace TestsProjet
{
    using System;
    using System.Collections.Generic;
    
    public class Examen
    {
        int pk_examen { get; set; }
        short section { get; set; }
        int grade { get; set; }
        int fk_evenement { get; set; }
        int fk_cours { get; set; }
    }
}
