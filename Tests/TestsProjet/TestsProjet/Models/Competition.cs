namespace TestsProjet
{
    using System;
    using System.Collections.Generic;

    public class Competition
    {
        int pk_competition{ get; set; }
        string titre { get; set; }
        List<int> fk_category { get; set; }
    }
}
