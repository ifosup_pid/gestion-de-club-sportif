﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Participer
namespace TestsProjet
{
    public class ParticiperDAO : DAO<Participer>
    {
        public override Participer Create(Participer newObject)
        {
            throw new NotImplementedException();
        }

        public override bool Delete(Participer actualObject)
        {
            throw new NotImplementedException();
        }

        public override Participer Find(string name)
        {
            throw new NotImplementedException();
        }

        public override Participer Find(int id)
        {
            throw new NotImplementedException();
        }

        public override Participer Update(Participer actualObject)
        {
            throw new NotImplementedException();
        }
    }
}