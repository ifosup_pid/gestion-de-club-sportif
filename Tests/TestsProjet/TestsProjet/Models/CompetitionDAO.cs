﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Competition
namespace TestsProjet
{
    public class CompetitionDAO : DAO<Competition>
    {
        public override Competition Create(Competition newObject)
        {
            throw new NotImplementedException();
        }

        public override bool Delete(Competition actualObject)
        {
            throw new NotImplementedException();
        }

        public override Competition Find(string name)
        {
            throw new NotImplementedException();
        }

        public override Competition Find(int id)
        {
            throw new NotImplementedException();
        }

        public override Competition Update(Competition actualObject)
        {
            throw new NotImplementedException();
        }
    }
}