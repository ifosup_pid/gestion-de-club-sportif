﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace TestsProjet
{
    //Implémentation de l'abstract DAO dans le cadre de la classe métier Personne
    public class PersonneDAO : DAO<Personne>
    {
        public override Personne Create(Personne newObject)
        {
            try
            {
                connection.Open();
                string sqlText = "INSERT INTO personne (nom, prenom, mdp, taille, poids, age, sexe, role, grade, id_lieu) VALUES "
                    + "(@nom, @prenom, @mdp, @taille, @poids, @age, @sexe, @role, @grade, @id_lieu);";

                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@nom", newObject.nom);
                cmd.Parameters.AddWithValue("@prenom", newObject.prenom);
                cmd.Parameters.AddWithValue("@mdp", newObject.mdp);
                cmd.Parameters.AddWithValue("@taille", newObject.taille);
                cmd.Parameters.AddWithValue("@poids", newObject.poids);
                cmd.Parameters.AddWithValue("@age", newObject.age);
                cmd.Parameters.AddWithValue("@sexe", newObject.sexe);
                cmd.Parameters.AddWithValue("@role", newObject.role);
                cmd.Parameters.AddWithValue("@grade", newObject.grade);
                cmd.Parameters.AddWithValue("@id_lieu", newObject.fk_lieu);

                cmd.ExecuteNonQuery();
                connection.Close();

                Personne myPerson = Find(newObject.nom);
                return newObject;
            }
            catch(Exception e)
            {
                Console.Write(e.ToString());
                Console.ReadLine();
                return null;
            }
        }

        public override bool Delete(Personne actualObject)
        {
            try
            {
                connection.Open();
                string sqlText = "DELETE FROM personne WHERE id_personne = @id_personne";

                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sqlText;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id_personne", actualObject.pk_personne);
                cmd.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
                Console.ReadLine();
                return false;
            }
        }

        public override Personne Find(string name)
        {
            try
            {
                connection.Open();
                string sqlText = "SELECT * FROM personne WHERE nom = '" + name + "';";
                MySqlCommand cmd = new MySqlCommand(sqlText, connection);
                MySqlDataReader reader = cmd.ExecuteReader();

                reader.Read();
                Personne myPerson = new Personne();
                myPerson.pk_personne = (int)reader["id_personne"];
                myPerson.nom = (string)reader["nom"];
                myPerson.prenom = (string)reader["prenom"];
                myPerson.mdp = (string)reader["mdp"];
                myPerson.taille = (int)reader["taille"];
                myPerson.poids = (int)reader["poids"];
                myPerson.age = (int)reader["age"];
                myPerson.sexe = (SByte)reader["sexe"];
                myPerson.role = (bool)reader["role"];
                myPerson.grade = (SByte)reader["grade"];
                myPerson.fk_lieu = (int)reader["id_lieu"];

                connection.Close();
                return myPerson;
                
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public override Personne Find(int id)
        {
            throw new NotImplementedException();
        }

        public override Personne Update(Personne actualObject)
        {
            throw new NotImplementedException();
        }

        public bool Authentifier(string name, string mdp)
        {

            throw new NotImplementedException();
        }
    }
}