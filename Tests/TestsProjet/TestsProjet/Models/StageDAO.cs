﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Stage
namespace TestsProjet
{
    public class StageDAO : DAO<Stage>
    {
        public override Stage Create(Stage newObject)
        {
            throw new NotImplementedException();
        }

        public override bool Delete(Stage actualObject)
        {
            throw new NotImplementedException();
        }

        public override Stage Find(string name)
        {
            throw new NotImplementedException();
        }

        public override Stage Find(int id)
        {
            throw new NotImplementedException();
        }

        public override Stage Update(Stage actualObject)
        {
            throw new NotImplementedException();
        }
    }
}