﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Lieu
namespace TestsProjet
{
    public class LieuDAO : DAO<Lieu>
    {
        public override Lieu Create(Lieu newObject)
        {
            throw new NotImplementedException();
        }

        public override bool Delete(Lieu actualObject)
        {
            throw new NotImplementedException();
        }

        public override Lieu Find(string name)
        {
            throw new NotImplementedException();
        }

        public override Lieu Find(int id)
        {
            throw new NotImplementedException();
        }

        public override Lieu Update(Lieu actualObject)
        {
            throw new NotImplementedException();
        }
    }
}