namespace TestsProjet
{
    using System;
    using System.Collections.Generic;
    
    public class Stage
    {
        int pk_stage { get; set; }
        int cout { get; set; }
        List<int> fk_theme { get; set; }
        int fk_evenement { get; set; }
    }
}
