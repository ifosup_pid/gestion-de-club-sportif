﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Examen
namespace TestsProjet
{
    public class ExamenDAO : DAO<Examen>
    {
        public override Examen Create(Examen newObject)
        {
            throw new NotImplementedException();
        }

        public override bool Delete(Examen actualObject)
        {
            throw new NotImplementedException();
        }

        public override Examen Find(string name)
        {
            throw new NotImplementedException();
        }

        public override Examen Find(int id)
        {
            throw new NotImplementedException();
        }

        public override Examen Update(Examen actualObject)
        {
            throw new NotImplementedException();
        }
    }
}