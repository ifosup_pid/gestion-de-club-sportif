namespace TestsProjet
{
    using System;
    using System.Collections.Generic;
    
    public class PrixCompetition
    {
        int pk_prixCompetition { get; set; }
        string prix { get; set; }
        int nbCategories { get; set; }
        int fk_evenement { get; set; }
    }
}
