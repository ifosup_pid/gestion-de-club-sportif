﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Theme
namespace TestsProjet
{
    public class ThemeDAO : DAO<Theme>
    {
        public override Theme Create(Theme newObject)
        {
            throw new NotImplementedException();
        }

        public override bool Delete(Theme actualObject)
        {
            throw new NotImplementedException();
        }

        public override Theme Find(string name)
        {
            throw new NotImplementedException();
        }

        public override Theme Find(int id)
        {
            throw new NotImplementedException();
        }

        public override Theme Update(Theme actualObject)
        {
            throw new NotImplementedException();
        }
    }
}