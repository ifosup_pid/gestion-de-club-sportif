﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Cours
namespace TestsProjet
{
    public class CoursDAO : DAO<Cours>
    {
        public override Cours Create(Cours newObject)
        {
            throw new NotImplementedException();
        }

        public override bool Delete(Cours actualObject)
        {
            throw new NotImplementedException();
        }

        public override Cours Find(string name)
        {
            throw new NotImplementedException();
        }

        public override Cours Find(int id)
        {
            throw new NotImplementedException();
        }

        public override Cours Update(Cours actualObject)
        {
            throw new NotImplementedException();
        }
    }
}