namespace TestsProjet
{
    using System;
    using System.Collections.Generic;
    
    public class Evenement
    {
        int pk_evenement { get; set; }
        short type_evenement { get; set; }
        DateTime dateDebut { get; set; }
        DateTime dateFin { get; set; }
        int fk_lieu { get; set; }
        List<int> fk_personne { get; set; }
    }
}
