﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

//Création de la classe abstraite générique(cf : généricité) "DAO"
namespace TestsProjet
{
    public abstract class DAO<T>
    {
        public MySqlConnection connection = ConnectionSGDB.Connection();

        public abstract T Find(int id);
        public abstract T Find(string name);
        public abstract T Create(T newObject);
        public abstract T Update(T actualObject);
        public abstract bool Delete(T actualObject);

    }
}