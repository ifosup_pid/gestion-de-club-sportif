﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Implémentation de l'abstract DAO dans le cadre de la classe métier Evenement
namespace TestsProjet
{
    public class EvenementDAO : DAO<Evenement>
    {
        public override Evenement Create(Evenement newObject)
        {
            throw new NotImplementedException();
        }

        public override bool Delete(Evenement actualObject)
        {
            throw new NotImplementedException();
        }

        public override Evenement Find(string name)
        {
            throw new NotImplementedException();
        }

        public override Evenement Find(int id)
        {
            throw new NotImplementedException();
        }

        public override Evenement Update(Evenement actualObject)
        {
            throw new NotImplementedException();
        }
    }
}